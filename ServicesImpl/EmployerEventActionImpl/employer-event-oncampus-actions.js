'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  /**
 * onCampusActions-used for event actions
 * @constructor
 * @param {object} eventResponseData -get event response data from employer event
 * @param {object} inputData -details entered by the user
 * @param {function} cb-callbcak
 */
function onCampusActions(eventResponseData, inputData, cb) {
  if (inputData.action == 'Event Request to Campus') {
    //send event request to campus
    eventRquestToCampus(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Share Shortlisted Students') {
    //employer event action share shortlisted students
    eventActionShortListedStudents(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Offer') {
    //employer event offer action
    eventActionOfferStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Reject') {
    //employer event reject action
    eventActionRejectStudent(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else if (inputData.action == 'Close') {
    //employer event close action
    eventActionClose(eventResponseData, inputData, function(err, respData) {
      if (err) {
        cb(err, null);
      } else if (respData) {
        cb (null, respData);
      } else {
        errorResponse(cb);
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Action Not Found';
    errResp.status = 'failure';
    cb(errResp, null);
  }
};

//Employer event request to campus
function eventRquestToCampus(eventResponseData, inputData, cb) {
  if (eventResponseData.eventStatusValueId == 313) {
    var eventApprovedObj = {};
    eventApprovedObj = eventResponseData;
    eventApprovedObj.eventStatusValueId = 314;
    eventResponseData.updateAttributes(eventApprovedObj, function(err, info) {
      if (err) {
        cb(err, null);
      } else {
        var resp = {};
        resp.message = 'event published';
        cb(null, resp);
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Can"t send request to campus';
    errResp.status = 'failure';
    cb(errResp, null);
  }
}

//Employer event short listed students
function eventActionShortListedStudents(eventResponseData, inputData, cb) {
  var inputshareObject = {};
  inputshareObject['employerEventId'] = eventResponseData.empEventId;
  if (eventResponseData.eventStatusValueId == 317) {
    var eventShortListedObj = {};
    eventShortListedObj = eventResponseData;
    eventShortListedObj.eventStatusValueId = 318;
    eventResponseData.updateAttributes(eventShortListedObj, function(err, info) {
      if (err) {
        cb(err, null);
      } else {
        //find employer event in campus event
        var campusEventmodel = server.models.CampusEvent;
        campusEventmodel.findOne({
          'where': {
            'and': [inputshareObject],
          },
        }, function(err, campusResp) {
          if (err) {
            cb(err, null);
          } else {
            var capusEventObj = {};
            capusEventObj = campusResp;
            capusEventObj.eventStatusValueId = 318;
            campusResp.updateAttributes(capusEventObj, function(err, campusInfo) {
              if (err) {
                cb(err, null);
              } else {
                var resp = {};
                resp.message = 'students short listed';
                cb(null, resp);
              }
            });
          }
        });
      }
    });
  } else {
    var errResp = {};
    errResp.message = 'Can"t short list the students';
    errResp.status = 'failure';
    cb(errResp, null);
  }
}

//Employer event Offer action
function eventActionOfferStudent(eventResponseData, inputData, cb) {
  if (eventResponseData.eventStatusValueId == 319) {
    var eventStudentListModel = server.models.EventStudentList;
    var inputOfferStudentObject = {};
    inputOfferStudentObject['studentId'] = inputData.studentId;
    //find student in event student list
    eventStudentListModel.findOne({
      'where': {
        'and': [inputOfferStudentObject],
      },
    }, function(err, eventStudentListResp) {
        //
      if (err) {
        cb(err, null);
      } else {
        var eventStudentListObj = {};
        eventStudentListObj = eventStudentListResp;
        eventStudentListObj.offerStatusValueId = 249;
        eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
          if (err) {
            cb(err, null);
          } else {
            var response = {};
            response.message = 'student offered';
            cb(null, response);
          }
        });
      }
    });
    //end update student list
  } else {
    var errResp = {};
    errResp.message = 'Can"t offer students';
    errResp.status = 'failure';
    cb(errResp, null);
  }
}

//Employer event reject action
function eventActionRejectStudent(eventResponseData, inputData, cb) {
  if (eventResponseData.eventStatusValueId == 319) {
    var eventStudentListModel = server.models.EventStudentList;
    var inputRejectStudentObject = {};
    inputRejectStudentObject['studentId'] = inputData.studentId;
    //find student in event student list
    eventStudentListModel.findOne({
      'where': {
        'and': [inputRejectStudentObject],
      },
    }, function(err, eventStudentListResp) {
          //
      if (err) {
        cb(err, null);
      } else {
        var eventStudentListObj = {};
        eventStudentListObj = eventStudentListResp;
        eventStudentListObj.offerStatusValueId = 251;
        //updating event student list
        eventStudentListResp.updateAttributes(eventStudentListObj, function(err, studentListUpdateInfo) {
          if (err) {
            cb(err, null);
          } else {
            var response = {};
            response.message = 'student rejected';
            cb(null, response);
          }
        });
      }
    });
      //end update student list
  } else {
    var errResp = {};
    errResp.message = 'Can"t offer students';
    errResp.status = 'failure';
    cb(errResp, null);
  }
}

//employer event close action
function eventActionClose(eventResponseData, inputData, cb) {
  var updateEmpEvent = {};
  updateEmpEvent = eventResponseData;
  updateEmpEvent.eventStatusValueId = 321;
  eventResponseData.updateAttributes(updateEmpEvent, function(err, info) {
    if (err) {
      cb(err, null);
    } else {
      var response = {};
      response.message = 'event closed';
      cb(null, response);
    }
  });
}

exports.onCampusActions = onCampusActions;
