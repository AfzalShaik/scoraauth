'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var request = require('request');
var getDetailsById = require('../CommonImpl/getEntityDetails.js');
var loopbackValidation = require('../../commonValidation/lookupMethods.js');
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
var employerEvent = server.models.EmployerEvent;
var lookup = server.models.LookupType;
var lookupvalue = server.models.LookupValue;
var eventCampus = server.models.CampusEvent;
var campusModel = server.models.CampusDrive;
var EmployerDriveCampuses = server.models.EmployerDriveCampuses;
var accpect = 'Accepted';
/**
 * campusEvent-this function deals with checking action
 * @constructor
 * @param {object} CampusEventData-contains companyid and employer event id
 * @param {function} cb-callback
 */
function campusBussinessEvent(CampusEventData, cb) {
  if (CampusEventData.Action == 'Accepted') {
    if (CampusEventData.campusDriveId) {
      if (CampusEventData.employerEventId) {
        companyAccpectEvent(CampusEventData, function(err, AcceptedResponse,
          code) {
          if (err) {
            throwError(err, cb);
          } else if (AcceptedResponse.count == 0) {
            throwError('event Not Created', cb);
          } else {
            var sucess = {};
            sucess.status = 'Sucessfully you accpected the event';
            sucess.campusEventId = null;
            sucess.statusCode = 200;
            cb(null, sucess);
          }
        });
      } else {
        throwError('employerEventId Required', cb);
      }
    } else {
      throwError('campusDriveId Required', cb);
    }
  } else if (CampusEventData.Action == 'Published') {
    if (CampusEventData.campusEventId) {
      CampusPublishEvent(CampusEventData, function(err, publishEvent) {
        if (err) {
          throwError('Error Occured', cb);
        } else {
          var publish = {};
          if (publishEvent.count == 0) {
            throwError('Invalid Id or Nothing with status Accpect', cb);
          } else {
            publish.status = 'sucess';
            cb(null, publish);
          }
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Students shortlisted') {
    if (CampusEventData.campusEventId) {
      ShareStudentList(CampusEventData, function(err, shortListResponse) {
        if (err) {
          throwError('cant update student Response', cb);
        } else {
          var status = {};
          if (shortListResponse.count == 0) {
            throwError('Invalid Id nothing with event published', cb);
          } else {
            status.status = 'sucessfully done';
            status.statusCode = 200;
            cb(null, status);
          }
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Scheduled') {
    if (CampusEventData.campusEventId) {
      if (CampusEventData.scheduledDate) {
        if (CampusEventData.scheduledStartTime) {
          scheduledEvent(CampusEventData, function(err, sheduledRes) {
            if (sheduledRes.count == 0) {
              throwError('event id you provided was not under required status', cb);
            } else {
              var status = {};
              if (err) {
                throwError('Cant shedule an Event', cb);
              } else {
                status.status = 'Sheduled Event Sucessully';
                cb(null, status);
              }
            }
          });
        } else {
          throwError('scheduledStartTime Required', cb);
        }
      } else {
        throwError('scheduledDate Required', cb);
      }
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Closed') {
    if (CampusEventData.campusEventId) {
      closeEvent(CampusEventData, function(err, closerResponse) {
        if (closerResponse.count == 0) {
          throwError('No Event was shedule with that id', cb);
        } else {
          var status = {};
          if (err) {
            throwError('THERE WAS AN ERROR', cb);
          } else {
            status.status = 'Event Closed Sucessfully';
            cb(null, status);
          }
        }
      });
    } else {
      throwError('campusEventId Required', cb);
    }
  } else if (CampusEventData.Action == 'Rejected') {
    actionRejected(CampusEventData, function(err, rejectResponse) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, rejectResponse);
      }
    });
  } else {
    throwError('Action Cant Be NULL', cb);
  }
}
/**
 * companyAccpectEvent- will work on creating am event when event was accpected
 * @constructor
 * @param {object} CampusEventData-contains Data
 * @param {function} AccpectCallback- Deals with response
 */
function companyAccpectEvent(CampusEventData, AccpectCallback) {
  var ob = {};
  ob.empEventId = CampusEventData.employerEventId;
  getDetailsById.entityDetailsById(ob, employerEvent, function(err, Campusres) {
    if (err) {} else if (Campusres.data == 0) {
      AccpectCallback('Invalid employerEventId', null);
    } else {
      eventStatusManagment(CampusEventData, Campusres,
        function(err, eventAccpect, code) {
          if (err) {
            throwError(err, AccpectCallback);
          } else {
            AccpectCallback(null, eventAccpect, code);
          }
        });
    }
  });
}
/**
 * eventStatusManagment-the function which was done after accpected company event
 * @constructor
 * @param {object} CampusEventData-contains all the data employer id and company id
 * @param {any} Campusres-contains data of employer
 */
function eventStatusManagment(CampusEventData, Campusres,
  eventAccpectCallback) {
  var lookupObject = {};
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var accpect = 'Accepted';
  typeCodeFunction(accpect, code, function(err, EducationEventres) {
    if (err) {
      throwError('error', eventAccpectCallback);
    } else {
      var company = server.models.Company;
      var companyObject = {};
      companyObject.companyId = Campusres.data.companyId;
      getDetailsById.entityDetailsById(companyObject, company,
        function(err, response2) {
          if (err) {
            throwError('cant find', eventAccpectCallback);
          } else {
            var campus = {};
            campus.campusDriveId = CampusEventData.campusDriveId;
            getDetailsById.entityDetailsById(campus, campusModel,
              function(err, campusResponse) {
                if (err) {
                  throwError('error', eventAccpectCallback);
                } else if (campusResponse.data == 0) {
                  eventAccpectCallback('Invalid Campus Drive Id', null);
                } else {
                  var object = {};
                  object = createObject(CampusEventData,
                    Campusres,
                    response2,
                    EducationEventres, campusResponse);
                  var lookupcodeEmployer = {};
                  var type = 'EMPLOYER_EVENT_STATUS_CODE';
                  var accpect = 'Accepted';
                  typeCodeFunction(accpect, type, function(err, re1) {
                    if (err) {
                      throwError('error', eventAccpectCallback);
                    } else {
                      var ob1 = {};
                      ob1.eventStatusValueId = re1[0].lookupValueId;
                      update(Campusres, object, ob1,
                        function(err, updatedAccpect, code) {
                          if (err) {
                            throwError(err, eventAccpectCallback);
                          } else if (updatedAccpect.count == 1) {
                            eventAccpectCallback(null, updatedAccpect, code);
                          } else {
                            throwError('cant create', eventAccpectCallback);
                          }
                        }
                      );
                    }
                  });
                }
              });
          }
        });
    }
  });
};
/**
 * update-deals with updatind datain the database
 * @constructor
 * @param {object} res-employer event response
 * @param {object} object-coject contains all the data need to get updated
 * @param {object} ob1-contains lookupvalueId
 */
function update(res, object, ob1, UpadatedResponse) {
  var id = res.data.empEventId;
  var employerDriveCampus = server.models.EmployerDriveCampuses;
  employerEvent.updateAll({
    'empEventId': id,
  }, ob1,
    function(err, updateResponse) {
      if (err) {
        throwError('error', UpadatedResponse);
      } else {
        // console.log('updated response', updateResponse);
        // eventCampus.create(object, function(err, objectCreated) {
        //   if (err) {
        //     console.log('sending error in 3', objectCreated);
        //     throwError('error', UpadatedResponse);
        //   } else {
        var sampleObject = {};
        sampleObject.empDriveStatusValueId = 243;
        employerDriveCampus.updateAll({
          'empEventId': 75678,
          'campsuId': 959816342,
        }, sampleObject,
          function(err, statusResponse) {
            var created = {};
            created.count = 1;
            UpadatedResponse(null, created, null);
          });
        //   } }
        // );
      }
    });
}
/**
 * CompanyPublishEvent-executes when ever event was set to public
 * @constructor
 * @param {object} CampusEventData-contains companyid
 */
function CampusPublishEvent(CampusEventData, CampuspublishCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var publish = 'Published';
  var a = {};
  var accpect = 'Accepted';
  typeCodeFunction(publish, code, function(err, PublishEventresponse) {
    if (err) {
      throwError('error', CampuspublishCallback);
    } else {
      typeCodeFunction(accpect, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', CampuspublishCallback);
        } else {
          var companyObject = {};
          var eventCampus = server.models.CampusEvent;
          companyObject.eventStatusValueId = PublishEventresponse[0].lookupValueId;
          eventCampus.updateAll({
            'eventStatusValueId': checkingResponse[0].lookupValueId,
            'campusEventId': CampusEventData.campusEventId,
          },
            companyObject,
            function(err, publishResponse) {
              if (err) {
                CampuspublishCallback('error', publishResponse);
              } else {
                if (publishResponse.count == 0) {
                  CampuspublishCallback(null, publishResponse);
                } else {
                  CampuspublishCallback(null, publishResponse);
                }
              }
            }
          );
        }
      });
    }
  });
}
//for Share Student List event
/**
 * ShareStudentList-executes when ever status was sharestudentList
 * @constructor
 * @param {object} CampusEventData-contains data with companyID
 */
function ShareStudentList(CampusEventData, studentshortListCallBack) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var share = 'Students shortlisted';
  var publish = 'Published';
  typeCodeFunction(share, code, function(err, ShareStudentEventresponse) {
    if (err) {
      studentshortListCallBack(err, null);
    } else {
      typeCodeFunction(publish, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', CampuspublishCallback);
        } else {
          var eventCampus = server.models.CampusEvent;
          var companyObject = {};
          companyObject.eventStatusValueId =
            ShareStudentEventresponse[0].lookupValueId;
          eventCampus.updateAll({
            'eventStatusValueId': checkingResponse[0].lookupValueId,
            'campusEventId': CampusEventData.campusEventId,
          },
            companyObject,
            function(err, SharedStudentsResponse) {
              if (err) {
                studentshortListCallBack('error', SharedStudentsResponse);
              } else {
                if (SharedStudentsResponse.count == 0) {
                  studentshortListCallBack(null, SharedStudentsResponse);
                } else {
                  var campusId = {};
                  campusId.campusEventId = CampusEventData.campusEventId;
                  getDetailsById.entityDetailsById(campusId, eventCampus,
                    function(err, eventRes) {
                      if (err) {
                        studentshortListCallBack('error', eventRes);
                      } else {
                        var evenStudentlist = server.models.EventStudentList;
                        var EventStudentListObject = {};
                        EventStudentListObject.companyId = eventRes.data[0].companyId;
                        EventStudentListObject.employerEventId =
                          eventRes.data[0].employerEventId;
                        evenStudentlist.updateAll({
                          'campusEventId': CampusEventData.campusEventId,
                        },
                          EventStudentListObject,
                          function(err, evenStudentlistResponse) {
                            if (err) {
                              studentshortListCallBack('error', evenStudentlistResponse);
                            } else {
                              var code = 'EMPLOYER_EVENT_STATUS_CODE';
                              var screening = 'Screening';
                              typeCodeFunction(screening, code, function(err, employerstatus) {
                                if (err) {
                                  studentshortListCallBack('error', employerstatus);
                                } else {
                                  var statusId = {};
                                  statusId.eventStatusValueId = employerstatus[0].lookupValueId;
                                  employerEvent.updateAll({
                                    'empEventId': eventRes.data[0].employerEventId,
                                  },
                                    statusId,
                                    function(err, employerstatus) {
                                      if (err) {
                                        studentshortListCallBack('error', employerstatus);
                                      } else {
                                        studentshortListCallBack(null, employerstatus);
                                      }
                                    });
                                }
                              });
                            }
                          });
                      }
                    }
                  );
                };
              }
            }
          );
        }
      });
    }
  });
}
//Sheduled event
/**
 * scheduledEvent-for changing event status to shedule event
 * @constructor
 * @param {object} CampusEventData-contains data that need to get updated
 */
function scheduledEvent(CampusEventData, sheduledCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var sheduled = 'Scheduled';
  var share = 'Students shortlisted';
  typeCodeFunction(sheduled, code, function(err, sheduledResponse) {
    if (err) {
      sheduledCallback('error', sheduledResponse);
    } else {
      typeCodeFunction(share, code, function(err, checkingResponse) {
        if (err) {
          throwError('error', CampuspublishCallback);
        } else {
          var sheduleCampus = {};
          sheduleCampus.campusEventId = CampusEventData.campusEventId;
          getDetailsById.entityDetailsById(sheduleCampus, eventCampus,
            function(err, sheduleRes) {
              if (err) {
                sheduledCallback('Invalid ID', sheduleRes);
              } else if (sheduleRes.data == 0) {
                sheduledCallback('Invalid ID', sheduleRes);
              } else {
                var sheduleEvent = {};
                sheduleEvent.eventStatusValueId = sheduledResponse[0].lookupValueId;
                sheduleEvent.scheduledDate = CampusEventData.scheduledDate;
                sheduleEvent.scheduledStartTime =
                  CampusEventData.scheduledStartTime;
                eventCampus.updateAll({
                  'eventStatusValueId': checkingResponse[0].lookupValueId,
                  'employerEventId': sheduleRes.data[0].employerEventId,
                },
                  sheduleEvent,
                  function(err, eventResponse) {
                    if (eventResponse.count == 0) {
                      sheduledCallback(null, eventResponse);
                    } else {
                      if (err) {
                        sheduledCallback('eror', eventResponse);
                      } else {
                        var code = 'EMPLOYER_EVENT_STATUS_CODE';
                        var Scheduled = 'Scheduled';
                        typeCodeFunction(Scheduled, code, function(err, employerstatus) {
                          if (err) {
                            sheduledCallback('error', employerstatus);
                          } else {
                            var statusId = {};
                            statusId.eventStatusValueId = employerstatus[0].lookupValueId;
                            employerEvent.updateAll({
                              'empEventId': sheduleRes.data[0].employerEventId,
                            },
                              statusId,
                              function(err, employerstatus) {
                                if (err) {
                                  sheduledCallback('error', employerstatus);
                                } else {
                                  sheduledCallback(null, employerstatus);
                                }
                              });
                          }
                        });
                      }
                    }
                  });
              }
            }
          );
        }
      });
    }
  });
}
/**
 * closeEvent - function Deals with close the event
 * @constructor
 * @param {object} CampusEventData -contains Data id and action
 * @param {function} closedCallback -Deals with response
 */
function closeEvent(CampusEventData, closedCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var Closed = 'Closed';
  typeCodeFunction(Closed, code, function(err, closedResponse) {
    if (err) {
      closedCallback('error', closedResponse);
    } else {
      var campusObject = {};
      campusObject.eventStatusValueId = closedResponse[0].lookupValueId;
      eventCampus.updateAll({
        'campusEventId': CampusEventData.campusEventId,
      },
        campusObject,
        function(err, closeResponse) {
          if (err) {
            closedCallback('error', null);
          } else {
            closedCallback(null, closeResponse);
          }
        });
    }
  });
}
/**
 * createObject-for creating data with responses and inputs
 * @constructor
 * @param {object} CampusEventData-contains all the data inputs
 * @param {any} res-contains employer event response
 * @param {any} response2-contains event details
 * @param {any} resp-contains lookupvalueId
 * @returns an object which contains all the details
 */
function createObject(CampusEventData, res, response2, resp, drive) {
  var object1 = {};
  object1.campusId = drive.data.campusId;
  object1.campusDriveId = drive.data.campusDriveId;
  object1.employerEventId = CampusEventData.employerEventId;
  object1.companyId = res.data.companyId;
  object1.eventName = response2.data.name;
  object1.eventTypeValueId = res.data.eventTypeValueId;
  object1.eventStatusValueId = resp[0].lookupValueId;
  object1.duration = 2;
  object1.createDatetime = new Date();
  object1.updateDatetime = new Date();
  object1.createUserId = CampusEventData.createUserId;
  object1.updateUserId = CampusEventData.createUserId;
  return object1;
}
/**
 * typeCodeFunction-for checking lookupValue and lookupId
 * @constructor
 * @param {var} value-contains lookupValue like accpected,publish....
 * @param {any} type-contains lookupCode
 * @param {any} cb-callBack function deals with response
 */
function typeCodeFunction(value, type, lookupCallback) {
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.find({
        'where': {
          lookupValue: value,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error', re1);
        } else {
          lookupCallback(null, re1);
        };
      });
    }
  });
}
var sample = [];

function actionRejected(data, rejectedCallback) {
  var code = 'EDUCATION_EVENT_STATUS_CODE';
  var value = 'Rejected';
  typeCodeFunction(value, code, function(err, lookupResponse) {
    var input = {};
    input.empEventId = data.employerEventId;
    // input.empDriveStatusValueId = 243;//lookupResponse[0].lookupValueId;
    EmployerDriveCampuses.find({
      'where': {
        'and': [input],
      },
    }, function(err, rejectEmpResponse) {
      if (err) {
        rejectedCallback(err, null);
      } else if (rejectEmpResponse == null) {
        throwError('no event with that id', rejectedCallback);
      } else {
        var async = require('async');
        sample.push(lookupResponse[0].lookupValueId);
        sample.push(rejectEmpResponse.length);
        async.map(rejectEmpResponse, findAndUpdateStatus, function(err, finalReject) {
          console.log('inasyncccccccccccccccccccc');
          if (err) {
            throwError('there was an error', rejectedCallback);
          } else {
            rejectedCallback(null, finalReject);
          }
        });
      }
    });
  });

  var count = 0;

  function findAndUpdateStatus(obj, cb) {
    if (obj.empDriveStatusValueId == 243 || obj.empDriveStatusValueId == null) {
      count++;
      console.log('sampleeeeeeeeeeeeeeeee ', sample[1], count);
      if (sample[1] === count) {
        var updateStatus = {};
        updateStatus.eventStatusValueId = sample[0];
        employerEvent.updateAll({
          'empEventId': obj.empEventId,
        }, updateStatus, function(err, updateRes) {
          if (err) {
            throwError('cant update the event status', cb);
          } else {
            console.log('in elsessssssssssssss', updateRes);
            cb(null, updateRes);
          }
        });
     }
    }
  }
}
exports.campusBussinessEvent = campusBussinessEvent;
exports.typeCodeFunction = typeCodeFunction;
