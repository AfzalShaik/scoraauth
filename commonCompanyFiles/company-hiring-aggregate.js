'use strict';
var server = require('../server/server');
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;

function createCompanyHiringAggregate(fileInput, inputData, callBc) {
  async.map(fileInput, validateInput, function(validateErr, validateOutput) {
    if (validateErr) {
      throwError(validateErr, callBc);
    } else {
      var inputArray = cleanArray(fileInput);
      if (inputArray.length > 0) {
        async.map(inputArray, verifyOrganization, organizationCB);

        function verifyOrganization(obj, orgCB) {
          var organization = server.models.Organization;
          var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
          organization.findOne({
            'where': {
              'and': [{
                'name': obj.organizationName,
              },
              {
                'companyId': inputData.companyId,
              },
              ],
            },
          }, function(orgErr, orgOutput) {
            if (orgErr) {
              obj.error = 'ORGANIZATION does not exist';
              companyHiringAggregatesWork.create(obj, function(err, errResp) {
                orgCB(null, null);
              });
            } else if (orgOutput) {
              obj.organizationId = orgOutput.organizationId;
              obj.companyId = orgOutput.companyId;
              orgCB(null, obj);
            } else {
              obj.error = 'ORGANIZATION does not exist';
              companyHiringAggregatesWork.create(obj, function(err, errResp) {
                orgCB(null, null);
              });
            }
          });
        }

        function organizationCB(orgError, orgOut) {
          var organizationResponse = cleanArray(orgOut);
          if (organizationResponse.length > 0) {
            async.map(organizationResponse, verifyCampus, campusCallBack);
          } else {
            throwError('Invalid File', callBc);
          }
        }

        function verifyCampus(object, campusCB) {
          var campus = server.models.Campus;
          var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
          campus.findOne({
            'where': {
              'name': object.campusName,
            },
          }, function(error, campusOut) {
            if (error) {
              object.error = 'CAMPUS does not exist';
              companyHiringAggregatesWork.create(object, function(err, errResp) {
                campusCB(null, null);
              });
            } else if (campusOut) {
              object.campusId = campusOut.campusId;
              campusCB(null, object);
            } else {
              object.error = 'CAMPUS does not exist';
              companyHiringAggregatesWork.create(object, function(err, errResp) {
                campusCB(null, null);
              });
            }
          });
        }

        function campusCallBack(error, campusOut) {
          var campusResponse = cleanArray(campusOut);
          if (campusResponse.length > 0) {
            async.map(campusResponse, verifyJobRole, jobRoleCallBack);
          } else {
            throwError('Invalid File', callBc);
          }
        }

        function verifyJobRole(obj, jobroleCb) {
          var jobRole = server.models.JobRole;
          var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
          jobRole.findOne({
            'where': {
              'and': [{
                'jobRoleName': obj.jobRoleName,
              }, {
                'companyId': inputData.companyId,
              }],
            },
          }, function(err, jobOut) {
            if (err) {
              obj.error = 'Job Role does not exist';
              companyHiringAggregatesWork.create(obj, function(err, errResp) {
                jobroleCb(null, null);
              });
            } else if (jobOut) {
              obj.jobRoleId = jobOut.jobRoleId;
              jobroleCb(null, obj);
            } else {
              obj.error = 'Job Role does not exist';
              companyHiringAggregatesWork.create(obj, function(err, errResp) {
                jobroleCb(null, null);
              });
            }
          });
        }

        function jobRoleCallBack(error, roleOut) {
          var roleResponse = cleanArray(roleOut);
          console.log(roleResponse.length);
          if (roleResponse.length > 0) {
            async.map(roleResponse, verifyValidation, validationCallBack);
          } else {
            throwError('Invalid File', callBc);
          }
        }

        function verifyValidation(obj, validationCB) {
          var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
          if (obj.noOfOffers > 0 && obj.sumOfOffers > 0 && obj.minimumOffer > 0 && obj.maximumOffer > 0 && (parseInt(obj.sumOfOffers) > parseInt(obj.maximumOffer)) && (parseInt(obj.maximumOffer) >= parseInt(obj.minimumOffer))) {
            validationCB(null, obj);
          } else {
            obj.error = 'Validation Failed';
            companyHiringAggregatesWork.create(obj, function(err, resp) {
              validationCB(null, null);
            });
          }
        }

        function validationCallBack(error, validationOut) {
          var validationResponse = cleanArray(validationOut);
          var companyHiringAggregates = server.models.CompanyHiringAggregates;
          var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
          var createCompanyLog = require('./create-company-log').createCompanyLog;
          var inputFilterObject = {};
          inputFilterObject['companyId'] = inputData.companyId;
          deleteSingleEntityDetails(inputFilterObject, companyHiringAggregates, function(error, destroy) {
            companyHiringAggregates.create(validationResponse, function(createErr, createOut) {
              companyHiringAggregatesWork.find({}, function(failerdErr, failedOutput) {
                readErrorRecords(failedOutput, inputData, function(readError, readResponse) {
                  var logInput = {
                    'compUploadTypeValueId': inputData.compUploadTypeValueId,
                    'companyId': inputData.companyId,
                    'container': inputData.fileDetails.container,
                    'name': inputData.fileDetails.name,
                    'originalFileName': inputData.fileDetails.originalFileName,
                    'totalNoRecs': fileInput.length,
                    'noFailRecs': cleanArray(failedOutput).length,
                    'noSuccessRecs': createOut.length,
                  };
                  createCompanyLog(logInput, function(logError, logOutput) {
                    companyHiringAggregatesWork.destroyAll({}, function(destroyError, destroyOutput) {
                      callBc(null, logOutput);
                    });
                  });
                });
              });
            });
          });
        }
      } else {
        throwError('Invalid File', callBc);
      }
    }
  });

  function validateInput(validateObj, validateCB) {
    var eventTestScoreValidation = require('../commonValidation/company-hiring-aggregate-validation').validateJson;
    var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
    eventTestScoreValidation(validateObj, function(validationErr, validationResponse) {
      if (validationErr) {
        validateObj.error = validationErr.err;
        companyHiringAggregatesWork.create(validateObj, function(err, resp) {
          validateCB(null, null);
        });
      } else {
        validateCB(null, validateObj);
      }
    });
  }

  function readErrorRecords(finalArray, stDataa, cb) {
    var ErrorArray = [];
    for (var i = 0; i < finalArray.length; i++) {
      var obj = {
        'rowNumber': finalArray[i].rowNumber,
        'campusName': finalArray[i].campusName,
        'testVendorName': finalArray[i].testVendorName,
        'organizationName': finalArray[i].organizationName,
        'jobRoleName': finalArray[i].jobRoleName,
        'error': finalArray[i].error,
        'noOfOffers': finalArray[i].noOfOffers,
        'sumOfOffers': finalArray[i].sumOfOffers,
        'minimumOffer': finalArray[i].minimumOffer,
        'maximumOffer': finalArray[i].maximumOffer,
        'calendarYear': finalArray[i].calendarYear,
      };
      ErrorArray.push(obj);
    }
    var fs = require('fs');
    var csv = require('fast-csv');
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    var errorCsv = './attachments/' + container + '/' + name;
    // console.log('errorCSV', errorCsv);
    var ws = fs.createWriteStream(errorCsv);
    csv
      .writeToPath(errorCsv, ErrorArray, {
        headers: true,
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, 'success');
  }
}
exports.createCompanyHiringAggregate = createCompanyHiringAggregate;
