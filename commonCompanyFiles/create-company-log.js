'use strict';
var server = require('../server/server');

function createCompanyLog(input, callBc) {
  var companyUploadLog = server.models.CompanyUploadLog;
  var logInput = {
    'compUploadTypeValueId': input.compUploadTypeValueId,
    'companyId': input.companyId,
    'inputParameters': 'string',
    'createUserId': 1,
    'createDatetime': new Date(),
    'csvFileLocation': './attachments/' + input.container + '/' + input.name + '/' + input.originalFileName,
    'errorFileLocation': './attachments/' + input.container + '/' + 'download' + '/' + input.name,
    'totalNoRecs': input.totalNoRecs,
    'noFailRecs': input.noFailRecs,
    'noSuccessRecs': input.noSuccessRecs,
  };
  companyUploadLog.create(logInput, function(logErr, logOutput) {
    callBc(null, logOutput);
  });
}
exports.createCompanyLog = createCompanyLog;
