'use strict';
var roleValidation = require('../commonValidation/job-role-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var jobRole = server.models.JobRole;
  /**
   *updateJobRoleService-function deals with updating the job role data
   *@constructor
   * @param {object} roleData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateJobRoleService(roleData, cb) {
  if (roleData) {
    if (roleData.jobRoleId && roleData.companyId) {
      var validValue;
      roleValidation.validateJobRoleJson(roleData, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['jobRoleId'] = roleData.jobRoleId;
          inputFilterObject['companyId'] = roleData.companyId;
          findEntity.entityDetailsById(inputFilterObject, jobRole, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching job role record');
            } else if (response) {
              roleValidation.validateJobRoleLookup(roleData, function(lookupErr, lookupValue) {
                if (lookupErr) {
                  throwError(lookupErr, cb);
                  logger.error('error while validating package lookups');
                } else if (lookupValue == true) {
                  response.data[0].updateAttributes(roleData, function(err, info) {
                    if (err) {
                      throwError('error while updating jo role', cb);
                    } else {
                      logger.info('job role record Updated Successfully');
                      info['requestStatus'] = true;
                      info['updateDatetime'] = new Date();
                      cb(null, info);
                    }
                  });
                } else {
                  throwError('invalid  jobRoleTypeValueId', cb);
                }
              });
            } else {
              throwError('Invalid companyId or jobRoleId', cb);
              logger.error('Invalid companyId or jobRoleId');
            }
          });
        }
      });
    } else {
      throwError('jobRoleId and companyId are required', cb);
      logger.error('jobRoleId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateJobRoleService = updateJobRoleService;
