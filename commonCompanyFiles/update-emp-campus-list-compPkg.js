'use strict';
var empCompPkgValidation = require('../commonValidation/all-models-validation');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js');
var server = require('../server/server');
var employerCampusListCompPakJson = require('../commonValidation/all-models-json').employerCampusListCompPakJson;
var employerCampusListCompPkg = server.models.EmployerCampusListCompPkg;
  /**
   *updateCompPkgService-function deals with updating the company pakage service
   *@constructor
   * @param {object} empCompData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateCompPkgService(empCompData, cb) {
  if (empCompData) {
    if (empCompData.listCompPkgId && empCompData.companyId && empCompData.listId && empCompData.compPackageId) {
      var validValue;
      empCompPkgValidation.validateModelsJson(empCompData, employerCampusListCompPakJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
          logger.error('validation error');
        } else {
          var inputFilterObject = {};
          inputFilterObject['compPackageId'] = empCompData.compPackageId;
          inputFilterObject['companyId'] = empCompData.companyId;
          inputFilterObject['listCompPkgId'] = empCompData.listCompPkgId;
          inputFilterObject['listId'] = empCompData.listId;
          findEntity.entityDetailsById(inputFilterObject, employerCampusListCompPkg, function(error, response) {
            if (error) {
              throwError(error, cb);
              logger.error('Error while fetching employer campus list compPkg record');
            } else if (response) {
              response.data.updateAttributes(empCompData, function(err, info) {
                if (err) {
                  throwError('error while updating emp campus CompPkg', cb);
                } else {
                  logger.info('emp campus CompPkg record Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  cb(null, info);
                }
              });
            } else {
              throwError('Invalid companyId or compPackageId or listCompPkgId or listId', cb);
              logger.error('Invalid companyId or compPackageId or listCompPkgId or listId');
            }
          });
        }
      });
    } else {
      throwError('listId, listCompPkgId, compPackageId and companyId are required', cb);
      logger.error('listId, listCompPkgId, compPackageId and companyId are required');
    }
  } else {
    throwError('Input cannot be blank', cb);
    logger.error('Input cannot be blank');
  }
}
exports.updateCompPkgService = updateCompPkgService;
