'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var dataToExport = [];
var unregStudentData = [];
var finaldata = [];
var companyName, eventName;
  /**
 *getStudentData- for getting the student data
 *@constructor
 * @param {object} data - contains data of company,campus,employer details
 * @param {function} studentDetailsCallback - deals with response
 */
var getStudentData = function(data, studentDetailsCallback) {
  var eventList = server.models.EventStudentList;
  eventList.find({'where': {'companyId': data.companyId,
  'employerEventId': data.eventId}},
function(err, response) {
  if (err) {
    errorResponse('there was an error', studentDetailsCallback);
  } else {
    var student = server.models.Student;
    var unregStudent = server.models.UnregisterCampusStudent;
    response.map(function(studentDetails) {
      console.log(studentDetails.studentId);
      student.findById(studentDetails.studentId, function(err, studentDetailsResponse) {
        if (err) {
          console.log('err');
        } else if (studentDetailsResponse == null) {
          unregStudent.findById(studentDetails.studentId, function(err, unregResponse) {
            if (err) {
              errorResponse('there was an error', studentDetailsCallback);
            } else {
              var needtoInsert = {};
              needtoInsert.companyName = data.companyName;
              needtoInsert.campusId = data.campusId;
              needtoInsert.campusName = data.campusName;
              needtoInsert.companyId = data.companyId;
              needtoInsert.eventName = data.eventName;
              needtoInsert.empEventId = data.eventId;
              needtoInsert.studentId = unregResponse.unregStudentId;
              needtoInsert.firstName = unregResponse.firstName;
              needtoInsert.middleName = unregResponse.middleName;
              needtoInsert.lastName = unregResponse.lastName;
              needtoInsert.emailId = unregResponse.emailId;
              unregStudentData.push(needtoInsert);
              console.log(unregResponse);
            }
          });
        } else {
          var needtoInsert = {};
          needtoInsert.companyName = data.companyName;
          needtoInsert.companyId = data.companyId;
          needtoInsert.campusId = data.campusId;
          needtoInsert.campusName = data.campusName;
          needtoInsert.eventName = data.eventName;
          needtoInsert.empEventId = data.eventId;
          needtoInsert.studentId = studentDetailsResponse.studentId;
          needtoInsert.firstName = studentDetailsResponse.firstName;
          needtoInsert.middleName = studentDetailsResponse.middleName;
          needtoInsert.lastName = studentDetailsResponse.lastName;
          needtoInsert.emailId = studentDetailsResponse.emailId;
          dataToExport.push(needtoInsert);
          console.log(studentDetailsResponse);
        }
        finaldata = dataToExport.concat(unregStudentData);
        convertToCsv(finaldata);
      });
    });
  }
  studentDetailsCallback(null, 'done');
});
};
  /**
 *convertToCsv- To convert the data to csv
 *@constructor
 * @param {object} data - contains data need to be converted as csv
 * @param {function} done - deals with response
 */
var convertToCsv = function(data, done) {
  console.log('this is final data', data);
  var json2csv = require('json2csv');
  var fs = require('fs');
  var fields = ['companyName', 'companyId', 'eventName', 'empEventId', 'studentId', 'firstName', 'middleName', 'lastName', 'emailId'];
  var csv = json2csv({data: data, fields: fields});
  fs.writeFile('./commonValidation/exported.csv', csv, function(err) {
    if (err) throw err;
  });
};
  /**
 *exportData- To export data with the input as employerEventId
 *@constructor
 * @param {number} employerEventId - contains employer event Id
 * @param {function} employerCallBack - deals with response
 */
var exportData = function(employerEventId, employerCallBack) {
  var eventList = server.models.EventStudentList;
  eventList.find({'where': {'employerEventId': employerEventId}}, function(err, employerRes) {
    if (err) {
      errorResponse('error occured', employerCallBack);
    } else if (employerRes == 0) {
      errorResponse('there was no event with that employer id', employerCallBack);
    } else {
      var employerDrive = server.models.EmployerDriveCampuses;
      employerDrive.findOne({'where': {'employerEventId': employerEventId}}, function(err, response) {
        if (err) {
          errorResponse('error occured', employerCallBack);
        } else {
          var campus = server.models.Campus;
          campus.findOne({'where': {'campusId': response.campusId}}, function(err, campusResponse) {
            if (err) {
              errorResponse('error occured', employerCallBack);
            } else {
              var employerEvent = server.models.EmployerEvent;
              employerEvent.findById(employerEventId, function(err, employerResponse) {
                if (err) {
                  errorResponse('error occured', employerCallBack);
                } else {
                  var company = server.models.Company;
                  company.findById(employerResponse.companyId, function(err, companyResponse) {
                    if (err) {
                      errorResponse('error occured', employerCallBack);
                    } else {
                      var data = {};
                      data.campusId = campusResponse.campusId;
                      data.campusName = campusResponse.name;
                      data.eventName = employerResponse.eventName;
                      data.eventId = employerResponse.empEventId;
                      data.companyId = companyResponse.companyId;
                      data.companyName = companyResponse.name;
                      getStudentData(data, function(err, studentResponse) {
                        if (err) {
                          errorResponse('there was an error', employerCallBack);
                        } else {
                          employerCallBack(null, studentResponse);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};
  /**
 *exportDetails- To export to csv by taking campusEventId as an input
 *@constructor
 * @param {number} campusEventId - unique id for each and every event done by campus
 * @param {function} campusCallback - deals with response
 */
var exportDetails = function(campusEventId, campusCallback) {
  var campusEvent = server.models.CampusEvent;
  campusEvent.findById(campusEventId, function(err, campusEventResponse) {
    if (err) {
      errorResponse('there was an error', campusCallback);
    } else {
      var campus = server.models.Campus;
      campus.findById(campusEventResponse.campusId, function(err, campusDetails) {
        if (err) {
          errorResponse('there was an error', campusCallback);
        } else {
          var company = server.models.Company;
          company.findById(campusEventResponse.companyId, function(err, companyDetails) {
            if (err) {
              errorResponse('there was no company id in that event', campusCallback);
            } else {
              var data = {};
              data.campusId = campusDetails.campusId;
              data.campusName = campusDetails.name;
              data.eventName = campusEventResponse.eventName;
              data.eventId = campusEventResponse.employerEventId;
              data.companyId = companyDetails.companyId;
              data.companyName = companyDetails.name;
              getStudentData(data, function(err, studentResponse) {
                if (err) {
                  errorResponse('there was an error', campusCallback);
                } else {
                  campusCallback(null, studentResponse);
                }
              });
            }
          });
        }
      });
    }
  });
};
exports.exportDetails = exportDetails;
exports.exportData = exportData;
  // var createCsv = function(data, studentDet, callBack) {
  //   var async = require('async');
  //   async.map(studentDet, filterDetails, function(err, res) {
  //     if (err) {
  //       errorResponse('there was an error', callBack);
  //     } else {
  //       console.log('this is an response', res);
  //       callBack(null, res);
  //     }
  //   });
  // };
  // var filterDetails = function(data, studentDataCb) {
  //   var student = server.models.Student;
  //   var unregStudent = server.models.UnregisterCampusStudent;
  //   if (data.registrationInd == 'y' || data.registrationInd == 'Y') {
  //     dataToExport.push(data);
  //   } else {
  //     unregStudent.findById(data.studentId, function(err, unregStudentResponse) {
  //       if (err) {
  //         errorResponse('there was an error in findint he dtails', studentDataCb);
  //       } else {
  //         unregStudentData.push(unregStudentResponse);
  //       }
  //       finaldata = dataToExport.concat(unregStudentData);
  //       console.log(finaldata);
  //       studentDataCb(null, finaldata);
  //     });
  //   }
  // };
//=====================================================
// var exportDetails = function(campusId, callBack) {
//   var eventList = server.models.EventStudentList;
//   var company = server.models.Company;
//   var employerEvent = server.models.EmployerEvent;
//   var student = server.models.Student;
//   var unregStudent = server.models.UnregisterCampusStudent;
//   company.findById(companyId, function(err, companyRes) {
//     if (err) {

//     } else {
//       companyName = companyRes.name;
//       console.log(companyRes.name);
//     }
//   });
//   employerEvent.findById(employerEventId, function(err, employerResponse) {
//     if (err) {

//     } else {
//       eventName = employerResponse.eventName;
//       console.log(employerResponse.eventName);
//     }
//   });
//   eventList.find({'where': {'companyId': companyId,
//                     'employerEventId': employerEventId}},
//   function(err, response) {
//     if (err) {
//       errorResponse('there was an error', callBack);
//     } else {
//       response.map(function(studentDetails) {
//         console.log(studentDetails.studentId);
//         student.findById(studentDetails.studentId, function(err, studentDetailsResponse) {
//           if (err) {
//             console.log('err');
//           } else if (studentDetailsResponse == null) {
//             unregStudent.findById(studentDetails.studentId, function(err, unregResponse) {
//               if (err) {
//                 console.log('err');
//               } else {
//                 var data = {};
//                 data.companyName = companyName;
//                 data.companyId = companyId;
//                 data.eventName = eventName;
//                 data.employerEventId = employerEventId;
//                 data.studentId = unregResponse.unregStudentId;
//                 data.firstName = unregResponse.firstName;
//                 data.middleName = unregResponse.middleName;
//                 data.lastName = unregResponse.lastName;
//                 data.emailId = unregResponse.emailId;
//                 unregStudentData.push(data);
//                 console.log(unregResponse);
//               }
//             });
//           } else {
//             var data = {};
//             data.companyName = companyName;
//             data.companyId = companyId;
//             data.eventName = eventName;
//             data.employerEventId = employerEventId;
//             data.studentId = studentDetailsResponse.unregStudentId;
//             data.firstName = studentDetailsResponse.firstName;
//             data.middleName = studentDetailsResponse.middleName;
//             data.lastName = studentDetailsResponse.lastName;
//             data.emailId = studentDetailsResponse.emailId;
//             dataToExport.push(data);
//             console.log(studentDetailsResponse);
//           }
//           finaldata = dataToExport.concat(unregStudentData);
//           convertToCsv(finaldata);
//         });
//       });
//     }
//   });
// };
