  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var contactValidation = require('../commonValidation/contactValidation');
  var updateContact = require('../commonValidation/updateContact');
  var allmethods = require('../commonValidation/allmethods');
  var OrganizationContact = server.models.OrganizationContact;
  var updateContactService = require('../commonValidation/update-service-contact');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *updateOrgContact-function deals with updating the Org contact
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function updateOrgContact(contactData, cb) {
    if (contactData.companyId && contactData.organizationId && contactData.contactId) {
      //validating the input
      OrganizationContact.findOne({
        'where': {
          'and': [{
            'companyId': contactData.companyId,
          }, {
            'organizationId': contactData.organizationId,
          }, {
            'contactId': contactData.contactId,
          }],
        },
      }, function(err, orgList) {
        if (err) {
          cb(err, contactData);
        } else if (orgList) {
          updateContactService.updateContactService(contactData, function(error, info) {
            if (error) {
              logger.error('error while updating organization contact');
              cb(error, contactData);
            } else {
              logger.info('organization address updated successfully');
              cb(null, info);
            }
          });
        } else {
          //throws error when invalid contatcId or organizationId or companyId
          throwError('Invalid contactId or companyId or organizationId ', cb);
          logger.error('Invalid contactId or companyId or organizationId');
        }
      });
    } else {
      throwError('companyId , organizationId and contactId are required', cb);
      logger.error('companyId , organizationId and contactId are required');
    }
  }
  exports.updateOrgContact = updateOrgContact;
