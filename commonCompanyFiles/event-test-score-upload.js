'use strict';
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var server = require('../server/server');
var async = require('async');
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;

function createMassUpload(fileInput, inputData, callBc) {
  async.map(fileInput, validateInput, function(validateErr, validateOutput) {
    if (validateErr) {
      throwError(validateErr, callBc);
    } else {
      var inputArray = cleanArray(fileInput);
      if (inputArray.length > 0) {
        async.map(inputArray, getStudentId, getStudentCB);

        function getStudentId(obj, studentCB) {
          var scoraUser = server.models.ScoraUser;
          var eventTestScoreWork = server.models.EventTestScoreWork;
          scoraUser.findOne({
            'where': {
              'email': obj.studentEmail,
            },
          }, function(userErr, userOutput) {
            if (userErr) {
              obj.error = 'Invalid Email';
              eventTestScoreWork.create(obj, function(testErr, testOutput) {
                studentCB(null, null);
              });
            } else if (userOutput) {
              obj.userId = userOutput.id;
              studentCB(null, obj);
            } else {
              obj.error = 'Invalid Email';
              eventTestScoreWork.create(obj, function(testErr, testOutput) {
                studentCB(null, null);
              });
            }
          });
        }

        function getStudentCB(studentError, userInfo) {
          var userResponse = cleanArray(userInfo);
          if (userResponse.length > 0) {
            async.map(userResponse, validateStudent, validateStudentCB);
          } else {
            throwError('Invalid File', callBc);
          }
        }

        function validateStudent(object, stdCB) {
          var student = server.models.Student;
          var eventTestScoreWork = server.models.EventTestScoreWork;
          student.findOne({
            'where': {
              'id': object.userId,
            },
          }, function(stdErr, studentOut) {
            if (stdErr) {
              object.error = 'Invalid Student Id';
              eventTestScoreWork.create(object, function(testErr, testOutput) {
                stdCB(null, null);
              });
            } else if (studentOut) {
              object.studentId = studentOut.studentId;
              stdCB(null, object);
            } else {
              object.error = 'Invalid Student Id';
              eventTestScoreWork.create(object, function(testErr, testOutput) {
                stdCB(null, null);
              });
            }
          });
        }

        function validateStudentCB(error, studentInfo) {
          var studentResponse = cleanArray(studentInfo);
          async.map(studentResponse, validate, validationCB);
        }

        function validate(obj, validateCallBC) {
          var eventTestScoreWork = server.models.EventTestScoreWork;
          if (obj.score > 0 && obj.maxScore > 0 && (parseInt(obj.maxScore) > parseInt(obj.score))) {
            validateCallBC(null, obj);
          } else {
            obj.error = 'Validation Failed';
            eventTestScoreWork.create(obj, function(testErr, testOutput) {
              validateCallBC(null, null);
            });
          }
        }

        function validationCB(err, validateInfo) {
          var validateResponse = cleanArray(validateInfo);
          var eventTestScore = server.models.EventTestScore;
          var eventTestScoreWork = server.models.EventTestScoreWork;
          // var readError = require('../commonValidation/read-error-into-file').readErrorRecords;
          var createCompanyLog = require('./create-company-log').createCompanyLog;
          var inputFilterObject = {};
          inputFilterObject['employerEventId'] = inputData.empEventId;
          deleteSingleEntityDetails(inputFilterObject, eventTestScore, function(error, destroy) {
            eventTestScore.create(validateResponse, function(createErr, createOutput) {
              if (createErr) {
                throwError(createErr, callBc);
              } else {
                eventTestScoreWork.find({}, function(failedErr, failedOutput) {
                  readErrorRecords(failedOutput, inputData, function(readError, readResponse) {
                    var logInput = {
                      'compUploadTypeValueId': inputData.compUploadTypeValueId,
                      'companyId': inputData.companyId,
                      'container': inputData.fileDetails.container,
                      'name': inputData.fileDetails.name,
                      'originalFileName': inputData.fileDetails.originalFileName,
                      'totalNoRecs': fileInput.length,
                      'noFailRecs': cleanArray(failedOutput).length,
                      'noSuccessRecs': createOutput.length,
                    };
                    createCompanyLog(logInput, function(logError, logOutput) {
                      eventTestScoreWork.destroyAll({}, function(destroyError, destroyOutput) {
                        callBc(null, logOutput);
                      });
                    });
                  });
                });
              }
            });
          });
        }
      } else {
        throwError('Invalid File', callBc);
      }
    }
  });
  function validateInput(validateObj, validateCB) {
    var eventTestScoreValidation = require('../commonValidation/event-test-score-validation').validateJson;
    var eventTestScoreWork = server.models.EventTestScoreWork;
    eventTestScoreValidation(validateObj, function(validationErr, validationResponse) {
      if (validationErr) {
        validateObj.error = validationErr.err;
        eventTestScoreWork.create(validateObj, function(testErr, testOutput) {
          validateCB(null, null);
        });
      } else {
        validateCB(null, validateObj);
      }
    });
  }

  function readErrorRecords(finalArray, stDataa, cb) {
    var ErrorArray = [];
    for (var i = 0; i < finalArray.length; i++) {
      var obj = {
        'rowNumber': finalArray[i].rowNumber,
        'testName': finalArray[i].testName,
        'testVendorName': finalArray[i].testVendorName,
        'description': finalArray[i].description,
        'studentEmail': finalArray[i].studentEmail,
        'error': finalArray[i].error,
        'score': finalArray[i].score,
        'maxScore': finalArray[i].maxScore,
        'companyId': finalArray[i].companyId,
      };
      ErrorArray.push(obj);
    }
    var fs = require('fs');
    var csv = require('fast-csv');
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    var errorCsv = './attachments/' + container + '/' + name;
    // console.log('errorCSV', errorCsv);
    var ws = fs.createWriteStream(errorCsv);
    csv
      .writeToPath(errorCsv, ErrorArray, {
        headers: true,
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, 'success');
  }
}
exports.createMassUpload = createMassUpload;
