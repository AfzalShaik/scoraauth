'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Employer campus list hdr
describe(' Employer Campus List HDR Cases ;', function() {
  it('Employer Campus List HDR POST returns 200', function(done) {
    var input = [{
      'companyId': 1026,
      'jobRoleId': 3333,
      'listName': 'category Name',
      'description': 'description of employer campus list',
      'compApprovalStatusValueId': 336,
      'createDatetime': '2017-08-11T11:04:18.000Z',

      'createUserId': 1,

    }];

    var url = 'http://localhost:3000/api/EmployerCampusListHdrs';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //Campus List hdr item put
  it('Employer Campus List HDR PUT returns 200', function(done) {
    var input = {
      'listId': 4444,
      'companyId': 1026,
      'jobRoleId': 3333,
      'listName': 'category Name',
      'description': 'description of employer campus list',
      'compApprovalStatusValueId': 336,
      'createDatetime': '2017-08-11T11:04:18.000Z',
      'updateDatetime': '2017-08-18T11:57:20.532Z',
      'createUserId': 1,
      'updateUserId': 1,

    };

    var url = 'http://localhost:3000/api/EmployerCampusListHdrs/updateEmployerCampusListHdr';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Campus List hdr get
  it('Employer Campus List HDR GET returns 200', function(done) {
    var input = {
      'compPackageItemId': 7767,
      'compPackageId': 2222,
      'companyId': 1026,
    };

    var url = 'http://localhost:3000/api/EmployerCampusListHdrs/getEmployerCampusListHdrDetails?listId=4444';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
