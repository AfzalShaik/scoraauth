'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//Employer campus list dtl
describe(' Employer Campus List dtl Cases ;', function() {
  it('Employer Campus List dtl POST returns 200', function(done) {
    var input = [{

      'listId': 4444,
      'companyId': 1026,
      'campusId': 388589389,
      'startDate': '2017-08-11T00:00:00.000Z',
      'endDate': '2017-08-11T00:00:00.000Z',
      'endReason': 'end reason for program',
      'createDatetime': '2017-08-11T13:04:55.000Z',
      'updateDatetime': '2017-08-11T13:04:55.000Z',
      'createUserId': 1,
    }];
    var url = 'http://localhost:3000/api/EmployerCampusListDtls';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //Campus List dtl item put
  it('Employer Campus List dtl PUT returns 200', function(done) {
    var input = {
      'listCampusId': 77877,
      'listId': 4444,
      'companyId': 1006,
      'campusId': 400787676,
      'startDate': '2017-08-16T00:00:00.000Z',
      'endDate': '2017-08-16T00:00:00.000Z',
      'endReason': 'endReason',
      'createDatetime': '2017-08-16T11:39:35.000Z',
      'updateDatetime': '2017-08-18T12:02:52.987Z',
      'createUserId': 1,
      'updateUserId': 1,

    };
    var url = 'http://localhost:3000/api/EmployerCampusListDtls/updateEmployerCampusListDtl';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //Campus List dtl get
  it('Employer Campus List dtl GET returns 200', function(done) {
    var input = {
      'compPackageItemId': 7767,
      'compPackageId': 2222,
      'companyId': 1026,
    };

    var url = 'http://localhost:3000/api/EmployerCampusListDtls/getEmployerCampusListDtl?listCampusId=7777';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
