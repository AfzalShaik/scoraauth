var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);
//Program contact
describe(' Program Contact Cases ;', function () {
//program post method
  it('Program Contact POST returns 200', function (done) {
    var input =
    {
  "programId": 101014,
    "campusId": 959816197,
"departmentId":9999,
   "contactTypeValueId": 14,
  "contactInfo": "string",
  "primaryInd": "N",
  "createDatetime": "2017-08-08T07:06:45.212Z",
  "updateDatetime": "2017-08-08T07:06:45.212Z",
  "createUserId": 1,
  "updateUserId": 1

}

    var url = "http://localhost:3000/api/ProgramContacts/createProgramContact";
    var request = require('request');

    request({
    url: url,
    method: "POST",
    json: true,
    body: input

    }, function(err, response, body) {
    if(err) { return err; }

      expect(response.statusCode).to.equal(200);

    done();
    });

  })

  //program put method
    it('Program Contact PUT returns 200', function (done) {
      var input =
      {
"contactId":625,
"contactInfo":"contact information",
"primaryInd":"N",
"programId":101014,
"campusId":959816197,
"departmentId":9999
}

      var url = "http://localhost:3000/api/ProgramContacts/updateProgramContact";
      var request = require('request');

      request({
      url: url,
      method: "PUT",
      json: true,
      body: input

      }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
      });

    })

    //program get method
      it('Program Contact GET returns 200', function (done) {
        var input =
        {"programId": 101014,
        "contactId": 625}


        var url = "http://localhost:3000/api/ProgramContacts/getProgramContact?programId=101014&contactId=625";
        var request = require('request');

        request({
        url: url,
        method: "GET",
        json: true

        }, function(err, response, body) {
        if(err) { return err; }

          expect(response.statusCode).to.equal(200);

        done();
        });

      })
})
