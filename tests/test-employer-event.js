'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);

//employer event
describe(' employer event Cases ;', function() {
  it('employer event POST returns 200', function(done) {
    var input = {
      'empDriveId': 65656,
      'companyId': 1007,
      'cgpa': 65,
      'eventName': 'emp event name',
      'eventTypeValueId': 309,
      'eventStatusValueId': 313,
      'scheduledDate': '2017-08-21T00:00:00.000Z',
      'scheduledStartTime': '09:19:41',
      'duration': 67,
      'hostName': 'host name',
      'hostContact': 'host contact',
      'addressLine1': 'add1',
      'addressLine2': 'ad2',
      'addressLine3': 'ad3',
      'cityId': 1,
      'stateCode': 'AN',
      'countryCode': 'IN',
      'postalCode': '1',
      'createUserId': 1,
      'eventRequirement': 'event requirement',
    };

    var url = 'http://localhost:3000/api/EmployerEvents';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
  //employer event  put
  it('employer event PUT returns 200', function(done) {
    var input = {
      'empEventId': 75675,
      'empDriveId': 65656,
      'companyId': 1007,
      'cgpa': 98,
      'eventName': 'emp event name',
      'eventTypeValueId': 310,
      'eventStatusValueId': 313,
      'scheduledDate': '2017-08-21T00:00:00.000Z',
      'scheduledStartTime': '09:19:41',
      'duration': 67,
      'hostName': 'host name',
      'hostContact': 'host contact',
      'addressLine1': 'add1',
      'addressLine2': 'ad2',
      'addressLine3': 'ad3',
      'cityId': 1,
      'stateCode': 'AN',
      'countryCode': 'IN',
      'postalCode': '1',
      'createDatetime': '2017-08-21T09:19:41.000Z',
      'updateDatetime': '2017-08-21T09:19:41.000Z',
      'createUserId': 1,
      'updateUserId': 1,
      'eventRequirement': 'event requirement',
    };

    var url = 'http://localhost:3000/api/EmployerEvents/updateEmployerEvent';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('response.statusCode', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });

  //employer event get
  it('employer event GET returns 200', function(done) {
    var url = 'http://localhost:3000/api/EmployerEvents/getEmployerEventDetails?empEventId=75676';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'GET',
      json: true,
    }, function(err, response, body) {
      if (err) {
        return err;
      }
      console.log('body', response.statusCode);
      expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
