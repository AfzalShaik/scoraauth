var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require("chai").expect;
var request = require("request");
chai.use(chaiHttp);

//Department
describe(' Department Cases ;', function () {

  it('departmentPost returns 200', function (done) {

      var input =
      {
    "campusId": 388589390,
    "shortName": "ECE",
    "name": "ELECTRONICS",
    "description": "ECE",
    "createUserId": 100
  }

    var url = "http://localhost:3000/api/Departments";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

      // console.log('Response:');
      if(response.statusCode==200){
        expect(response.statusCode).to.equal(200);
          // console.log("----------------output department:----------------------- ",body.data);
      }else if(response.statusCode==422){
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(422);
      }
      else{
          expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });

  // department get method
  it('departmentGet returns 200', function (done) {


    var url = "http://localhost:3000/api/Departments/getDepartment?departmentId=9999&campusId=959816197";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "GET",
      json: true
    }, function(err, response, body) {
      if(err) { return err; }

      // console.log('Response:');
      if(response.statusCode==200){
        expect(response.statusCode).to.equal(200);
          // console.log("----------------output department:----------------------- ",body.data);
      }else if(response.statusCode==422){
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(422);
      }
      else{
          expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });
//department update negative scenerio
  it('departmentPost returns 422 for invalid input', function (done) {

      var input =
      {
    "campusId": 3885893903,
    "shortName": "ECE",
    "name": "ELECTRONICS",
    "description": "ECE",
    "createUserId": 100
  }

    var url = "http://localhost:3000/api/Departments";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "POST",
      json: true,
      body: input

    }, function(err, response, body) {
      if(err) { return err; }

      // console.log('Response:');
      if(response.statusCode==200){
        expect(response.statusCode).to.equal(200);
          // console.log("----------------output department:----------------------- ",body.data);
      }else if(response.statusCode==422){
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(422);
      }
      else{
          expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });

  // department Delete method
  it('DepartmentDelete returns 401', function (done) {


    var url = "http://localhost:3000/api/Departments/9999";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "DELETE",
      json: true
    }, function(err, response, body) {
      if(err) { return err; }

      // console.log('Response:');
      if(response.statusCode==401){
        expect(response.statusCode).to.equal(401);
          // console.log("----------------output department:----------------------- ",body.data);
      }
      done();
    });
  });

  // department Contact method
  it('DepartmentContact POST returns 200', function (done) {
    var input = {

  "contactTypeValueId": 14,
"departmentId":9999,
"campusId":959816205,
  "contactInfo": "string",
  "primaryInd": "Y",
  "createDatetime": "2017-08-02T06:21:23.052Z",
  "updateDatetime": "2017-08-02T06:21:23.052Z",
  "createUserId": 1,
  "updateUserId": 1
}

    var url = "http://localhost:3000/api/Contacts";
    var request = require('request');
    request({
      url: url,
      method: "POST",
      json: true,
      body: input
    }, function(err, response, body) {
      if(err) { return err; }
        expect(response.statusCode).to.equal(200);
      done();
    });
  });



  // department Contact method
  it('DepartmentContact PUT returns 200', function (done) {
    var input = {"departmentId":9999,
"campusId":959816205,
"contactId":506,
 "contactTypeValueId": 14,
"contactInfo":"contact information"

}


    var url = "http://localhost:3000/api/Contacts/UpdateDepartmentContact";
    var request = require('request');
    request({
      url: url,
      method: "PUT",
      json: true,
      body: input
    }, function(err, response, body) {
      if(err) { return err; }
        expect(response.statusCode).to.equal(200);
      done();
    });
  });
//department contact get method
  it('departmentContact Get returns 200', function (done) {


    var url = "http://localhost:3000/api/DepartmentContacts/getCampusDeptContact?departmentId=9999&campusId=959816205&contactId=506";
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: "GET",
      json: true
    }, function(err, response, body) {
      if(err) { return err; }

        expect(response.statusCode).to.equal(200);

      done();
    });
  });
});
