'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Department Update
describe(' Department Update Cases ;', function() {
  // UpdateDepartment case1: Success
  it('DepartmentUpdate returns 200', function(done) {
    var input = {
      'departmentId': 9999,
      'campusId': 959816197,
      'name': 'electronics and communication engineering iiit',
    };
    var url = 'http://localhost:3000/api/Departments/UpdateDepartment';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      // console.log('Response:');
      if (response.statusCode == 200) {
        expect(response.statusCode).to.equal(200);
        // console.log("----------------output department:----------------------- ",body.data);
      } else {
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });
  //UpdateDepartment case2 : invalid data should return 422
  it('DepartmentUpdate returns 422 for invalid data', function(done) {
    var input = {
      'departmentId': 99999,
      'campusId': 959816197,
      'name': 'electronics and communication engineering iiit',
    };
    var url = 'http://localhost:3000/api/Departments/UpdateDepartment';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      // console.log('Response:');
      if (response.statusCode == 422) {
        expect(response.statusCode).to.equal(422);
        // console.log("----------------output department:----------------------- ",body.data);
      } else {
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });

  //UpdateDepartment case2 : Missed Required Fields
  it('DepartmentUpdate returns 422 for missing required fields', function(done) {
    var input = {
      'campusId': 959816197,
      'name': 'electronics and communication engineering iiit',
    };
    var url = 'http://localhost:3000/api/Departments/UpdateDepartment';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      // console.log('Response:');
      if (response.statusCode == 422) {
        expect(response.statusCode).to.equal(422);
        // console.log("----------------output department:----------------------- ",body.data);
      } else {
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });

  //UpdateDepartment case2 : validation
  it('DepartmentUpdate returns 422 for validation', function(done) {
    var input = {
      'departmentId': 99999987654321097,
      'campusId': 959816197,
      'name': 'electronics and communication engineering iiit',
    };
    var url = 'http://localhost:3000/api/Departments/UpdateDepartment';
    var request = require('request');

    // console.log('Request:');
    // console.log(input);
    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) {
        return err;
      }

      // console.log('Response:');
      if (response.statusCode == 422) {
        expect(response.statusCode).to.equal(422);
        // console.log("----------------output department:----------------------- ",body.data);
      } else {
        // console.log("Department Update Method gives StatusCode: ",response.statusCode);
        expect(response.statusCode).to.equal(500);
      }
      done();
    });
  });
});
