'use strict';
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/server');
var should = chai.should();
var assert = require('assert');
var expect    = require('chai').expect;
var request = require('request');
chai.use(chaiHttp);
//Program
describe(' Assessment Cases ;', function() {
//program post method
// all valid data
  it('Assessment POST returns 200', function(done) {
    var input = {
      'enrollmentId': 1,
      'programUnitTypeValueId': 1,
      'subjectDetails': 'dbms',
      'score': 90,
      'highlights': 'good',
      'dataVerifiedInd': 'Y',
      'createDatetime': '2017-08-31T12:47:55.633Z',
      'updateDatetime': '2017-08-31T12:47:55.633Z',
      'createUserId': 2,
      'updateUserId': 2,
    };

    var url = 'http://localhost:3000/api/Assessments';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(200);

      done();
    });
  });
 // cahnging score data type
  it('Assessment POST returns 200', function(done) {
    var input = {
      'enrollmentId': 1,
      'programUnitTypeValueId': 1,
      'subjectDetails': 'dbms',
      'score': 'h',
      'highlights': 'good',
      'dataVerifiedInd': 'Y',
      'createDatetime': '2017-08-31T12:47:55.633Z',
      'updateDatetime': '2017-08-31T12:47:55.633Z',
      'createUserId': 2,
      'updateUserId': 2,
    };

    var url = 'http://localhost:3000/api/Assessments';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
  // trying to insert non valid enrollmentId
  it('Assessment POST returns 200', function(done) {
    var input = {
      'enrollmentId': 9,
      'programUnitTypeValueId': 1,
      'subjectDetails': 'dbms',
      'score': 90,
      'highlights': 'good',
      'dataVerifiedInd': 'Y',
      'createDatetime': '2017-08-31T12:47:55.633Z',
      'updateDatetime': '2017-08-31T12:47:55.633Z',
      'createUserId': 2,
      'updateUserId': 2,
    };

    var url = 'http://localhost:3000/api/Assessments';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// trying to insert assessment id which was auto generated
  it('Assessment POST returns 200', function(done) {
    var input = {
      'assessmentId': 400,
      'enrollmentId': 1,
      'programUnitTypeValueId': 1,
      'subjectDetails': 'dbms',
      'score': 90,
      'highlights': 'good',
      'dataVerifiedInd': 'Y',
      'createDatetime': '2017-08-31T12:47:55.633Z',
      'updateDatetime': '2017-08-31T12:47:55.633Z',
      'createUserId': 2,
      'updateUserId': 2,
    };

    var url = 'http://localhost:3000/api/Assessments';
    var request = require('request');

    request({
      url: url,
      method: 'POST',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }

      expect(response.statusCode).to.equal(500);

      done();
    });
  });
// get method
  it('Assessment  GET returns 200', function(done) {
    var input =
      {

        'assessmentId': 9,
      };

    var url = 'http://localhost:3000/api/Assessments';
    var request = require('request');

    request({
      url: url,
      method: 'GET',
      json: true,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  //update method with valid data
  it('Assessment  PUT returns 200', function(done) {
    var input =
      {

        'assessmentId': 9,
        'subjectDetails': 'stm',
        'score': 90,
        'highlights': 'very good',

      };

    var url = 'http://localhost:3000/api/Assessments/updateAssessmentDet';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(200);
      done();
    });
  });
  // update method test with in valid assessment id
  it('Assessment  PUT returns 200', function(done) {
    var input =
      {

        'assessmentId': 900,
        'subjectDetails': 'stm',
        'score': 90,
        'highlights': 'very good',

      };

    var url = 'http://localhost:3000/api/Assessments/updateAssessmentDet';
    var request = require('request');

    request({
      url: url,
      method: 'PUT',
      json: true,
      body: input,

    }, function(err, response, body) {
      if (err) { return err; }
      expect(response.statusCode).to.equal(422);
      done();
    });
  });
}
);

