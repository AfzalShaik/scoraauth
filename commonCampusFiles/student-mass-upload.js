'use strict';
var cleanArray = require('../commonValidation/common-mass-upload').cleanArray;
var studentMassJson = require('../common/models/STUDENTS_MASS_UPLOAD_WORK.json');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateJsonInput;
var server = require('../server/server');
var async = require('async');
var count = 0;

function createMassUpload(inputArray, stDataa, callBc) {
  async.map(inputArray, validateInput, function(error, response) {
    if (error) {
      throwError('Invalid DataTypes', callBc);
    } else {
      var validatedResponse = cleanArray(response);
      if (validatedResponse.length > 0) {
        async.map(validatedResponse, getEmailValid, function(uploadErr, workData) {
          if (uploadErr) {
            throwError(uploadErr, callBc);
          } else {
            var workDataArray = cleanArray(workData);
            if (workDataArray.length > 0) {
              async.map(workDataArray, getAdmissionNumberValid, function(adminErr, adminResponse) {
                var createStudent = require('./create-student-mass-upload').createStudent;
                var finalArray = cleanArray(adminResponse);
                createStudent(finalArray, stDataa, function(finalErr, finalOutput) {
                  if (finalErr) {
                    throwError(finalErr, callBc);
                  } else {
                    // callBc(null, finalOutput);
                    var studentsMassUploadWork = server.models.StudentsMassUploadWork;
                    studentsMassUploadWork.find({}, function(workErr, workResponse) {
                      if (workErr) {
                        studentsMassUploadWork.destroyAll({}, function(error, destroy) {
                          throwError(error, callBc);
                        });
                      } else {
                        var campusUploadLog = server.models.CampusUploadLog;
                        var details = stDataa.fileDetails;
                        var container = stDataa.fileDetails.container;
                        getfailureMass(workResponse, function(err, failedArray) {
                          var logInput = {
                            'campusUploadTypeValueId': stDataa.campusUploadTypeValueId,
                            'campusId': stDataa.campusId,
                            'inputParameters': 'string',
                            'createUserId': 1,
                            'createDatetime': new Date(),
                            'csvFileLocation': './attachments/' + container + '/' + details.name + '/' + details.originalFileName,
                            'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + details.name,
                            'totalNoRecs': inputArray.length,
                            'noFailRecs': workResponse.length,
                            'noSuccessRecs': cleanArray(finalOutput).length,
                          };
                          campusUploadLog.create(logInput, function(logErr, logOutput) {
                            studentsMassUploadWork.destroyAll({}, function(error, destroy) {
                              callBc(null, logOutput);
                            });
                          });
                        });
                      }
                    });
                  }
                });
              });
            } else {
              throwError('Invalid Records Found', callBc);
            }
          }
        });
      } else {
        var studentsMassUploadWork = server.models.StudentsMassUploadWork;
        studentsMassUploadWork.destroyAll({}, function(error, destroy) {
          throwError('Invalid File', callBc);
        });
      }
    }
  });

  function postIntoUploadWork(object, cb) {
    // var count = 0;
    var input = {};
    input = {
      'rowNumber': object.rowNumber,
      'firstName': object.firstName,
      'lastName': object.lastName,
      'email': object.email,
      'admissionNo': object.admissionNo,
      'startDate': object.startDate,
      'plannedCompletionDate': object.plannedCompletionDate,
      'skipUserInd': object.skipUserInd,
      'programId': stDataa.programId,
      'campusId': stDataa.campusId,
      'departmentId': stDataa.departmentId,
      'error': object.error,
    };
    // console.log(input);
    var studentsMassUploadWork = server.models.StudentsMassUploadWork;
    studentsMassUploadWork.create(input, function(err, uploadResponse) {
      if (err) {
        throwError(err, cb);
      } else {
        cb(null, uploadResponse);
      }
    });
  }
  exports.postIntoUploadWork = postIntoUploadWork;
  function getEmailValid(obj, callBack) {
    var scoraUser = server.models.ScoraUser;
    var student = server.models.Student;
    var enrollment = server.models.Enrollment;
    scoraUser.findOne({
      'where': {
        'email': obj.email,
      },
    }, function(err, userResult) {
      if (err) {
        obj.error = 'Email Already Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          callBack(null, null);
        });
      } else if (userResult) {
        obj.error = 'Email Already Exist';
        if (obj.skipUserInd == 'Y' || obj.skipUserInd == 'y') {
          student.findOne({
            'where': {
              'id': userResult.id,
            },
          }, function(studentErr, studentInfo) {
            if (studentErr) {
              obj.error = 'Email Already Exist';
              postIntoUploadWork(obj, function(error, errorResponse) {
                callBack(null, null);
              });
            } else if (studentInfo) {
              enrollment.findOne({
                'where': {
                  'and': [{
                    'programId': obj.programId,
                  }, {
                    'studentId': studentInfo.studentId,
                  }],
                },
              }, function(enrollErr, enrollInfo) {
                if (enrollErr) {
                  obj.error = 'Email Already Exist';
                  postIntoUploadWork(obj, function(error, errorResponse) {
                    callBack(null, null);
                  });
                } else if (enrollInfo) {
                  obj.error = 'Email Already Exist';
                  postIntoUploadWork(obj, function(error, errorResponse) {
                    callBack(null, null);
                  });
                } else {
                  obj.studentId = studentInfo.studentId;
                  callBack(null, obj);
                }
              });
            } else {
              obj.error = 'Email Already Exist';
              postIntoUploadWork(obj, function(error, errorResponse) {
                callBack(null, null);
              });
            }
          });
        } else {
          obj.error = 'Email Already Exist';
          postIntoUploadWork(obj, function(error, errorResponse) {
            callBack(null, null);
          });
        }
      } else {
        obj.error = 'no error';
        callBack(null, obj);
      }
    });
  }

  function getAdmissionNumberValid(obj, cBack) {
    var enrollment = server.models.Enrollment;
    enrollment.findOne({
      'where': {
        'and': [{
          'admissionNo': obj.admissionNo,
        },
        {
          'campusId': obj.campusId,
        },
        ],
      },
    }, function(err, adminResult) {
      if (err) {
        obj.error = 'Admission Number Already Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          cBack(null, null);
        });
      } else if (adminResult) {
        obj.error = 'Admission Number Already Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          cBack(null, null);
        });
      } else {
        obj.error = 'no error';
        cBack(null, obj);
      }
    });
  }

  function getfailureMass(failedArray, cb) {
    var finalArray = [];
    for (var i = 0; i < failedArray.length; i++) {
      var obj = {
        'rowNumber': failedArray[i].rowNumber,
        'firstName': failedArray[i].firstName,
        'lastName': failedArray[i].lastName,
        'email': failedArray[i].email,
        'admissionNo': failedArray[i].admissionNo,
        'error': failedArray[i].error,
        'skipUserInd': failedArray[i].skipUserInd,
        'startDate': failedArray[i].startDate,
        'planedCompletionDate': failedArray[i].planedCompletionDate,
      };
      finalArray.push(obj);
    }
    var fs = require('fs');
    var csv = require('fast-csv');
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    var errorCsv = './attachments/' + container + '/' + name;
    var ws = fs.createWriteStream(errorCsv);
    csv
      .writeToPath(errorCsv, finalArray, {
        headers: true
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, 'success');
  }

  function createEnrollment(obj, studentResp, enrollCallBck) {
    var enrollment = server.models.Enrollment;
    var enrollmentObj = {
      'studentId': studentResp.studentId,
      'programId': obj.programId,
      'admissionNo': obj.admissionNo,
      'startDate': obj.startDate,
      'planedCompletionDate': obj.planedCompletionDate,
      'dataVerifiedInd': 'Y',
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
      'campusId': obj.campusId,
    };
    enrollment.create(enrollmentObj, function(enrollError, enrollInfo) {
      enrollCallBck(null, enrollInfo);
    });
  }

  function validateInput(validateObj, validateCB) {
    var validateMassUpload = require('../commonValidation/student-mass-upload-validation').validateJson;
    validateMassUpload(validateObj, function(validationErr, validationResponse) {
      if (validationErr) {
        validateObj.error = validationErr.err;
        postIntoUploadWork(validateObj, function(postErr, postResp) {
          validateCB(null, null);
        });
      } else {
        validateCB(null, validateObj);
      }
    });
  }
}
exports.createMassUpload = createMassUpload;
