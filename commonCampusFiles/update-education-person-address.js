  'use strict';
  var server = require('../server/server');
  var logger = require('../server/boot/lib/logger');
  var validation = require('../commonValidation/addressValidation');
  var updateAddress = require('../commonValidation/updateAddress');
  var lookupMethods = require('../commonValidation/lookupMethods');
  var updateAddService = require('../commonValidation/update-service-address');
  var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  var EducationPersonAddress = server.models.EducationPersonAddress;
  /**
   *educationPersonAddressUpdate-function deals with updating the education person address
   *@constructor
   * @param {object} eduPersonData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
  function educationPersonAddressUpdate(eduPersonData, cb) {
    if (eduPersonData) {
      if (eduPersonData.campusId && eduPersonData.educationPersonId && eduPersonData.addressId) {
        EducationPersonAddress.findOne({
          'where': {
            'and': [{
              'campusId': eduPersonData.campusId,
            }, {
              'addressId': eduPersonData.addressId,
            }, {
              'educationPersonId': eduPersonData.educationPersonId,
            }],
          },
        }, function(err, EducationPersonList) {
          if (err) {
            //throws error
            cb(err, eduPersonData);
          } else if (EducationPersonList) {
            logger.info('record found for given campusId, addressid and educationpersonId');
            //updating record in address table with addressId
            updateAddService.updateAddresService(eduPersonData, function(err, eduPersonAddressList) {
              if (err) {
                logger.error('error while performing update education person address updation');
                cb(err, eduPersonData);
              } else {
                logger.info('education person address updated successfully');
                cb(null, eduPersonAddressList);
              }
            });
          } else {
            //throws error incase of invalid address_id or campusId
            throwError('Invalid address_id or campusId or educationPersonId', cb);
            logger.error('Invalid address_id or campusId or educationPersonId');
          }
        });
      } else {
        logger.error('campusId and educationPersonId and addressId is required');
        throwError('campusId and educationPersonId and addressId is required ', cb);
      }
    } else {
      logger.error("Input can't be Null");
      throwError('Input is Null: ', cb);
    }
  }

  exports.educationPersonAddressUpdate = educationPersonAddressUpdate;
