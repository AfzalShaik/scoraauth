'use strict';
var organizationJson = require('../commonValidation/all-models-json.js');
var organizationValidation = require('../commonValidation/all-models-validation');
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var organization = server.models.Organization;
var validValue;
  /**
   *updateOrganizationProfile-function deals with updating the organization profile
   *@constructor
   * @param {object} orgdata - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateOrganizationProfile(orgdata, cb) {
  if (orgdata) {
    if (orgdata.companyId && orgdata.organizationId) {
      organizationValidation.validateModelsJson(orgdata, organizationJson, function(err, validValue) {
        validValue = validValue;
        if (validValue == false) {
          throwError(err, cb);
        } else {
          //checking record for provided input
          organization.findOne({
            'where': {
              'and': [{
                'companyId': orgdata.companyId,
              }, {
                'organizationId': orgdata.organizationId,
              }],
            },
          }, function(err, orgList) {
            if (err) {
              //throws error
              cb(err, orgList);
            } else if (orgList) {
              //if record found it will the  updateAttributes
              orgList.updateAttributes(orgdata, function(err, info) {
                if (err) {
                  cb(err, info);
                } else {
                  //making requestStatus is true;
                  logger.info('Company Organization Updated Successfully');
                  info['requestStatus'] = true;
                  info['updateDatetime'] = new Date();
                  cb(null, info);
                }
              });
            } else {
              //throws error incase of invalid address_id or companyId
              var error = new Error('Invalid organizationId or companyId');
              logger.error('Invalid organizationId or companyId');
              error.statusCode = 422;
              error.requestStatus = false;
              cb(error, orgdata);
            }
          });
        }
      });
    } else {
      throwError('companyId or organizationId cannot be blank ', cb);
    }
  } else {
    //throws error if input is null
    throwError('Input Cannot be Blank: ', cb);
    logger.error("Input Can't be Null");
  }
};
exports.updateOrganizationProfile = updateOrganizationProfile;
