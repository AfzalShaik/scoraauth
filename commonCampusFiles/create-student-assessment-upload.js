'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;

function createAssessment(assessmentData, assessmentInput, cb) {
  async.map(assessmentData, validateInput, function(error, response) {
    if (error) {
      throwError('Invalid DataTypes', callBc);
    } else {
      var assessmentArray = cleanArray(response);
      if (assessmentArray.length > 0) {
        async.map(assessmentArray, getInfo, function(scoreErr, scoreResp) {
          if (scoreErr) {
            throwError(scoreErr, cb);
          } else {
            var scoreResponse = cleanArray(scoreResp);
            if (scoreResponse.length > 0) {
              async.map(scoreResponse, checkScore, function(err, scoreOutput) {
                var scoreArray = cleanArray(scoreOutput);
                async.map(scoreArray, createOrUpdate, function(createErr, responseCreated) {
                  var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
                  studentsAssessmentUploadWork.find({}, function(workErr, workResponse) {
                    if (workErr) {
                      studentsAssessmentUploadWork.destroyAll({}, function(error, destroy) {
                        throwError(error, cb);
                      });
                    } else {
                      var campusUploadLog = server.models.CampusUploadLog;
                      var details = assessmentInput.fileDetails;
                      var container = assessmentInput.fileDetails.container;
                      getfailureMass(workResponse, function(err, failedArray) {
                        var logInput = {
                          'campusUploadTypeValueId': assessmentInput.campusUploadTypeValueId,
                          'campusId': assessmentInput.campusId,
                          'inputParameters': 'string',
                          'createUserId': 1,
                          'createDatetime': new Date(),
                          'csvFileLocation': './attachments/' + container + '/' + details.name + '/' + details.originalFileName,
                          'errorFileLocation': './attachments/' + container + '/' + 'download' + '/' + details.name,
                          'totalNoRecs': assessmentData.length,
                          'noFailRecs': workResponse.length,
                          'noSuccessRecs': cleanArray(responseCreated).length,
                        };
                        campusUploadLog.create(logInput, function(logErr, logOutput) {
                          studentsAssessmentUploadWork.destroyAll({}, function(error, destroy) {
                            cb(null, logOutput);
                          });
                        });
                      });
                    }
                  });
                });
              });
            } else {
              throwError('Invalid File', cb);
            }
          }
        });
      } else {
        throwError('Invalid File', cb);
      }
    }
  });

  function getInfo(obj, callback) {
    var enrollmentModel = server.models.Enrollment;
    enrollmentModel.findOne({
      'where': {
        'and': [{
          'admissionNo': obj.admissionNo,
        },
        {
          'campusId': assessmentInput.campusId,
        },
        ],
      },
    }, function(error, findResp) {
      if (error) {
        createAssessmentUpload(obj, error, function(err, errOutput) {
          callback(null, null);
        });
      } else if (findResp) {
        obj.enrollmentId = findResp.enrollmentId;
        callback(null, obj);
      } else {
        createAssessmentUpload(obj, 'Invalid Admission Number', function(err, errOutput) {
          callback(null, null);
        });
      }
    });
  }

  function createAssessmentUpload(object, error, callBc) {
    var studentsAssessmentUploadWork = server.models.StudentsAssessmentUploadWork;
    var input = {
      'rowNumber': object.rowNumber,
      'admissionNo': object.admissionNo,
      'score': object.score,
      'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
      'departmentId': assessmentInput.departmentId,
      'programId': assessmentInput.programId,
      'error': error,
    };
    studentsAssessmentUploadWork.create(input, function(err, out) {
      if (err) {
        throwError(err, callBc);
      } else {
        callBc(null, out);
      }
    });
  }

  function checkScore(obj, callBack) {
    if (obj.score >= 0) {
      callBack(null, obj);
    } else {
      createAssessmentUpload(obj, 'Invalid Score', function(err, errOutput) {
        callBack(null, null);
      });
    }
  }

  function createOrUpdate(object, createCB) {
    var assessment = server.models.Assessment;
    assessment.findOne({
      'where': {
        'and': [{
          'enrollmentId': object.enrollmentId,
        }, {
          'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
        }],
      },
    }, function(error, response) {
      if (response) {
        var updateInput = response;
        updateInput.score = object.score;
        updateInput.updateDatetime = new Date();
        response.updateAttributes(updateInput, function(updateErr, updateResponse) {
          createCB(null, updateResponse);
        });
      } else {
        var createInput = {
          'enrollmentId': object.enrollmentId,
          'programUnitTypeValueId': assessmentInput.programUnitTypeValueId,
          'subjectDetails': null,
          'score': object.score,
          'highlights': null,
          'dataVerifiedInd': 'Y',
          'createDatetime': new Date(),
          'updateDatetime': new Date(),
          'createUserId': 1,
          'updateUserId': 1,
        };
        assessment.create(createInput, function(createError, createResponse) {
          if (createError) {
            createCB(null, null);
          } else {
            createCB(null, createResponse);
          }
        });
      }
    });
  }
  function getfailureMass(failedArray, cb) {
    var finalArray = [];
    for (var i = 0; i < failedArray.length; i++) {
      var obj = {
        'rowNumber': failedArray[i].rowNumber,
        'admissionNo': failedArray[i].admissionNo,
        'score': failedArray[i].score,
        'departmentId': failedArray[i].departmentId,
        'programUnitTypeValueId': failedArray[i].programUnitTypeValueId,
        'error': failedArray[i].error,
        'programId': failedArray[i].programId,
      };
      finalArray.push(obj);
    }
    var name = assessmentInput.fileDetails.name;
    var container = assessmentInput.fileDetails.container;
    var errorCsv = './attachments/' + container + '/' + name;
    var fs = require('fs');
    var csv = require('fast-csv');
    var ws = fs.createWriteStream(errorCsv);
    csv
    .writeToPath(errorCsv, finalArray, {headers: true})
    .on('finish', function() {
      // console.log('done!');
    });
    cb(null, 'success');
  }
  function validateInput(validateObj, validateCB) {
    var validateMassUpload = require('../commonValidation/student-assessment-upload-validation').validateJson;
    validateMassUpload(validateObj, function(validationErr, validationResponse) {
      if (validationErr) {
        createAssessmentUpload(validateObj, validateObj.error, function(postErr, postResp) {
          validateCB(null, null);
        });
      } else {
        validateCB(null, validateObj);
      }
    });
  }
}
exports.createAssessment = createAssessment;
