'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var contactValidation = require('../commonValidation/contactValidation');
var updateContact = require('../commonValidation/updateContact');
var allmethods = require('../commonValidation/allmethods');
var updateContactService = require('../commonValidation/update-service-contact');
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
  /**
   *programContactUpdate-function deals with updating the program contact detalis
   *@constructor
   * @param {object} contactData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function programContactUpdate(contactData, cb) {
  if (contactData.programId && contactData.contactId && contactData.campusId) {
    //validating the input
    var ProgramContact = server.models.ProgramContact;
    ProgramContact.findOne({
      'where': {
        'and': [{
          'campusId': contactData.campusId,
        }, {
          'programId': contactData.programId,
        }, {
          'contactId': contactData.contactId,
        }],
      },
    }, function(err, employerPersonList) {
      if (err) {
        cb(err, contactData);
      } else if (employerPersonList) {
        updateContactService.updateContactService(contactData, function(error, info) {
          if (error) {
            logger.error('error while updating program contact');
            cb(error, contactData);
          } else {
            logger.info('succefully program contact updated');
            cb(null, info);
          }
        });
      } else {
        //throws error when invalid contatcId or programId or campusId
        throwError('Invalid contactId or campusId or programId ', cb);
        logger.error('Invalid contactId or campusId or programId');
      }
    });
  } else {
    throwError('programId, contactId and campusId required', cb);
    logger.error('programId, contactId and campusId required');
  }
}
exports.programContactUpdate = programContactUpdate;
