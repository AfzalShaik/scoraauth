'use strict';
var async = require('async');
var server = require('../server/server');
var lookup = require('../commonValidation/lookupMethods').lookupMethod;
var throwerror = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var postIntoUploadWork = require('./student-mass-upload').postIntoUploadWork;

function createStudent(finalArray, stDataa, cb) {
  async.map(finalArray, studentInfoCreation, function(studentErr, studentInfo) {
    cb(null, studentInfo);
  });

  function studentInfoCreation(obj, callBc) {
    var scorauser = server.models.ScoraUser;
    var enrollment = server.models.Enrollment;
    if (obj.skipUserInd == 'Y' || obj.skipUserInd == 'y') {
      var enrollmentObj = {
        'studentId': obj.studentId,
        'programId': obj.programId,
        'admissionNo': obj.admissionNo,
        'startDate': obj.startDate,
        'planedCompletionDate': obj.plannedCompletionDate,
        'dataVerifiedInd': 'Y',
        'createDatetime': new Date(),
        'updateDatetime': new Date(),
        'createUserId': 1,
        'updateUserId': 1,
        'campusId': obj.campusId,
      };
      enrollment.create(enrollmentObj, function(enrollError, enrollObj) {
        if (enrollError) {
          obj.error = 'Invalid Admission Number';
          postIntoUploadWork(obj, function(error, errorResponse) {
            callBc(null, null);
          });
        } else {
          callBc(null, obj);
        }
      });
    } else {
      lookup('ROLE_TYPE_CODE', function(err, response) {
        var studentIndicator = response.find(findStudent);
          // To Get Student Role_Type_Value_Id
        function findStudent(studentVal) {
          return studentVal.lookupValue === 'Student';
        }
        var input = {
          'email': obj.email,
          'roleTypeValueId': studentIndicator.lookupValueId,
          'firstName': obj.firstName,
          'lastName': obj.lastName,
          'programId': stDataa.programId,
          'admissionNo': obj.admissionNo,
          'startDate': obj.startDate,
          'planedCompletionDate': obj.plannedCompletionDate,
          'campusId': obj.campusId,
        };
        var url = 'http://139.162.253.126:3000/api/ScoraUsers';
        var request = require('request');
  
        request({
          url: url,
          method: 'POST',
          json: true,
          body: input,
  
        }, function(err, response, body) {
          if (err) { throwerror(err, callBc); }
          var enrollment = server.models.Enrollment;
          enrollment.findOne({'where': {'enrollmentId': body.data.enrollmentId}}, function(enrollErr, enrollData) {
            var enrollObj = enrollData;
            enrollObj.campusId = obj.campusId;
            enrollData.updateAttributes(enrollObj, function(updateErr, updateOut) {
              callBc(null, updateOut);
            });
          });
        });
      });
    }
  }
}
exports.createStudent = createStudent;
