'use strict';
var server = require('../server/server');
var eventStudentList = server.models.EventStudentList;
var async = require('async');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var logger = require('../server/boot/lib/logger');
  /**
   *updateStudentEvents-function deals with updating the events
   *@constructor
   * @param {object} stData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function updateStudentEvents(stData, cb) {
  // console.log('objjjjjjjjjjjjjjjjjjjjjjj ', stData);
  eventStudentList.findOne({
    'where': {
      'and': [{
        'campusEventId': stData.campusEventId,
      },
      {
        'studentId': stData.studentId,
      },
      ],
    },
  }, function(err, events) {
    if (events) {
      checkEventISOpen(events, function(error, openEvent) {
        // console.log('openEventopenEventopenEvent ', openEvent);
        if (openEvent) {
          updateOffer(events, stData, function(finalErr, updateEvent) {
            cb(null, updateEvent);
          });
        } else {
          cb(error, null);
        }
      });
    } else {
      cb('Records not found', null);
    }
  });
}
exports.updateStudentEvents = updateStudentEvents;
  /**
   *checkEventISOpen-function deals with checking the event was open or not
   *@constructor
   * @param {object} obj - contains all the data which required for searching an event
   * @param {any} callBc - deals with the response
   */
function checkEventISOpen(obj, callBc) {
  var openEventCheck = require('./get-student-events').getStudentEvents;
  openEventCheck(obj.studentId, function(err, resp) {
    var eventObj = resp.find(findId);

    function findId(statusVal) {
      return statusVal.campusEventId === obj.campusEventId;
    }
    callBc(null, eventObj);
  });
}
  /**
   *updateOffer-function deals with updating the offer
   *@constructor
   * @param {object} response - contains offer details
   * @param {object} input - contains id to which it get updated
   * @param {any} cb - deals with the response
   */
function updateOffer(response, input, cb) {
  response.updateAttributes(input, function(err, info) {
    if (err) {
      throwError(err, cb);
    } else {
      logger.info('Student Event Offer record Updated Successfully');
      info['requestStatus'] = true;
      info['updateDatetime'] = new Date();
      cb(null, info);
    }
  });
}
  /**
   *checkOfferValue-function deals with checking the offer
   *@constructor
   * @param {any} cb - deals with the response
   */
function checkOfferValue(cb) {
  var offerStatusValueId;
  var lookup = require('../commonValidation/lookupMethods').lookupMethod;
  lookup('OFFER_STATUS_CODE', function(err, response) {
    offerStatusValueId = response.find(findStatus);
    function findStatus(statusVal) {
      return statusVal.lookupValue === 'Offered';
    }
  });
}
