'use strict';
var async = require('async');
var server = require('../server/server');
var cleanArray = require('../commonValidation/common-mass-upload.js').cleanArray;
var deleteSingleEntityDetails = require('../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;

function createCampusPlacementAggregate(fileInput, inputData, callBc) {
  async.map(fileInput, verifyDepartment, departmentCB);

  function verifyDepartment(obj, departmentCallBc) {
    var department = server.models.Department;
    department.findOne({
      'where': {
        'and': [{
          'name': obj.departmentName,
        }, {
          'campusId': inputData.campusId,
        }],
      },
    }, function(deptErr, deptOut) {
      var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
      if (deptErr) {
        obj.error = 'Department Does Not Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          departmentCallBc(null, null);
        });
      } else if (deptOut) {
        obj.departmentId = deptOut.departmentId;
        departmentCallBc(null, obj);
      } else {
        obj.error = 'Department Does Not Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          departmentCallBc(null, null);
        });
      }
    });
  }

  function verifyProgram(obj, programCallBc) {
    var program = server.models.Program;
    program.findOne({
      'where': {
        'and': [{
          'programName': obj.programName,
        }, {
          'campusId': inputData.campusId,
        }],
      },
    }, function(programErr, programOut) {
      var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
      if (programErr) {
        obj.error = 'Program Does Not Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          programCallBc(null, null);
        });
      } else if (programOut) {
        obj.programMajorValueId = programOut.programMajorValueId;
        obj.programId = programOut.programId;
        programCallBc(null, obj);
      } else {
        obj.error = 'Program Does Not Exist';
        postIntoUploadWork(obj, function(error, errorResponse) {
          programCallBc(null, null);
        });
      }
    });
  }

  function postIntoUploadWork(object, cb) {
    // var count = 0;
    var input = {};
    input = {
      'rowNumber': object.rowNumber,
      'departmentName': object.departmentName,
      'programName': object.programName,
      'noOfOffers': object.noOfOffers,
      'acedemicYear': object.acedemicYear,
      'noOfStudents': object.noOfStudents,
      'totalOffers': object.totalOffers,
      'minimumOffer': object.minimumOffer,
      'maximumOffer': object.maximumOffer,
      'top5': object.top5,
      '6To10': object.sixToTen,
      '11To20': object.levenTo20,
      '21To50': object.twentyOneTo50,
      'above50': object.above50,
      'error': object.error,
      'programId': object.programId,
      'departmentId': object.departmentId,
    };
    var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
    campusPlacementAggregatesWork.create(input, function(err, uploadResponse) {
      if (err) {
        throwError(err, cb);
      } else {
        cb(null, uploadResponse);
      }
    });
  }

  function programCB(programErr, programOutput) {
    var departmentResponse = cleanArray(programOutput);
    async.map(departmentResponse, verifyValidation, validationCB);
  }

  function departmentCB(deparmentErr, departmentOutput) {
    var departmentResponse = cleanArray(departmentOutput);
    async.map(departmentResponse, verifyProgram, programCB);
  }

  function validationCB(validationErr, validationOutput) {
    var validationResponse = cleanArray(validationOutput);
    var inputFilterObject = {};
    inputFilterObject['campusId'] = inputData.campusId;
    var campusPlacementAggregates = server.models.CampusPlacementAggregates;
    var campus = server.models.Campus;
    deleteSingleEntityDetails(inputFilterObject, campusPlacementAggregates, function(deletedErr, deletedRecords) {
      campus.findOne({
        'where': {
          'campusId': inputData.campusId,
        },
      }, function(campusErr, campusResponse) {
        var universityId = campusResponse.universityId;
        async.map(validationResponse, createAggregate, aggregateCB);

        function createAggregate(obj, aggregateCallBC) {
          var input = {
            'campusId': inputData.campusId,
            'universityId': universityId,
            'departmentId': obj.departmentId,
            'programId': obj.programId,
            'noOfStudents': obj.noOfStudents,
            'noOfOffers': obj.noOfOffers,
            'total_offers': obj.totalOffers,
            'minimumOffer': obj.minimumOffer,
            'maximumOffer': obj.maximumOffer,
            'academicYear': obj.acedemicYear,
            'top5': obj.top5,
            '6To10': obj.sixToTen,
            '11To20': obj.levenTo20,
            '21To50': obj.twentyOneTo50,
            'above50': obj.above50,
          };
          campusPlacementAggregates.create(input, function(createErr, createOut) {
            aggregateCallBC(null, createOut);
          });
        }
      });
    });
  }

  function aggregateCB(aggError, aggregateOutput) {
    var aggregateResponse = cleanArray(aggregateOutput);
    var createCampusLog = require('./create-campus-log').createCampusLog;
    var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
    var readError = require('../commonValidation/read-error-into-file').readErrorRecords;
    campusPlacementAggregatesWork.find({}, function(findErr, failedOutput) {
      if (findErr) {
        campusPlacementAggregatesWork.destroyAll({}, function(error, destroy) {
          callBc(null, failedOutput);
        });
      } else {
        readError(failedOutput, inputData, function(readError, readResponse) {
          var logInput = {
            'campusUploadTypeValueId': inputData.campusUploadTypeValueId,
            'campusId': inputData.campusId,
            'container': inputData.fileDetails.container,
            'name': inputData.fileDetails.name,
            'originalFileName': inputData.fileDetails.originalFileName,
            'totalNoRecs': fileInput.length,
            'noFailRecs': cleanArray(failedOutput).length,
            'noSuccessRecs': aggregateResponse.length,
          };
          createCampusLog(logInput, function(logErr, logOutput) {
            if (logErr) {
              campusPlacementAggregatesWork.destroyAll({}, function(error, destroy) {
                callBc(null, failedOutput);
              });
            } else {
              campusPlacementAggregatesWork.destroyAll({}, function(error, destroy) {
                callBc(null, logOutput);
              });
            }
          });
        });
      }
    });
  }

  function verifyValidation(object, verifyCallBack) {
    if (object.noOfOffers > 0 && object.noOfStudents > 0 && object.totalOffers > 0 && object.minimumOffer > 0 && object.maximumOffer > 0 && object.top5 > 0 && object.sixToTen > 0 && object.levenTo20 > 0 && object.twentyOneTo50 > 0 && object.above50 > 0 && (parseInt(object.maximumOffer) > parseInt(object.minimumOffer)) && (parseInt(object.noOfStudents) > parseInt(object.noOfOffers))) {
      verifyCallBack(null, object);
    } else {
      object.error = 'Validation Failed';
      postIntoUploadWork(object, function(error, errorResponse) {
        verifyCallBack(null, null);
      });
    }
  }
}
exports.createCampusPlacementAggregate = createCampusPlacementAggregate;
