'use strict';
var server = require('../server/server');

function createCampusLog(input, callBc) {
  var campusUploadLog = server.models.CampusUploadLog;
  var logInput = {
    'campusUploadTypeValueId': input.campusUploadTypeValueId,
    'campusId': input.campusId,
    'inputParameters': 'string',
    'createUserId': 1,
    'createDatetime': new Date(),
    'csvFileLocation': './attachments/' + input.container + '/' + input.name + '/' + input.originalFileName,
    'errorFileLocation': './attachments/' + input.container + '/' + 'download' + '/' + input.name,
    'totalNoRecs': input.totalNoRecs,
    'noFailRecs': input.noFailRecs,
    'noSuccessRecs': input.noSuccessRecs,
  };
  campusUploadLog.create(logInput, function(logErr, logOutput) {
    callBc(null, logOutput);
  });
}
exports.createCampusLog = createCampusLog;
