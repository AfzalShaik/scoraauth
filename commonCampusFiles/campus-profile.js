'use strict';
var server = require('../server/server');
var errorFunction = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var getLookups = require('./get-campus-dashboard').getLookups;
var campus = server.models.Campus;
var enrolledStudents = 0;
var placed = 0;
var offerd = [];
var totalEnrollStudents = [];
var finayYearStudents = 0;
var selectedPeople = [];
var totalOfferedJobs = 0;
var compvisted = 0;
var fnlYr = [];
function getCampusProfile(campusId, callBc) {
  campus.findOne({'where': {'campusId': campusId}}, function(err, campusResponse) {
    if (err) {
      callBc(err, null);
    } else if (campusResponse == 0) {
      errorFunction('cant find the campus details', callBc);
    } else {
      getDepartment(campusResponse, campusId, function(err, depRes) {
        if (err) {
          errorFunction('err', callBc);
        } else {
          var departmentId = false;
          if (departmentId) {
            getDepartmentDetails(campusId, departmentId, function(err, responseFinal) {
              if (err) {
                errorFunction('err occured', callBc);
              } else {
                depRes.totalSelectedPeople = totalOfferedJobs;
                depRes.selectedPeople = selectedPeople;
                callBc(null, depRes);
              }
            });
          } else {
            callBc(null, depRes);
          }
        }
      });
    }
  });
}
function getDepartment(campusResponse, campusId, depCallback) {
  var dep = server.models.Department;
  dep.find({'where': {'campusId': campusId}}, function(error1, depRes) {
    if (error1) {
      callBc(error1, null);
    } else {
      getUniversity(campusResponse, depRes, campusId, function(err, univRes) {
        if (err) {
          errorFunction('err', depCallback);
        } else {
          console.log(depRes);
          depCallback(null, univRes);
        }
      });
    }
  });
}
function getUniversity(campusResponse, depRes, campusId, univCallback) {
  var university = server.models.University;
  university.findOne({'where': {'universityId': campusResponse.universityId}},
  function(error, universityResponse) {
    if (error) {
      callBc(error, null);
    } else {
      getLookUp(campusResponse, depRes, universityResponse, campusId,
            function(err, lookupResponse) {
              if (err) {
                errorFunction('err', univCallback);
              } else {
                univCallback(null, lookupResponse);
              }
            });
    }
  });
}
function getLookUp(campusResponse, depRes, universityResponse, campusId, lookupCb) {
  lookup('UNIVERSITY_TYPE_CODE', universityResponse.universityTypeValueId,
    function(er, lookUpRes) {
      if (er) {
        callBc(er, null);
      } else {
        getContact(campusResponse, depRes, universityResponse, campusId, lookUpRes,
            function(err, contactRes) {
              if (err) {
                errorFunction('err', lookupCb);
              } else {
                lookupCb(null, contactRes);
              }
            });
      }
    });
}
function getContact(campusResponse, depRes, universityResponse,
                    campusId,
                    lookUpRes,
                    contactCallback) {
  var campusContact = server.models.CampusContact;
  campusContact.findOne({'where': {'campusId': campusId, 'primaryInd': 'Y'}},
    function(error2, contactResponse) {
      if (error2) {
        contactCallback(error2, null);
      } else if (contactResponse == null) {
        var contactInfo = [];
        getTotalStudentsEnrolled(campusResponse, campusId,
          contactInfo,
          depRes,
          universityResponse, lookUpRes,
          function(err, enrollmentRes) {
            if (err) {
              errorFunction('err', contactCallback);
            } else {
              contactCallback(null, enrollmentRes);
            }
          });
      } else {
        var contact = server.models.Contact;
        contact.findOne({'where': {'contactId': contactResponse.contactId}},
        function(error3, contactInfo) {
          if (error3) {
            contactCallback(error3, null);
          } else {
            getTotalStudentsEnrolled(campusResponse, campusId,
                contactInfo,
                depRes,
                universityResponse, lookUpRes,
                function(err, enrollmentRes) {
                  if (err) {
                    errorFunction('err', contactCallback);
                  } else {
                    contactCallback(null, enrollmentRes);
                  }
                });
          }
        });
      };
    });
}
function getTotalStudentsEnrolled(campusResponse, campusId,
                                    contactInfo,
                                    depRes,
                                    universityResponse, lookUpRes,
                                    enrollcb) {
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'campusId': campusId}}, function(err, enrollRes) {
    if (err) {
      errorFunction('err', enrollcb);
    } else {
      var async = require('async');
      enrollRes.filter(function(ob) {
        var date = new Date().getFullYear();
        var date1 = JSON.stringify(new Date());
        var compYear = date1.substr(6, 2);
        var final = ob.createDatetime;
        var parse = JSON.stringify(final);
        var p = parse.substr(1, 4);
        if (p == date) { enrolledStudents++; }
        var finalYear = JSON.stringify(ob.actualCompletionDate);
        var finalYr = finalYear.substr(1, 4);
        if (compYear < 6) { if (finalYr == date) { finayYearStudents++; } }
        if (compYear >= 6) { if (finalYr == date + 1) { finayYearStudents++; } }
      });
      // numberOfStudentsPlaced(campusResponse, campusId, contactInfo, depRes,
      //                   universityResponse,
      //                   lookUpRes,
      //                   enrollRes,
      //                   function(err, placedRes) {
      //                     if (err) {
      //                       errorFunction('err', enrollcb);
      //                     } else {
      //                       var campEvent = server.models.CampusEvent;
      //                       campEvent.find({'where': {'campusId': campusId}},
      //                       function(err3, evntRes) {
      //                         if (err3) {
      //                           errorFunction('err', enrollcb);
      //                         } else {
      //                           evntRes.filter(function(data) {
      //                             compvisted++;
      //                           });
      var final = createObject(campusResponse, contactInfo, depRes, universityResponse, lookUpRes);
      enrollcb(null, final);
                        //       }
                        //     });
                        //   }
                        // });
    }
  });
}
function numberOfStudentsPlaced(campusResponse, campusId, contactInfo, depRes,
                                universityResponse,
                                lookUpRes,
                                enrollRes, placedCb) {
  var eventStudent = server.models.EventStudentList;
  eventStudent.find({'where': {'campusId': campusId}}, function(err, placedRes) {
    if (err) {
      errorFunction('err', placedCb);
    } else {
      offerd = [];
      var async = require('async');
      async.map(placedRes, getPlacedCount, function(err, res) {
        if (err) {
          errorFunction('err', placedCb);
        } else {
          placedCb(null, 'done');
        }
      });
    }
  });
}
function getPlacedCount(obj, cb) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
  lookup.findOne({'where': {'lookupCode': 'CANDIDATE_STATUS_TYPE'}}, function(err, res) {
    lookupvalue.findOne({'where': {'lookupValue': 'Offered', 'lookupTypeId': res.lookupTypeId}},
    function(err1, res1) {
      var async = require('async');
      // console.log(res1);
      //   console.log(obj.candidateStatusValueId);
      if (obj.candidateStatusValueId == res1.lookupValueId) {
        var comp = server.models.CompensationPackage;
        comp.findOne({'where': {'companyId': obj.companyId, 'compPackageId': obj.compPackageId}}, function(err, compRes) {
           // console.log(compRes);
          if (err) {
            errorFunction(err, placedCb);
          } else if (compRes == null) {
            cb(null, 'done');
          } else {
             // console.log('hear');
            offerd.push(compRes.totalCompPkgValue);
            cb(null, 'done');
          }
        });
        placed++;
      }
    });
  });
}
function createObject(campus, contact, dep, university, lookup) {
  var data = {};
  data.campusName = campus.name;
  data.brandingImage = campus.brandingImage;
  data.facebook = campus.facebook;
  data.twitter = campus.twitter;
  data.youtube = campus.youtube;
  data.linkedin = campus.linkedin;
  data.logo = campus.logo;
  data.description = campus.description;
  data.DepartmentList = dep;
  data.UniversityName = university.name;
  data.establishedDate = campus.establishedDate;
  data.indiaRank = campus.rank;
  data.Tier = university.tier;
  data.rating = campus.rating;
  data.website = campus.website;
  data.contact = contact;
  data.totalStudentsEnrolled = enrolledStudents;
  // data.placedStudents = placed;
  // if (offerd.length > 0) {
  //   data.maxOffered = offerd.reduce(function(a, b) { return Math.max(a, b); });
  // } else {
  //   data.maxOffered = [];
  // }
  data.finalYearStudents = finayYearStudents;
  // data.totalCompaniesVisited = compvisted;
  return data;
};
function lookup(type, ob, lookupCallback) {
  var lookup = server.models.LookupType;
  var lookupvalue = server.models.LookupValue;
      //var type = 'SKILL_TYPE_CODE';
  lookup.find({
    'where': {
      lookupCode: type,
    },
  }, function(err, re) {
    if (err) {
      lookupCallback(err, null);
    } else {
      lookupvalue.findOne({
        'where': {
          lookupValueId: ob,
          lookupTypeId: re[0].lookupTypeId,
        },
      }, function(err, re1) {
        if (err) {
          lookupCallback('error in lookup', re1);
        } else {
          lookupCallback(null, re1);
        };
      });
    }
  });
}
function getDepartmentDetails(campusId, departmentId, depcb) {
  // console.log(campusId);
  // console.log(departmentId);
  var program = server.models.Program;
  program.find({'where': {'departmentId': departmentId, 'campusId': campusId}},
  function(err, res) {
    if (err) {
      errorFunction('error occured in dep', depcb);
    } else {
      var async = require('async');
      totalEnrollStudents = [];
      async.map(res, getStudents, function(err2, res1) {
        if (err2) {
          errorFunction('err', depcb);
        } else {
          // console.log('getting hear');
          // console.log(totalEnrollStudents);
          if (totalEnrollStudents == 0) {
            depcb(null, 'no one');
          } else {
            for (var i = 0; i < totalEnrollStudents.length; i++) {
              var async = require('async');
              fnlYr = [];
              async.map(totalEnrollStudents[i], getFinalYearStudents, function(err4, res3) {
                if (err4) {
                  errorFunction('err', depcb);
                } else {
                  //console.log('inhear');
                  if (i == totalEnrollStudents.length - 1) {
                    getStudentCompinesHiredDetails(function(err, hireRes) {
                      if (err) {
                        errorFunction('an err', depcb);
                      } else {
                        depcb(null, 'done');
                      }
                    });
                  }
                }
              });
            }
          }
        }
      });
    }
  });
}
function getStudents(ob, cb) {
  var enroll = server.models.Enrollment;
  enroll.find({'where': {'programId': ob.programId}}, function(err2, res2) {
    if (err2) {
      errorFunction('an error occured', cb);
    } else {
      if (res2 != 0) { totalEnrollStudents.push(res2); }
      cb(null, 'done');
    }
  });
}
var count = 0;

function getFinalYearStudents(ob1, cb1) {
  if (ob1 != 0) {
    var date = new Date().getFullYear();
    var date1 = JSON.stringify(new Date());
    var compYear = date1.substr(6, 2);
    var finalYear = JSON.stringify(ob1.actualCompletionDate);
    var finalYr = finalYear.substr(1, 4);
    if (compYear < 6) { if (finalYr == date) { fnlYr.push(ob1); } }
    if (compYear >= 6) { if (finalYr == date + 1) { fnlYr.push(ob1); } }
    cb1(null, 'done');
  }
}
function getStudentCompinesHiredDetails(hireCb) {
  var async = require('async');
  async.map(fnlYr, getPlacedDetails, function(err5, res4) {
    if (err5) {
      errorFunction('err5', hireCb);
    } else {
      hireCb(null, res4);
    }
  });
}
function getPlacedDetails(ob2, hiredCalBc) {
  var eventStudent = server.models.EventStudentList;
  eventStudent.find({'where': {'studentId': ob2.studentId}},
                                      function(err6, res5) {
                                        if (err6) {
                                          errorFunction('err6', hiredCalBc);
                                        } else {
                                          if (res5 != 0) {
                                            var async = require('async');
                                            selectedPeople = [];
                                            totalOfferedJobs = 0;
                                            async.map(res5, getData, function(err7, res6) {
                                              if (err7) {
                                                errorFunction('err', hiredCalBc);
                                              } else {
                                                hiredCalBc(null, 'done');
                                              }
                                            });
                                          } else {
                                            hiredCalBc(null, 'done');
                                          }
                                        }
                                      });
}
function getData(data, callBc1) {
  getLookups('CANDIDATE_STATUS_TYPE', 'Offered',
  function(candidateStatus) {
    if (data.candidateStatusValueId == candidateStatus.lookupValueId) {
      totalOfferedJobs++;
      selectedPeople.push(data);
      callBc1(null, 'done');
    }
  });
}
var mysql = require('mysql');
var con = mysql.createConnection({
  host: '139.162.253.126',
  user: 'devRead',
  password: 'devRead123',
  database: 'devScora',
});
var CompanyDetails = [];
var total = [];
function getGraphAndTableData(campusId, cb) {
  var date = new Date().getFullYear();
  var noOfOffersAndGraph = 'select sum(no_of_offers) , sum(total_offers)/sum(no_of_offers) ,max(maximum_offer) FROM CAMPUS_PLACEMENT_AGGREGATES' +
  ' where academic_year=' + date + ' and campus_id=' + campusId + '';
  MySql(noOfOffersAndGraph, function(err, sqlRes) {
    if (err) {
      errorFunction('an err', cb);
    } else {
      var companiesVisited = 'select count(distinct(company_id)) from CAMPUS_PLACEMENT_AGGREGATES' +
     ' where academic_year=' + date + ' and campus_id=' + campusId + '';
      MySql(companiesVisited, function(err, sqlRes1) {
        if (err) {
          errorFunction('err', cb);
        } else {
          var studentsSelected = 'select count(no_of_students) from CAMPUS_OFFER_AGGREGATES' +
          ' where campus_id=' + campusId + ' and academic_year=' + date + '';
          MySql(studentsSelected, function(err, sqlRes2) {
            if (err) {
              errorFunction('err', cb);
            } else {
              var past = date - 5;
              var graph = 'select company_id,sum(no_of_offers) from CAMPUS_PLACEMENT_AGGREGATES' +
              ' where academic_year between ' + past + ' and ' + date + ' and campus_id=388589390 group by company_id';
              MySql(graph, function(err, sqlRes3) {
                if (err) {
                  errorFunction('err', cb);
                } else {
                  var avg = 'select department_id,sum(no_offers)/sum(no_of_students)*100 from CAMPUS_OFFER_AGGREGATES' +
                  ' where campus_id=' + campusId + ' and academic_year between ' + past + ' and ' + date + ' group by department_id;';
                  MySql(avg, function(err, sqlRes4) {
                    if (err) {
                      errorFunction('err', cb);
                    } else {
                      createObjectForGraph(sqlRes, sqlRes1, sqlRes2, sqlRes3, sqlRes4, function(err, finalData) {
                        if (err) {
                          errorFunction('an err', cb);
                        } else {
                          cb(null, finalData);
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
}
function MySql(statment, sqlCb) {
  console.log(statment);
  con.query(statment, function(err, result, fields) {
    if (err) {
      errorFunction(err, sqlCb);
    } else {
      console.log(result);
      sqlCb(null, result);
    }
  });
}
function createObjectForGraph(offerCount, visitedCompines, selectedStudents, graphData, avg, createCb) {
  var async = require('async');
  CompanyDetails = [];
  async.map(graphData, getCompany, function(err, res) {
    if (err) {
      errorResponse('err', createCb);
    } else {
      var final = {};
      final.offer = offerCount;
      final.visited = visitedCompines;
      final.selectedPeopleCount = selectedStudents;
      final.companyDetails = CompanyDetails;
      final.avgOfferGraph = avg;
      createCb(null, final);
    }
  });
}
function getCompany(ob, cb5) {
  for (var key in ob) {
    if (key == 'sum(no_of_offers)') {
      total.push(ob[key]);
      passAnObj();
    }
  }
  function passAnObj() {
    var campus = server.models.Company;
    campus.findOne({'where': {'companyId': ob.company_id}}, function(err, res) {
      if (err) {
        errorFunction('err', cb5);
      } else if (res == null) {
        var data = {};
        data.campusName = null;
        data.TotalJobsForThatCampus = total[0];
        CompanyDetails.push(data);
        total = [];
        cb5(null, 'done');
      } else {
        var data = {};
        data.campusName = res.shortName;
        data.TotalJobsForThatCampus = total[0];
        CompanyDetails.push(data);
        total = [];
        cb5(null, 'done');
      }
    });
  }
}
exports.getCampusProfile = getCampusProfile;
exports.getGraphAndTableData = getGraphAndTableData;
