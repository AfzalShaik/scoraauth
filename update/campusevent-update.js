'use strict';
var server = require('../server/server');
var validateModel = require('../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var lookupValidation = require('../commonValidation/lookupMethods.js').validateLookups;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var logger = require('../server/boot/lib/logger');
var findEntity = require('../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var campusevent = server.models.CampusEvent;

/**
 * updateCampusEvent-- called by remote method which deals with updating the data in the database
 * @constructor
 * @param {object} input -holds the data which want to get updated
 * @param {function} cb -call back function which sends back the data to the called function
 */
function campusEvent(input, cb) {
  if (input) {
    var inputData = input;
    findAndUpdate(inputData, function(error, output) {
      if (error) {
        throwError(JSON.stringify(error), cb);
      } else {
        cb(null, output);
      }
    });
  } else {
    throwError('Invalid Input', cb);
  }
};
 /**
   *findAndUpdate-function deals with finding and updating the data
   *@constructor
   * @param {object} input - contains all the data need to get updated
   * @param {function} cb - deals with the response
   */
function findAndUpdate(input, cb) {
  var campusevent = server.models.CampusEvent;
  var inputFilterObject = {};
  inputFilterObject['campusEventId'] = input.campusEventId;
  findEntity(inputFilterObject, campusevent, function(error, response) {
    if (error) {
      throwError(error, cb);
    } else if (response) {
      response.data[0].updateAttributes(input, function(err, info) {
        if (err) {
          throwError(err, cb);
        } else {
          logger.info('campus event record Updated Successfully');
          info['requestStatus'] = true;
          info['updateDatetime'] = new Date();
          cb(null, info);
        }
      });
    } else {
      throwError('Invalid campusEventId', cb);
    }
  });
};

/*'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var errorResponse = require('../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var Joi = require('joi');
/**
 * campusEvent function called by updateCampusEvent
 *@constructor
 * @param {object} data-holds all the data need to be get updated
 * @param {function} cb- as an callback function

function campusEvent(data, cb) {
  if (data) {
    //fields that cant updated in the update method
    if (data.createDatetime || data.updateDatetime || data.createUserId ||
       data.updateUserId ||
    data.campusDriveId || data.campusId || data.employerEventId ||
    data.companyId || data.eventTypeValueId || data.eventStatusValueId) {
      var error = new Error('You cannot perform update operation: ');
      error.statusCode = 422;
      error.requestStatus = false;
      cb(error, data);
    } else {
      //checks for id was present or not
      if (data.campusEventId) {
        var schema = Joi.object().keys({
          campusEventId: Joi.number().integer().max(999999999999999).required(),
          eventName: Joi.string().max(50),
          scheduledDate: Joi.date(),
          scheduledStartTime: Joi.string().max(100),
          duration: Joi.number().max(20),
        });
        Joi.validate(data, schema, function(err, response) {
          if (err) {
            cb(err, data);
          } else {
            var eventName, scheduledDate, scheduledStartTime, Duration;
            var campusEvent = server.models.CampusEvent;
            //finds form the database using campusEventId
            campusEvent.find({'where': {'campusEventId': data.campusEventId}}, function(err, resp) {
              if (err) {
                errorResponse(cb);
                cb(err, data);
              } else if (resp == 0) {
                data.statusCode = 422;
                errorResponse(cb);
                cb(err, data);
              } else {
                console.log(resp);
                //updates detalis
                campusEvent.updateAll({campusEventId: data.campusEventId}, data, function(err, r) {
                  if (err) {
                    cb(err, data);
                    errorResponse(cb);
                  } else {
                    data.requestStatus = true;
                    console.log(data);
                    cb(null, data);
                  }
                });
              }
            }
            );
          }
        });
      }
      else {
        var error = new Error('You need to provide CampusEventId to update any operation: ');
        error.statusCode = 422;
        error.requestStatus = false;
        cb(error, data);
      }
    }
  }
}*/
exports.campusEvent = campusEvent;
