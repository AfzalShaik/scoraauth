'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    organizationName: Joi.string().max(100).required(),
    jobRoleName: Joi.string().max(50).required(),
    campusName: Joi.string().max(100).required(),
    calendarYear: Joi.number().integer().max(9999),
    maximumOffer: Joi.string().max(18),
    rowNumber: Joi.number().integer().max(999999999999999),
    minimumOffer: Joi.string().max(18),
    sumOfOffers: Joi.string().max(18),
    noOfOffers: Joi.number().integer().max(99999),
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;
