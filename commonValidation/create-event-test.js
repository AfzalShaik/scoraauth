'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createEventTestService-function deals with creating event test service
   *@constructor
   * @param {object} testata - contains all the test data test name vendor name etc..
   * @param {function} callBc - deals with the response
   */
function createEventTestService(testData, callBc) {
  async.map(testData, testMethod, function(err, resp) {
    if (err) {
      callBc(err.errors, null);
    } else {
      callBc(null, resp);
    }
  });

  function testMethod(obj, callback) {
    var eventTestScore = server.models.EventTestScore;
    testInput(obj, testData, function(eventTestData) {
      eventTestScore.create(eventTestData, function(err, eventTestResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, eventTestResp);
        }
      });
    });
  }
}

function testInput(obj, testData, callBc) {
  var eventTestData = {
    'studentId': obj.studentId,
    'testName': obj.testName,
    'description': obj.description,
    'score': obj.score,
    'employerEventId': obj.employerEventId,
    'testVendorName': obj.testVendorName,
    'maxScore': obj.maxScore,
  };
  callBc(eventTestData);
}
exports.createEventTestService = createEventTestService;
