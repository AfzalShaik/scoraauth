'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var updateAddress = require('./updateAddress');
var allmethods = require('./allmethods');
var Address = server.models.Address;
var lookupMethods = require('./lookupMethods');
var addressValidation = require('./addressValidation');
/**
   *updateAddresService-function deals with updating the address
   *@constructor
   * @param {object} addressData - contains the id need to get updated
   * @param {function} cb - deals with the response
   */
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Breakdown into multiple methods. High cyclomatic complexity and difficult to understand. 
function updateAddresService(addressData, cb) {
  if (addressData) {
    if (addressData.addressId) {
      //validating input json fields
      addressValidation.validateJson(addressData, function(err, validVal) {
        if (err) {
          cb(err, addressData);
        } else if (validVal == true) {
          var cityValue, stateValue, typeCodeVal, countryValue, postalCodeVal;
          var addressTypeCode = 'ADDRESS_TYPE_CODE';
          var addressTypeObj = {};
          addressTypeObj['lookupValueId'] = addressData.addressTypeValueId;
          var Address = server.models.Address;
          //addressTypeCodeFunction to verify addressTypeValueId
          lookupMethods.typeCodeFunction(addressTypeObj, addressTypeCode, function(typeCodeCheck) {
            typeCodeVal = typeCodeCheck;
            if (typeCodeVal == false) {
              var error = new Error('invalid addressTypeCode');
              error.statusCode = 422;
              error.requestStatus = false;
              cb(error, addressData);
            } else {
              //addressFunction function verifies city id , countryCode and stateCode
              allmethods.addressFunction(addressData, function(cityCheck) {
                cityValue = cityCheck;
                if (typeCodeVal == false || cityValue == false) {
                  var error = new Error(' cityId and stateCode and countryCode required and should be valid');
                  error.statusCode = 422;
                  error.requestStatus = false;
                  cb(error, addressData);
                } else {
                  Address.findOne({
                    'where': {
                      'addressId': addressData.addressId,
                    },
                  }, function(err, addressList) {
                    if (err) {
                      //throws error

                      cb(err, addressData);
                    } else if (addressList) {
                      //updating record in address table with addressId
                      console.log('this is update address', addressData);
                      updateAddress.updateAddress(addressData, function(error, info) {
                        if (error) {
                          cb(error, addressData);
                        } else {
                          cb(null, info);
                        }
                      });
                    } else {
                      //throws error incase of invalid employerPersonId or companyId
                      var error = new Error('Invalid addressId ');
                      logger.error('Invalid addressId');
                      error.statusCode = 422;
                      error.requestStatus = false;
                      cb(error, addressData);
                    }
                  });
                }
              });
            }
          });
        } else {
          var error = new Error('You cannot perform update operation: ');
          error.statusCode = 422;
          error.requestStatus = false;
          cb(error, addressData);
        }
      });
    } else {
      var error = new Error('required addressId');
      error.statusCode = 422;
      error.requestStatus = false;
      cb(error, addressData);
    }
  } else {
    logger.error("Input can't be Null");
    var error = new Error('Input is Null: ');
    error.statusCode = 422;
    error.requestStatus = false;
    cb(error, addressData);
  }
}
exports.updateAddresService = updateAddresService;
