'use strict';
var Joi = require('joi');
var validVal;
  /**
   *validateJson-function deals with validating the correct json format
   *@constructor
   * @param {object} massuploadData - contains all the data need to get updated
   * @param {any} cb - deals with the response
   */
function validateJson(massuploadData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    admissionNo: Joi.string().max(50).required(),
    error: [Joi.string().max(500), Joi.allow(null)],
    rowNumber: Joi.number().integer().max(999999999999999),
    campusId: Joi.number().integer().max(999999999999999),
    score: Joi.number().integer().max(999).required(),
  });

  Joi.validate(massuploadData, schema, function(err, value) {
    if (err) {
      var error = new Error('');
      error.statusCode = 422;
      error.requestStatus = false;
      error.err = err.details[0].message;
      validVal = false;

      cb(error, null);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}
exports.validateJson = validateJson;
