'use strict';
var Joi = require('joi');
var validVal;
var throwError = require('../ErrorHandling/customizeErrorTypes').errorFunction;
var lookupMethods = require('../commonValidation/lookupMethods');
var logger = require('../server/boot/lib/logger');
/**
   *validateCompensationPackageJson-function deals with validating json data
   *@constructor
   * @param {object} pakageData - contains all the data need to get valiadated
   * @param {function} cb - deals with the response
   */
function validateCompensationPackageJson(packageData, cb) {
  //validating input json fields
  var schema = Joi.object().keys({
    compPackageId: Joi.number().max(999999999999999).required(),
    compPackageItemId: Joi.number().max(999999999999999),
    companyId: Joi.number().max(999999999999999).required(),
    compPackageTypeValueId: Joi.number().max(999999999999999),
    compApprovalStatusValueId: Joi.number().max(999999999999999),
    compPackageName: Joi.string().max(50),
    compPackageItemName: Joi.string().max(50),
    description: Joi.string().max(100),
    currencyCode: Joi.string().max(100),
    createDatetime: Joi.date().iso(),
    updateDatetime: Joi.date().iso(),
    createUserId: Joi.number().max(999999999999999),
    updateUserId: Joi.number().max(999999999999999),
  });

  Joi.validate(packageData, schema, function(err, value) {
    if (err) {
      throwError('Validation Error', cb);
    } else {
      validVal = true;
      cb(null, validVal);
    }
  });
}

function validateCompensationPackageLookup(packageData, cb) {
  var compPackageTypeVal, compApprovalStatusValue, packageVal;
  var compPackageTypeObj = {};
  var compApprovalStatusObj = {};
  compPackageTypeObj['lookupValueId'] = packageData.compPackageTypeValueId;
  compApprovalStatusObj['lookupValueId'] = packageData.compApprovalStatusValueId;
  var compPackageTypeCode = 'COMP_PACKAGE_TYPE_CODE';
  var compApprovalStatusCode = 'COMP_APPROVAL_STATUS_CODE';
  lookupMethods.typeCodeFunction(compPackageTypeObj, compPackageTypeCode, function(compPackageTypeVal) {
    compPackageTypeVal = compPackageTypeVal;
    if (compPackageTypeVal == true) {
      lookupMethods.typeCodeFunction(compApprovalStatusObj, compApprovalStatusCode, function(compApprovalStatusValue) {
        compApprovalStatusValue = compApprovalStatusValue;
        
        if (compApprovalStatusValue == true) {
          packageVal = true;
          cb(null, packageVal);
        } else {
          packageVal = false;
          throwError('Invalid compApprovalStatusValueId', cb);// TODO: CODEREVIEW - VARA – 09SEP17  -- Give some meaningful description instead of variable names
          logger.error('Invalid compApprovalStatusValueId');// TODO: CODEREVIEW - VARA – 09SEP17  -- Give some meaningful description instead of variable names
        }
      });
    } else {
      packageVal = false;
      throwError('Invalid compPackageTypeValueId', cb);// TODO: CODEREVIEW - VARA – 09SEP17  -- Give some meaningful description instead of variable names
      logger.error('Invalid compPackageTypeValueId');// TODO: CODEREVIEW - VARA – 09SEP17  -- Give some meaningful description instead of variable names
    }
  });
}
exports.validateCompensationPackageJson = validateCompensationPackageJson;
exports.validateCompensationPackageLookup = validateCompensationPackageLookup;
