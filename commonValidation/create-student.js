'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createStudentService-function deals with creating student service
   *@constructor
   * @param {object} data - contains all the needed for finding data from the database
   * @param {object} stData - contains student data with student id
   * @param {function} cb - deals with the response
   */
function createStudentService(data, stData, cb) {
  async.map(data, createStudentMethod, function(e, r) {
    if (e) {
      cb(e.errors, null);
    } else {
      cb(null, r);
    }
  });

  function createStudentMethod(obj, callback) {
    var studentModel = server.models.Student;
    studentInput(obj, stData, function(studentData) {
      studentModel.create(studentData, function(err, studentResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, studentResp);
        }
      });
    });
  }
}
function studentInput(obj, stData, callBc) {
  var lookup = require('./lookupMethods').lookupMethod;
  lookup('STUDENT_STATUS_CODE', function(err, response) {
    var studentStatusValueId = response.find(findStatus);
    function findStatus(statusVal) {
      return statusVal.lookupValue === 'Confirmed';
    }
    var massData = [];
    massData = stData.find(findMail);
    function findMail(mass) {
      return mass.email === obj.userName;
    }
    var studentData = {
      'userId': obj.id,
      'firstName': massData.firstName,
      'lastName': massData.lastName,
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
      'offCampusInd': 'N',
      'studentStatusValueId': studentStatusValueId.lookupValueId,
    };
    callBc(studentData);
  });
}
exports.createStudentService = createStudentService;
