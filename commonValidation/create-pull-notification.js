'use strict';
var server = require('../server/server');
var logger = require('../server/boot/lib/logger');
var throwError = require('../ErrorHandling/customizeErrorTypes.js').errorFunction;
var async = require('async');
var loginRole;

function createPullNotification(input, callBack) {
  loginRole = input.role;
  var notificationDetails = server.models.NotificationDetails;
  notificationDetails.find({
    'where': {
      'and': [{
        'recipientId': input.userId,
      }, {
        'notificationDismissedInd': 'N',
      }],
    },
  }, function(err, output) {
    if (err) {
      throwError(err, callBack);
    } else {
      async.map(output, getNotification, function(notificationError, notification) {
        if (notificationError) {
          throwError(notificationError, callBack);
        } else {
          // console.log('notificationArray::::: ', notification);
          // async.map(notification, getTemplate, function(templateErr, template) {
          //   if (templateErr) {
          //     throwError(templateErr, null);
          //   } else {
          //     var finalOutput = {};
          //     finalOutput.notificationDetails = output;
          //     finalOutput.notificationEvent = notification;
          //     finalOutput.notificationTemplate = template;
          callBack(null, notification);
          //   }
          // });
        }
      });
    }
  });
}

function getNotification(obj, callBC) {
  var inputObj = {};
  var notificationEvents = server.models.NotificationEvents;
  inputObj['notificationEventId'] = obj.notificationEventId;
  notificationEvents.findOne({
    'where': {
      'and': [inputObj],
    },
  }, function(notificationError, notificationObj) {
    if (notificationError) {
      callBC(notificationError, null);
    } else {
      // console.log('......', obj, notificationObj);
      // cb(null, notification);

      // function getTemplate(obj, callBC) {
      // console.log('objjjjjjjjjjjjjjjjjjjjjjj ', JSON.stringify(obj));
      var notificationMessageTemplates = server.models.NotificationMessageTemplates;
      var inputObj = {};
      inputObj['notificationTemplateId'] = notificationObj.notificationTemplateId;

      notificationMessageTemplates.findOne({
        'where': {
          'and': [inputObj],
        },
      }, function(templateError, template) {
        if (templateError) {
          callBC(templateError, null);
        } else {
          var employerEvent = server.models.EmployerEvent;
          var campusEvent = server.models.CampusEvent;
          var eventModel = (loginRole == 'PLCDIR') ? campusEvent : employerEvent;
          var empObj = {};
          var eduObj = {};
          var inputObject = {};
          empObj['empEventId'] = notificationObj.transactionId;
          eduObj['campusEventId'] = notificationObj.transactionId;
          inputObject = (loginRole == 'PLCDIR') ? eduObj : empObj;
          eventModel.findOne({
            'where': {
              'and': [inputObject],
            },
          }, function(eventErr, eventOutput) {
            if (eventErr) {
              callBC(null, eventErr);
            } else {
              var campus = server.models.Campus;
              var company = server.models.Company;
              var modelName = (loginRole == 'PLCDIR') ? company : campus;
              var employerObj = {};
              var educationObj = {};
              var object = {};
              // employerObj['companyId'] = obj.parentId;
              employerObj.companyId = notificationObj.parentId;
              educationObj.campusId = notificationObj.parentId;
              object = (loginRole == 'PLCDIR') ? employerObj : educationObj;
              // console.log(object);
              modelName.findOne({
                'where': object,
              }, function(campusErr, campusOut) {
                if (campusErr) {
                  callBC(campusErr, null);
                } else {
                  var notificationName = template.notificationName;
                  var receiverRoleCode = template.receiverRoleCode;
                  var eventName = eventOutput.eventName;
                  var entityName = campusOut.name;
                  var validator = (loginRole === 'PLCDIR') ? 'campus' : 'employer';
                  // console.log('validatorrrrrrrrrrrrrr ', loginRole, validator, notificationName);
                  getMessage(notificationName, receiverRoleCode, eventName, entityName, validator, function(messageErr, messageOut) {
                    if (messageErr) {
                      callBC(messageErr, null);
                    } else {
                      template.messageText = messageOut;
                      var finalOutput = {};
                      finalOutput.notificationDetails = obj;
                      finalOutput.notificationEvent = notificationObj;
                      finalOutput.notificationTemplate = template;
                      callBC(null, finalOutput);
                    }
                  });
                }
              });
            }
          });
        }
      });
      // }
    }
  });
}

function getMessage(notificationName, receiverRoleCode, eventName, entityName, validator, messageCB) {
  if (notificationName === 'CAMPUS_REJECT_COMP_EVENT') {
    var messageText = 'Your event' + ' ' + eventName + ' ' + 'has been rejected by' + ' ' + entityName;
    messageCB(null, messageText);
  } else if (notificationName === 'CAMPUS_ACCEPT_COMPANY_EVENT') {
    var messageText = 'Your event' + ' ' + eventName + ' ' + 'has been accepted by' + ' ' + entityName;
    messageCB(null, messageText);
  } else if (notificationName === 'SHARE_STUDENTS_LIST') {
    var messageText = entityName + ' ' + 'has shared the student list for your event' + ' ' + eventName;
    messageCB(null, messageText);
  } else if (notificationName === 'CAMPUS_SCHEDULE_EVENT') {
    var messageText = entityName + ' ' + 'has scheduled your event' + ' ' + eventName + ' ' + 'on' + ' ' + new Date();
    messageCB(null, messageText);
  } else if (notificationName === 'STUDENT_EVENT_UNSUBSCRIBE' && validator === 'employer') {
    var messageText = 'One or more students have unsubscribed from' + ' ' + eventName + '.' + ' ' + 'Please review the updated list.';
    messageCB(null, messageText);
  } else if (notificationName === 'PUBLISH_COMP_EVENT') {
    var messageText = 'PUBLISH_COMP_EVENT';
    messageCB(null, messageText);
  } else if (notificationName === 'SHARE_SHORTLISTED_STUDENTS') {
    var messageText = entityName + ' ' + 'has shared the shortlisted students for your event' + ' ' + eventName;
    messageCB(null, messageText);
  } else if (notificationName === 'STUDENT_EVENT_UNSUBSCRIBE' && validator === 'campus') {
    // console.log('11111111111111111111111111111111111111111111111111111111111111111111');
    var messageText = 'One or more students have unsubscribed from' + ' ' + eventName + ' ' + 'Please review the updated list.';
    messageCB(null, messageText);
  } else if (notificationName === 'STUDENT_LIST_REMINDER') {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to share the stuent list for your event' + ' ' + eventName + '.';
    messageCB(null, messageText);
  } else if (notificationName === 'SCHEDULE_REMINDER') {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to schedule the event' + ' ' + eventName + '.';
    messageCB(null, messageText);
  } else if (notificationName === 'SHORT_LIST_REMINDER') {
    var messageText = 'Reminder: ' + ' ' + entityName + ' ' + 'has requested to share the shortlisted students for the event' + ' ' + eventName + '.';
    messageCB(null, messageText);
  } else if (notificationName === 'PUBLISH_COMP_EVENT') {
    var messageText = 'PUBLISH_COMP_EVENT';
    messageCB(null, messageText);
  }
}
exports.createPullNotification = createPullNotification;
