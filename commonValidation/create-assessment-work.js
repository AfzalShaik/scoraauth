'use strict';
var server = require('../server/server');
var async = require('async');

function createAssessmentService(assessData, callBc) {
  async.map(assessData, assessMethod, function(err, resp) {
    if (err) {
      callBc(err.errors, null);
    } else {
      callBc(null, resp);
    }
  });

  function assessMethod(obj, callback) {
    testInput(obj, assessData, function(assessRespData) {
      var assessmentModel = server.models.Assessment;
      assessmentModel.create(assessRespData, function(err, assessResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, assessResp);
        }
      });
    });
  }
}

function testInput(obj, assessData, callBc) {
  var enrollmentModel = server.models.Enrollment;
  enrollmentModel.findOne({'where': {'admissionNo': obj.admissionNo}}, function(error, findResp) {
    if (error) {
      callBc(error);
    } else {
      var assessData = {
        'enrollmentId': findResp.enrollmentId,
        'studentId': findResp.studentId,
        'subjectDetails': findResp.subjectDetails,
        'dataVerifiedInd': findResp.dataVerifiedInd,
        'admissionNo': findResp.admissionNo,
        'score': obj.score,
        'programUnitTypeValueId': obj.programUnitTypeValueId,
        'createDatetime': new Date(),
        'updateDatetime': new Date(),
        'createUserId': 1,
        'updateUserId': 1,
      };
      callBc(assessData);
    }
  });
}
exports.createAssessmentService = createAssessmentService;
