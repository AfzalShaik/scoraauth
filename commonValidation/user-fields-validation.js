'use strict';
var fieldValid;
/**
   *fieldsvalidation-used to check weather the user trying to change the fields like user id and date
   *@constructor
   * @param {object} userData - contains the that user want topudate
   * @param {function} cb - returns true or false based on user input
   */
function fieldsvalidation(userData, cb) {
  if (userData.createDatetime || userData.updateDatetime || userData.createUserId || userData.updateUserId) {
    fieldValid = true;
    cb(fieldValid);
  } else {
    fieldValid = false;
    cb(fieldValid);
  }
};
exports.fieldsvalidation = fieldsvalidation;
