'use strict';
var server = require('../server/server');
var async = require('async');
/**
   *createUserService-function deals with creating user service
   *@constructor
   * @param {object} user - contains user email
   * @param {function} cb - deals with the response
   */
function createUserService(user, cb) {
  async.map(user, createUserMethod, function(e, r) {
    if (e) {
      cb(e.errors, null);
    } else {
      cb(null, r);
    }
  });

  function createUserMethod(obj, callback) {
    var massUser = server.models.ScoraUser;
    userInput(obj, function(userData) {
      massUser.create(userData, function(err, roleResp) {
        if (err) {
          callback(err, null);
        } else {
          callback(null, roleResp);
        }
      });
    });
  }
}

function userInput(obj, cb) {
  var lookup = require('./lookupMethods').lookupMethod;
  lookup('USER_STATUS_CODE', function(err, response) {
    var userStatusValueId = response.find(findStatus);

    function findStatus(statusVal) {
      return statusVal.lookupValue === 'Confirmed';
    }

    var userData = {
      'userStatusValueId': userStatusValueId.lookupValueId,
      'createDatetime': new Date(),
      'updateDatetime': new Date(),
      'createUserId': 1,
      'updateUserId': 1,
      'email': obj.email,
      'password': '1234',
    };
    cb(userData);
  });
}
exports.createUserService = createUserService;
