'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EDUCATION_PERSON) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to campus model then we execute logic or change request object before saving into database
  EDUCATION_PERSON.observe('before save', function educationPersonBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.educationPersonId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('education-person created successfully');
      next();
    } else if (ctx.isNewInstance) {
      logger.info('error while creating education-person');
      errorResponse(next);
    } else {
      next();
    }
  });
    /**
 *getEducationPerson- To education person by taking required fields
 *@constructor
 * @param {object} educationPersonId - Unique id for ecah and every education person
 * @param {number} campusId - unique id for each and every campus
 * @param {function} callBc - deals with response
 */
  // remote method definition to get education person details
  EDUCATION_PERSON.getEducationPerson = function(educationPersonId, campusId, callBc) {
    if (educationPersonId && campusId) {
      var educationPersonDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['educationPersonId'] = educationPersonId;
      inputFilterObject['campusId'] = campusId;
      var educationPerson = server.models.EducationPerson;
      logger.info('retrieved education-person details successfully based on educationPersonId and campusId');
      educationPersonDetById(inputFilterObject, educationPerson, callBc);
    } else {
      logger.error('error while retrieving education-person details');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get education person details
  EDUCATION_PERSON.remoteMethod('getEducationPerson', {
    description: 'To get education person details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'educationPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEducationPerson',
      verb: 'get',
    },
  });
  //  EDUCATION_PERSON remoteMethod
   /**
 *updateEducationPerson- To update education person Details
 *@constructor
 * @param {object} personData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EDUCATION_PERSON.updateEducationPerson = function(personData, cb) {
    var personUpdate = require('../../commonCampusFiles/update-education-person.js');
    personUpdate.educationPersonUpdate(personData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update EDUCATION_PERSON remoteMethod
  EDUCATION_PERSON.remoteMethod('updateEducationPerson', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEducationPerson',
      verb: 'PUT',
    },
  });
};
