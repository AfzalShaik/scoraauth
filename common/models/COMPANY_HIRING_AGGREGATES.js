'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var async = require('async');
var output = [];
var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
module.exports = function(COMPANY_HIRING_AGGREGATES) {
  //create company hiring mass upload data
  COMPANY_HIRING_AGGREGATES.companyHiringMassUpload = function(companyHiringData, cb) {
    var companyHiringAggregatesWork = server.models.CompanyHiringAggregatesWork;
    companyHiringAggregatesWork.destroyAll({}, function(destroyError, destroyOutput) {
      // var inputFile = './commonValidation/company_hiring_aggregates.csv';
      var name = companyHiringData.fileDetails.name;
      var container = companyHiringData.fileDetails.container;
      var inputFile = './attachments/' + container + '/' + name;
      loadMassUploadHiringAggregatesData(inputFile, function(err, fileResponse) {
        if (err) {
          cb(err, null);
        } else {
          var createMass = require('../../commonCompanyFiles/company-hiring-aggregate.js').createCompanyHiringAggregate;
          for (var i = 0; i < fileResponse.length; i++) {
            var obj = {};
            obj = fileResponse[i];
            obj.rowNumber = i + 1;
            output.push(obj);
          }
          createMass(output, companyHiringData, function(createErr, createResponse) {
            if (createErr) {
              throwError(createErr, cb);
            } else {
              output = [];
              cb(null, createResponse);
            }
          });
        };
      });
    });
  };

  //comapny hiring mass upload method creation
  COMPANY_HIRING_AGGREGATES.remoteMethod('companyHiringMassUpload', {
    description: 'Send Valid assessment Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/companyHiringMassUpload',
      verb: 'POST',
    },
  });

  //To load mass upload data
  function loadMassUploadHiringAggregatesData(inputFile, cb) {
    var fs = require('fs');
    var parse = require('csv-parse');
    // var async = require('async');
    var parser = parse({
      delimiter: ',',
      columns: true,
    }, function(err, data) {
      //   async.eachSeries(data, function(line, callBc) {  });
      if (err) {
        cb(err, null);
      } else {
        cb(null, data);
      }
    });

    fs.createReadStream(inputFile).pipe(parser);
  }
};
