'use strict';

module.exports = function(NOTIFICATION_DETAILS) {
  NOTIFICATION_DETAILS.pullNotification = function(input, callBack) {
    var createPull = require('../../commonValidation/create-pull-notification.js').createPullNotification;
    createPull(input, function(error, output) {
      if (error) {
        callBack(error, null);
      } else {
        callBack(null, output);
      }
    });
  };
  NOTIFICATION_DETAILS.remoteMethod('pullNotification', {
    description: 'Send Valid Input ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/pullNotification',
      verb: 'POST',
    },
  });
};
