'use strict';
module.exports = function(COMPANY_CONTACT) {
  var server = require('../../server/server');
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwerror = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var Joi = require('joi');
  // remote method definition to create contact for particular company
  /**
 *createCompanyContact- To create company contact details
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  COMPANY_CONTACT.createCompanyContact = function(data, callBc) {
    if (data.companyId && data.contacts.length > 0) {
      var myArray = data.contacts;
      var iterateobj = {};
      var count = 0;
      for (var j = 0; j < myArray.length; j++) {
        iterateobj = myArray[j];
        if (myArray[j].primaryInd == 'Y') {
          iterateobj = myArray[j];
          count++;
        }
      }
      // console.log('this is count', count);
      if (count <= 1) {
        if (count == 1) {
          searchForInd(data, iterateobj, function(err, res) {
            if (err) {
              errorResponse('there was an error', callBc);
            } else {
              // console.log('sucess');
            }
          });
        }
        var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
        var inputFilterObject = {};
        inputFilterObject['companyId'] = data.companyId;
        var contactModel = server.models.Contact;
        var companyModel = server.models.Company;
        checkEntityExistence(companyModel, inputFilterObject, function(checkStatus) {
          if (checkStatus) {
            contactModel.create(data.contacts, function(error, contactRes) {
              if (error) {
                callBc(error, null);
              } else if (contactRes) {
              var persistContactDataInForeignEntity = require('../../ServicesImpl/ContactImpl/persistContactRelationData.js').persistContactDataInForeignEntity;
              var companyContactModel = server.models.CompanyContact;
              var inputObj = {};
              inputObj['companyId'] = Number(data.companyId);
              persistContactDataInForeignEntity(contactRes.length, contactRes, companyContactModel, inputObj, callBc);
            } else {
              errorResponse(callBc);
            }
            });
          } else {
            errorResponse(callBc);
          }
        });
      } else {
        throwerror('cant have two or more primary indicators', callBc);
      }
    } else {
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    COMPANY_CONTACT.find({'where': {'companyId': data.companyId, 'primaryInd': 'Y'}},
    function(err, res) {
      if (err) {
        errorResponse('campusId was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
        priCallback(null, res);
      } else {
        var update = {};
        update.primaryInd = 'N';
        COMPANY_CONTACT.updateAll({'companyId': data.companyId, 'primaryInd': 'Y'},
        update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to contact for particular company
  COMPANY_CONTACT.remoteMethod('createCompanyContact', {
    description: 'Useful to create contact for particular company',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{arg: 'data', type: 'object', required: true, http: {source: 'body'}},
    ],
    http: {
      path: '/createCompanyContact',
      verb: 'POST',
    },
  });
  //UpdateCompanyContact remote method starts here
  /**
 *updateCompanyContact- To get update compoany contact details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  COMPANY_CONTACT.updateCompanyContact = function(contactData, cb) {
    var companyUpdate = require('../../commonCompanyFiles/update-company-contact.js');
    if (contactData.primaryInd == 'Y') {
      searchForInd(contactData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    companyUpdate.updateCompanyContact(contactData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = contactData.primaryInd;
        COMPANY_CONTACT.updateAll({'companyId': contactData.companyId, 'contactId': contactData.contactId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };

  //UpdateCompanyContact method to update both company and campus contacts
  COMPANY_CONTACT.remoteMethod('updateCompanyContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCompanyContact',
      verb: 'PUT',
    },
  });
};
