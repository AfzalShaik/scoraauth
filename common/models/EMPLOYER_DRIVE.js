'use strict';
var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
var employerDriveDetailsJson = require('./EMPLOYER_DRIVE.json').properties;
var employerDriveCampusesJson = require('./EMPLOYER_DRIVE_CAMPUSES.json').properties;
delete employerDriveCampusesJson["empDriveId"];
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_DRIVE) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_DRIVE.observe('before save', function employerDriveBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empDriveId == undefined) {
      var employerDriveTypeCode = 'EMPLOYER_DRIVE_TYPE_CODE';
      var employerDriveTypObj = {};
      employerDriveTypObj['lookupValueId'] = ctx.instance.driveTypeValueId;
      lookupMethods.typeCodeFunction(employerDriveTypObj, employerDriveTypeCode, function(employerEventTypeCodeStatus) {
        if (employerEventTypeCodeStatus) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('EmployerEvent Creation Initiated');
          next();
        } else {
          errorFunction('employerEventTypeCode validation error', next);
        }
      });
    } else if (ctx.isNewInstance) {
      errorFunction('empDriveId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get employer drive details
  /**
   *getEmployerDriveDetails- To get employer drive details by taking required fields
   *@constructor
   * @param {number} empDriveId - Unique id for ecah and every drive
   * @param {function} callBc - deals with response
   */
  EMPLOYER_DRIVE.getEmployerDriveDetails = function(empDriveId, companyId, callBc) {
    var inputObj = {};
    var employerDriveModel = server.models.EmployerDrive;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (empDriveId || companyId) {
      inputObj['empDriveId'] = (empDriveId) ? empDriveId : undefined;
      inputObj['companyId'] = (companyId) ? companyId : undefined;
      var includeModels = ['employerDriveLookupValue'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, employerDriveModel, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer drive details
  EMPLOYER_DRIVE.remoteMethod('getEmployerDriveDetails', {
    description: 'To get employer drive details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerDriveDetails',
      verb: 'GET',
    },
  });
  //  emp drive remoteMethod
  /**
   *updateEmployerDrive- To update employer drive Details
   *@constructor
   * @param {object} driveData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EMPLOYER_DRIVE.updateEmployerDrive = function(driveData, cb) {
    var updateDrive = require('../../commonCompanyFiles/update-employer-drive.js');
    updateDrive.updateEmpDriveService(driveData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update emp drive remoteMethod
  EMPLOYER_DRIVE.remoteMethod('updateEmployerDrive', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerDrive',
      verb: 'PUT',
    },
  });


  var employerDriveCreation = function(employerDrive) {
    var employerDriveModel = server.models.EmployerDrive;
    return new Promise(function(resolve, reject) {
      employerDriveModel.create(employerDrive, function(err, employerDriveResp) {
        if (err) {
          reject(err);
        } else if (employerDriveResp) {
          resolve(employerDriveResp);
        }
      });
    });
  };
  var employerDriveCampusesCreation = function(employerDrive, employerDriveCampuses) {
    return new Promise(function(resolve, reject) {
      var async = require('async');

      var empDriveId = employerDrive[0].empDriveId;
      async.map(employerDriveCampuses, createIndividualemployerDriveCampuses.bind({
        empDriveId: empDriveId,
      }),
        function(error, employerDriveCampusesCreateResp) {
          if (error) {
            reject(error);
          } else if (employerDriveCampusesCreateResp) {
            var response = {};
            response.employerDrive = employerDrive;
            response.employerDriveCampuses = employerDriveCampusesCreateResp;
            resolve(response);
          }
        });
    });
  };

  function createIndividualemployerDriveCampuses(employerDriveCampus, callback) {
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    employerDriveCampus.empDriveId = this.empDriveId;
    employerDriveCampuses.create(employerDriveCampus, function(err, employerDriveCampusResp) {
      callback(null, employerDriveCampusResp);
    });
  }
  EMPLOYER_DRIVE.createCompleteEmployerDrive = function(inputData, cb) {
    if (inputData.employerDrive.length == 1 && inputData.employerDriveCampuses.length > 0) {
      validateModel(inputData.employerDrive, employerDriveDetailsJson, function(err, employerDriveValidationResp) {
        validateModel(inputData.employerDriveCampuses, employerDriveCampusesJson, function(error, employerDriveCampusesValidationResp) {
          if (employerDriveValidationResp && employerDriveCampusesValidationResp) {
            employerDriveCreation(inputData.employerDrive).then(function(employerDriveResolveResp) {
              employerDriveCampusesCreation(employerDriveResolveResp, inputData.employerDriveCampuses).then(function(employerDriveCampusesResolveResp) {
                cb(null, employerDriveCampusesResolveResp);
              }, function(employerDriveCampusesRejectResp) {
                cb(employerDriveCampusesRejectResp, null);
              });
            }, function(employerDriveRejectResp) {
              cb(employerDriveRejectResp, null);
            });
          } else {
            errorResponse(cb);
          }
        });
      });
    }
  };
  //Create employerDrive
  EMPLOYER_DRIVE.remoteMethod('createCompleteEmployerDrive', {
    description: 'createCompleteEmployerDrive ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createCompleteEmployerDrive',
      verb: 'POST',
    },
  });
};
