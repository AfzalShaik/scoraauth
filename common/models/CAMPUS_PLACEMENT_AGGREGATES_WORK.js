'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
process.setMaxListeners(0);
module.exports = function(CAMPUS_PLACEMENT_AGGREGATES_WORK) {
  //mass upload remote method starts here
  var async = require('async');
  CAMPUS_PLACEMENT_AGGREGATES_WORK.campusPlacementUpload = function(inputData, cb) {
    // var inputFile = './commonValidation/campus_placement_aggregates.csv';
    var name = inputData.fileDetails.name;
    var container = inputData.fileDetails.container;
    var inputFile = './attachments/' + container + '/' + name;
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
    var campusAggregateJson = require('./CAMPUS_PLACEMENT_AGGREGATES_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    var campusPlacementAggregates = server.models.CampusPlacementAggregates;
    var campusPlacementAggregatesWork = server.models.CampusPlacementAggregatesWork;
    CAMPUS_PLACEMENT_AGGREGATES_WORK.destroyAll({}, function(error, destroy) {
      readCsvFile(inputFile, function(err, fileResponse) {
        if (err) {
          cb(err, null);
        } else {
          var createMass = require('../../commonCampusFiles/campus-placement-aggregate.js').createCampusPlacementAggregate;
          for (var i = 0; i < fileResponse.length; i++) {
            var obj = {};
            obj = fileResponse[i];
            obj.rowNumber = i + 1;
            output.push(obj);
          }
          createMass(output, inputData, function(createErr, createResponse) {
            if (createErr) {
              throwError(createErr, cb);
            } else {
              output = [];
              cb(null, createResponse);
            }
          });
        }
      });
    });
  };
  //event test upload method creation
  CAMPUS_PLACEMENT_AGGREGATES_WORK.remoteMethod('campusPlacementUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/campusPlacementUpload',
      verb: 'POST',
    },
  });
};
