'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var async = require('async');
var empId = [];
var skills = [];
var interests = [];
module.exports = function(CAMPUS_EVENT_STUDENT_SEARCH_VW) {
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  // remote method definition to get campusDetails item details
  /**
   *searchCampus- To search campus based on given parameters
   *@constructor
   * @param {number} studentId - unique id of campus
   * @param {number} obj.programId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getOnCampusSearch = function(obj, callBc) {
    if (obj) {
      var campusObj = {};
      campusObj['studentId'] = (obj.studentId) ? obj.studentId : undefined;
      campusObj['programCatValueId'] = (obj.programCatValueId) ? obj.programCatValueId : undefined;
      campusObj['programClassValueId'] = (obj.programClassValueId) ? obj.programClassValueId : undefined;
      campusObj['programTypeValueId'] = (obj.programTypeValueId) ? obj.programTypeValueId : undefined;
      campusObj['programMajorValueId'] = (obj.programMajorValueId) ? obj.programMajorValueId : undefined;
      campusObj['highlights'] = (obj.highlights) ? obj.highlights : undefined;
      campusObj['cgpaScore'] = (obj.cgpaScore) ? obj.cgpaScore : undefined;
      var campusEventStudentSearchVw = server.models.CampusEventStudentSearchVw;
      campusEventStudentSearchVw.find({
        'where': {
          'and': [campusObj],
        },
      }, function(err, campusSearch) {
        if (err) {
          errorFunction(err, callBc);
        } else if (campusSearch.length > 0) {
          async.map(campusSearch, oncampusData, function(error, placementInfo) {
            // console.log(placementInfo);

            callBc(null, placementInfo);
          });
        } else {
          errorFunction('Records not found', callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getOnCampusSearch', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getOnCampusSearch',
      verb: 'POST',
    },
  });
  CAMPUS_EVENT_STUDENT_SEARCH_VW.searchData = function(obj, cb) {
    if (obj) {
      var campusObj = {};
      campusObj['studentId'] = (obj.studentId) ? obj.studentId : undefined;
      campusObj['departmentId'] = (obj.departmentId) ? obj.departmentId : undefined;
      campusObj['programId'] = (obj.programId) ? obj.programId : undefined;
      var campusEventStudentSearchVw = server.models.CampusEventStudentSearchVw;
      campusEventStudentSearchVw.find({
        'where': {
          'and': [campusObj],
        },
      }, function(err, campusSearch) {
        if (err) {
          errorFunction(err, cb);
        } else if (campusSearch.length > 0) {
          console.log(campusSearch);
          async.map(campusSearch, getStudentIntrestsAndSkills, function(error, placementInfo) {
            // console.log(placementInfo);
            cb(null, placementInfo);
          });
        } else {
          errorFunction('Records not found', cb);
        }
      });
    } else {
      errorResponse(cb);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('searchData', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/searchData',
      verb: 'POST',
    },
  });
  // function oncampusData(obj, cb) {
  //   var studentSkills = server.models.StudentSkills;
  //   var studentInterests = server.models.StudentInterests;
  //   var studentDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
  //   var studentObj = {};
  //   studentObj['studentId'] = obj.studentId;
  //   console.log('studentObjstudentObjstudentObjstudentObj ', studentObj);
  //   studentDetails(studentObj, studentSkills, function(error, skillsOut) {
  //     if (error) {
  //       cb(error, cb);
  //     } else {
  //       //obj.skills = (skillsOut) ? skillsOut.data : [];
  //      // console.log(obj);
  //       studentDetails(studentObj, studentInterests, function(error, interestsOut) {
  //         if (error) {
  //           cb(error, null);
  //         } else {
  //           var studentDetails = {};
  //           studentDetails.skills = skillsOut;
  //           studentDetails.intrests = interestsOut;
  //           cb(null, studentDetails);
  //         }
  //         //obj.interests = (interestsOut) ? interestsOut.data : null;
  //       });
  //     }
  //   });
  // }
  function oncampusData(obj, cb) {
    console.log(obj);
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    var studentObj = {};
    //studentObj['studentId'] = obj.studentId;
    studentSkills.find({'where': {'studentId': obj.studentId}}, function(err, skills) {
      if (err) {
        errorFunction('error in finding skills', cb);
      } else {
        var async = require('async');
        async.map(skills, getSkillValue, function(err, skillsOfStudent) {
          if (err) {

          } else {
            studentInterests.find({'where': {'studentId': obj.studentId}}, function(error, intrests) {
              if (err) {

              } else {
                async.map(intrests, getIntrestsValue, function(err, intrestOfStudents) {
                  if (err) {

                  } else {
                    var final = skillsOfStudent.concat(intrestOfStudents);
                    cb(null, final);
                  }
                });
              }
            });
          }
        });
      }
    });
  }
    // studentDetails(studentObj, studentSkills, function(error, skillsOut) {
    //   if (error) {
    //     cb(error, cb);
    //   } else {
    //     //obj.skills = (skillsOut) ? skillsOut.data : [];
    //    // console.log(obj);
    //     studentDetails(studentObj, studentInterests, function(error, interestsOut) {
    //       if (error) {
    //         cb(error, null);
    //       } else {
    //         var studentDetails = {};
    //         studentDetails.skills = skillsOut;
    //         studentDetails.intrests = interestsOut;
    //         cb(null, studentDetails);
    //       }
    //       //obj.interests = (interestsOut) ? interestsOut.data : null;
    //     });
    //   }
    // });

  // function oncampusData(obj, cb) {
  //   var studentSkills = server.models.StudentSkills;
  //   var studentInterests = server.models.StudentInterests;
  //   var studentDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
  //   var studentObj = {};
  //   studentObj['studentId'] = obj.studentId;
  //   console.log('studentObjstudentObjstudentObjstudentObj ', studentObj);
  //   studentDetails(studentObj, studentSkills, function(error, skillsOut) {
  //     if (error) {
  //       cb(error, cb);
  //     } else {
  //       // obj.skills = (skillsOut) ? skillsOut.data : [];
  //       // console.log(obj);
  //       // studentDetails(studentObj, studentInterests, function(error, interestsOut) {
  //       //   obj.interests = (interestsOut) ? interestsOut.data : null;
  //       cb(null, skillsOut);
  //       // });
  //     }
  //   });
  // }

  function getDrives(campusDrives, obj, callBc) {
    var campus = server.models.Campus;
    var enrollmentModel = server.models.Enrollment;
    var campusObj = {};
    campusObj['obj.studentId'] = obj.studentId;
    campus.findOne({
      'where': {
        'and': [campusObj],
      },
    }, function(err, campusDetails) {
      enrollmentModel.find({
        'where': {
          'and': [campusObj],
        },
      }, function(errEnroll, enrollmentDetails) {
        var start = enrollmentDetails[0];
        start = (enrollmentDetails.length > 0) ? enrollmentDetails[0].startDate : new Date();
        var actual = (enrollmentDetails.length > 0) ? enrollmentDetails[0].planedCompletionDate : new Date();
        var currentDate = new Date();
        var enrollmentArray = [];
        enrollmentArray = (start.getTime() < currentDate.getTime() && actual.getTime() > currentDate.getTime()) ? enrollmentDetails.length : 0;
        obj.activeoncampusData = 'Active';
        obj.numberOfStudents = campusDetails.numberOfStudents;
        obj.studentsAvailable = enrollmentArray;
        callBc(null, obj);
      });
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getAllStudents = function(empEventId, cb) {
    var event = server.models.EventStudentList;
    event.find({'where': {'employerEventId': empEventId}}, function(err, studentsRes) {
      if (err) {

      } else {
        //console.log(studentsRes);
        empId[0] = empEventId;
        var async = require('async');
        async.map(studentsRes, getStudentDetails, function(err1, res) {
          if (err1) {

          } else {
            cb(null, res);
          }
        });
      }
    });
  };
  function getStudentDetails(ob, cb1) {
    getStudentIntrestsAndSkills(ob.studentId, function(error, finalResp) {
      if (error) {
        errorFunction('error occured', cb);
      } else {
        var data = {};
        data.studentDetails = ob;
        data.studentFullDetails = finalResp;
        cb1(null, data);
      }
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.getSkills = function(studentId, cb) {
    getStudentIntrestsAndSkills(studentId, function(err, res) {
      if (err) {
        errorFunction('err', cb);
      } else {
        cb(null, res);
      }
    });
  };
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getSkills', {
    description: 'Send Valid Details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getSkills',
      verb: 'GET',
    },
  });
  function getStudentIntrestsAndSkills(obj, callBack) {
    var studentSkills = server.models.StudentSkills;
    var studentInterests = server.models.StudentInterests;
    //var student = server.models.Student;
    CAMPUS_EVENT_STUDENT_SEARCH_VW.findOne({'where': {'studentId': obj.studentId}}, function(err, studentRes) {
      if (err) {
        errorFunction('err occured', callBack);
      } else {
        studentSkills.find({'where': {'studentId': obj.studentId}}, function(er, skillResponse) {
          if (er) {
            errorFunction('an error', callBack);
          } else {
            var async = require('async');
            async.map(skillResponse, getSkillValue, function(err, skillsOfStudent) {
              if (err) {

              } else {
                studentInterests.find({'where': {'studentId': obj.studentId}}, function(error, intrests) {
                  if (err) {

                  } else {
                    async.map(intrests, getIntrestsValue, function(err, intrestOfStudents) {
                      if (err) {

                      } else {
                        var skillsAndIntrests = {};
                        skillsAndIntrests.studentInfo = studentRes;
                        getObject(skillsOfStudent, function(err, res) {
                          if (err) {

                          } else {
                            getObject1(intrestOfStudents, function(err, res1) {
                              skillsAndIntrests.skills = skills;
                              skillsAndIntrests.intrests = interests;
                      //var final = skillsOfStudent.concat(intrestOfStudents);
                              callBack(null, skillsAndIntrests);
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  CAMPUS_EVENT_STUDENT_SEARCH_VW.remoteMethod('getAllStudents',
    {
      description: 'Provide Student Id ',
      returns: {
        arg: 'data',
        type: 'object',
      },
      accepts: [{
        arg: 'empEventId',
        type: 'number',
        http: {
          source: 'query',
        },
      }],
      http: {
        path: '/getAllStudents',
        verb: 'GET',
      },
    });
  function getObject(array, skillintcb) {
    skills = [];
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        skills.push(array[i][j]);
      }
    }
    skillintcb(null, 'done');
  }
  function getObject1(array, Intcb) {
    interests = [];
    for (var i = 0; i < array.length; i++) {
      for (var j = 0; j < array[i].length; j++) {
        interests.push(array[i][j]);
      }
    }
    Intcb(null, 'done');
  }
  function getSkillValue(obj, callBc) {
    lookup('SKILL_TYPE_CODE', obj.skillTypeValueId, function(err, skillResponse) {
      if (err) {
        errorFunction('err', callBc);
      } else {
        callBc(null, skillResponse);
      }
    });
  }
  function getIntrestsValue(obj1, callBack) {
    lookup('INTEREST_TYPE_CODE', obj1.interestTypeValueId, function(err, intrestResponse) {
      if (err) {
        errorFunction('err', callBack);
      } else {
        callBack(null, intrestResponse);
      }
    });
  }
  function lookup(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
      //var type = 'SKILL_TYPE_CODE';
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValueId: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', re1);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
};
