'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var final = [];
var notInsertedIntoDataBase = [];
var inserteddata1 = [];
var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
module.exports = function(UNREGISTER_CAMPUS_STUDENT_WORK) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var async = require('async');
  var unsucess = 0;
  var sucess = 0;
  UNREGISTER_CAMPUS_STUDENT_WORK.unregisteredCampusStudentUpload = function(stdata, cb) {
    var name = stdata.fileDetails.name;
    var container = stdata.fileDetails.container;
    var inputFile = './attachments/' + container + '/' + name;
    var employerevent = server.models.EmployerEvent;
    employerevent.find({'where': {'empEventId': stdata.employerEventId}}, function(err, res) {
      if (err) {
        errorResponse('An error occured', cb);
      } else if (res == 0) {
        errorResponse('Employer event id is invalid', cb);
      } else {
        var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
        var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
        var validateModel = require('../../ServicesImpl/CommonImpl/models-validation.js').validateInput;
        var unregisteredCampusStudentJson = require('./UNREGISTER_CAMPUS_STUDENT.json');
        var logger = require('../../server/boot/lib/logger');
        readCsvFile(inputFile, function(err, fileResponse) {
          if (err) {
            cb(err, null);
          } else {
            validateModel(fileResponse, unregisteredCampusStudentJson, function(error, response) {
              if (error) {
                throwError(JSON.stringify(error), cb);
              } else {
                async.map(fileResponse, createDetails, function(err, res) {
                  if (err) {
                    errorResponse('err', cb);
                  } else {
                    var async = require('async');
                    async.map(inserteddata1, createEvent,
                          function(err, response1) {
                            if (err) {
                              cb(err, null);
                            } else {
                              var companyFileUpload = server.models.CompanyUploadLog;
                              var upload = {};
                              upload.compUploadTypeValueId = stdata.compUploadTypeValueId;
                              upload.companyId = stdata.companyId;
                              upload.inputParameters = stdata.inputParameters;
                              upload.createUserId = stdata.createUserId;
                              upload.createDatetime = new Date();
                              upload.csvFileLocation = inputFile;
                              upload.errorFileLocation = inputFile;
                              upload.totalNoRecs = unsucess + sucess;
                              upload.noSuccessRecs = sucess;
                              upload.noFailRecs = unsucess;
                              companyFileUpload.create(upload, function(err, uploadRes) {
                                if (err) {
                                  errorResponse('cant create an event', cb);
                                } else {
                                  cb(null, uploadRes);
                                }
                              });
                            }
                          });
                  }
                });
              }
            });
          }
        });
      }
    });
    function createEvent(eventData, eventCallback) {
      var eventStudent = server.models.EventStudentList;
      var date = new Date();
      var input = {

        'studentId': eventData.unregStudentId,
        'companyId': stdata.companyId,
        'employerEventId': stdata.employerEventId,
        'campusId': 113914986,
        'campusEventId': 1,
        'candidateStatusValueId': 1,
        'campusComments': 'string',
        'employerComments': 'string',
        'createDatetime': date,
        'updateDatetime': date,
        'createUserId': stdata.createUserId,
        'updateUserId': stdata.createUserId,
        'eligibilityInd': 'y',
        'studentSubscribeInd': 'y',
        'campusPublishInd': 'y',
        'offerStatusValueId': 1,
        'compPackageId': null,
      };
      eventStudent.create(input, function(err, res) {
        if (err) {
          errorResponse(err, eventCallback);
        } else {
          sucess++;
          eventCallback(null, res);
        }
      });
    }
    function createDetails(data, callb) {
      var skippedDetails = [];
      var inserteddata = [];
      var count = 0;
      var indicator = 0;
      checkexistense(data, function(err, status) {
        if (status == 'error') {
          errorResponse('an error occured', cb);
        } else if (status == 'false') {
          var data1 = {};
          data1.empEventId = data.empEventId;
          data1.searchName = data.searchName;
          data1.firstName = data.firstName;
          data1.lastName = data.lastName;
          data1.middelName = data.middleName;
          data1.gender = data.gender;
          data1.DateOfBrith = data.dateOfBirth;
          data1.highlights = data.highlights;
          data1.highlights = data.highlights;
          data1.phoneNo = data.phoneNo;
          data1.emailId = data.emailId;
          data1.error = 'DuplicateEntry';
          notInsertedIntoDataBase.push(data1);
          failed(notInsertedIntoDataBase);
          callb(null, 0);
        } else {
          data.createDatetime = new Date();
          data.updateDatetime = new Date();
          var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
          UnregisterCampusStudent.create(data, function(err, response) {
            if (err) {
              errorResponse('there was error in creating', callb);
            } else {
              inserteddata[indicator] = response;
              indicator = indicator + 1;
              inserteddata1.push(response);
              callb(null, inserteddata);
            }
          });
        }
      });
    }
    function failed(data) {
      unsucess++;
      var fs = require('fs');
      var csv = require('fast-csv');
      var ws = fs.createWriteStream(inputFile);
      csv
    .writeToPath(inputFile, data, {headers: true})
    .on('finish', function() {
    });
    }
  };
  UNREGISTER_CAMPUS_STUDENT_WORK.remoteMethod('unregisteredCampusStudentUpload', {
    description: 'Send Valid Data ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/unregisteredCampusStudentUpload',
      verb: 'POST',
    },
  });
  function checkexistense(data, existCallBack) {
    var obj = {};
    obj.firstName = data.firstName;
    obj.lastName = data.lastName;
    obj.middleName = data.middleName;
    obj.emailId = data.emailId;
    obj.phoneNo = data.phoneNo;
    var UnregisterCampusStudent = server.models.UnregisterCampusStudent;
    UnregisterCampusStudent.find({'where': {
      'and': [obj],
    },
    }, function(err, checkResponse) {
      if (err) {
        var status = 'error';
        existCallBack(null, status);
      } else if (checkResponse == 0) {
        var  status = 'true';
        existCallBack(null, checkResponse);
      } else {
        var status = 'false';
        existCallBack(null, status);
      }
    });
  }
};
