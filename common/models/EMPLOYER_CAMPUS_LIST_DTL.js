'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(EMPLOYER_CAMPUS_LIST_DTL) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_CAMPUS_LIST_DTL.observe('before save', function employerCampusListDTLBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.listCampusId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      logger.info('EmployerCampusListDTL Creation Initiated');
      next();
    } else if (ctx.isNewInstance) {
      errorFunction('EmployerCampusListDTL listCampusId is system generated value', next);
    } else {
      next();
    }
  });

  // remote method definition to get employer campus list details
  /**
   *getEmployerCampusListDtl- To get employer details taking required fields
   *@constructor
   * @param {object} listCampusId - Unique id for ecah and campus
   * @param {function} callBc - deals with response
   */
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerCampusListDtl = function(listId, listCampusId, callBc) {
    var inputObj = {};
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var employerCampusListDtlDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (listId || listCampusId) {
      inputObj['listId'] = listId;
      inputObj['listCampusId'] = (listCampusId) ? listCampusId : undefined;
      var includeModels = ['employerCampusListDTLCompany', 'employerCampusListHdrData', 'employerCampusListDtlCampus'];
      // below function will give details for an entity based on loopback include filter
      employerCampusListDtlDetails(inputObj, employerCampusListDtl, includeModels, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer campus list details
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerCampusListDtl', {
    description: 'To get employer campus list details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'listId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'listCampusId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerCampusListDtl',
      verb: 'GET',
    },
  });
  //UpdateEmployerCampusListDtl remote method starts here
  /**
   *updateEmployerCampusListDtl- To update employer campus list details
   *@constructor
   * @param {object} programData - contains all the data need to get updated
   * @param {function} cb - deals with response
   */
  EMPLOYER_CAMPUS_LIST_DTL.updateEmployerCampusListDtl = function(programData, cb) {
    var programUpdate = require('../../commonCompanyFiles/update-emp-campus-list-dtl');
    programUpdate.updateEmpCampusProgramService(programData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //UpdateEmployerCampusListDtl method creation
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('updateEmployerCampusListDtl', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerCampusListDtl',
      verb: 'PUT',
    },
  });

  // remote method definition to get employer campus list hdr and dtl details
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerCampusList = function(jobRoleId, empDriveId, callBc) {
    var inputObj = {};
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    var employerDriveCampuses = server.models.EmployerDriveCampuses;
    var entityDetailsById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (jobRoleId || empDriveId) {
      inputObj['jobRoleId'] = (jobRoleId) ? jobRoleId : undefined;
      inputObj['empDriveId'] = (empDriveId) ? empDriveId : undefined;
      var modelsName = (jobRoleId) ? employerCampusListHdr : employerDriveCampuses;
      getDrivesList(inputObj, modelsName, function(err, response) {
        if (err) {
          errorFunction(err, callBc);
        } else {
          var hrdResponse = response.data;
          async.map(hrdResponse, getCampusList, function(error, campusList) {
            var output = [];
            output = cleanArray(campusList);
            async.map(output, getCampusId, function(finalErr, finalOutput) {
              var outputArray = [];
              outputArray = cleanArray(finalOutput);
              if (finalErr) {
                errorFunction(finalErr, callBc);
              } else {
                callBc(null, outputArray);
              }
            });
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };

  function getCampusList(obj, callBack) {
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var inputObj = {};
    var findCampusList = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    inputObj['listId'] = obj.listId;
    employerCampusListDtl.findOne({
      'where': {
        'listId': obj.listId,
      },
    }, function(rrr, resp) {
      if (typeof resp !== 'undefined') {
        // the array is defined and has at least one element
        callBack(null, resp);
      } else {
        callBack(null, null);
      }
    });
  }

  function getCampusId(obj, callBc) {
    var campusSearchVw = server.models.CampusSearchVw;
    var searchCampus = campusSearchVw.searchCampus;
    var employerCampusListDtl = server.models.EmployerCampusListDtl;
    var employerCampusListHdr = server.models.EmployerCampusListHdr;
    searchCampus(obj.campusId, undefined, undefined, undefined, undefined, undefined, undefined, undefined, function(err, searchResponse) {
      employerCampusListHdr.findOne({'where': {'listId': obj.listId}}, function(listErr, listResp) {
        var campusHdr = {};
        campusHdr.campusDetails = searchResponse[0];
        campusHdr.EmployerCampusListHdr = listResp;
        callBc(null, campusHdr);
      });
    });
  }
  // remote method definition to get employer campus list hdr and dtl details
  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerCampusList', {
    description: 'To get employer campus list details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'jobRoleId',
      type: 'number',
      http: {
        source: 'query',
      },
    }, {
      arg: 'empDriveId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getEmployerCampusList',
      verb: 'GET',
    },
  });
  // getEmployerList method to get list info based on campusId
  EMPLOYER_CAMPUS_LIST_DTL.getEmployerList = function(campusArray, callBack) {
    if (campusArray) {
      async.map(campusArray.campusList, getListData, function(err, listResponse) {
        if (err) {
          errorFunction(err, callBack);
        } else {
          callBack(null, listResponse);
        }
      });
    } else {
      errorFunction('Invalid Input', callBack);
    }

    function getListData(obj, cb) {
      EMPLOYER_CAMPUS_LIST_DTL.find({
        'where': {
          'campusid': obj.campusId
        }
      }, function(listErr, listResp) {
        if (listErr) {
          errorFunction(listErr, cb);
        } else {
          cb(null, listResp);
        }
      });
    }
  };

  EMPLOYER_CAMPUS_LIST_DTL.remoteMethod('getEmployerList', {
    description: 'Send Valid Details ',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getEmployerList',
      verb: 'POST',
    },
  });
  
  function getDrivesList(inputObj, modelsName, callBack) {
    console.log(inputObj);
    var entityDetailsById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    entityDetailsById(inputObj, modelsName, function(err, response) {
      if (err) {
        errorFunction(err, callBack);
      } else {
        callBack(null, response);
      }
    });
  }
};
