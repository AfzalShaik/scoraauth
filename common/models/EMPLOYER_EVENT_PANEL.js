'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
module.exports = function(EMPLOYER_EVENT_PANEL) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  var lookupMethods = require('../../commonValidation/lookupMethods');
  EMPLOYER_EVENT_PANEL.observe('before save', function employerEventPanelBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.empEventPanelId == undefined) {
          ctx.instance.updateUserId = ctx.instance.createUserId;
          ctx.instance.createDatetime = new Date();
          ctx.instance.updateDatetime = new Date();
          logger.info('EmployerEventPanel Creation Initiated');
          next();
    } else if (ctx.isNewInstance) {
      errorFunction('empEventPanelId is system generated value', next);
    } else {
      next();
    }
  });
  // remote method definition to get employer event panel details
    /**
 *getEmployerEventPanelDetails- To get employer event details by taking required fields
 *@constructor
 * @param {number} empEventId - Unique id for ecah and every employer
 * @param {number} empEventPanelId - unique id for event panel
 * @param {function} callBc - deals with response
 */
  EMPLOYER_EVENT_PANEL.getEmployerEventPanelDetails = function(empEventId, empEventPanelId, callBc) {
    var inputObj = {};
    var employerEventPanelModel = server.models.EmployerEventPanel;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (empEventId && empEventPanelId) {
      inputObj['empEventId'] = empEventId;
      inputObj['empEventPanelId'] = empEventPanelId;
      var includeModels = ['employerEvent','empEvntPnlEmployerPerson'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, employerEventPanelModel, includeModels, callBc);
    }else if (empEventId) {
      inputObj['empEventId'] = empEventId;
      var includeModels = ['employerEvent','empEvntPnlEmployerPerson'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, employerEventPanelModel, includeModels, callBc);
    }
    else {
      errorResponse(callBc);
    }
  };
  // remote method definition to get employer event panel details
  EMPLOYER_EVENT_PANEL.remoteMethod('getEmployerEventPanelDetails', {
    description: 'To get employer event panel details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
      {
        arg: 'empEventPanelId',
        type: 'number',
        http: {
          source: 'query',
        },
      }
    ],
    http: {
      path: '/getEmployerEventPanelDetails',
      verb: 'GET',
    },
  });
  //  emp drive remoteMethod
   /**
 *updateEmployerEventPanel- To update employer event panel Details
 *@constructor
 * @param {object} panelData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EMPLOYER_EVENT_PANEL.updateEmployerEventPanel = function(panelData, cb) {
    var updatePanel = require('../../commonCompanyFiles/update-employer-event-panel.js');
    updatePanel.updateEmpPanelService(panelData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update emp drive remoteMethod
  EMPLOYER_EVENT_PANEL.remoteMethod('updateEmployerEventPanel', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployerEventPanel',
      verb: 'PUT',
    },
  });
};
