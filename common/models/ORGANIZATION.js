'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var Joi = require('joi');

module.exports = function(ORGANIZATION) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  ORGANIZATION.observe('before save', function organizationBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
    /**
 *getOrganizationDetails - To get Organization details by taking required fields
 *@constructor
 * @param {object} organizationId - Unique id for ecah and every Organization
 * @param {number} companyId - unique id for each and every company
 * @param {function} callBc - deals with response
 */// remote method definition to get organization details based on on organizationId and companyId

  ORGANIZATION.getOrganizationDetails = function(organizationId, companyId, callBc) {
    var inputObj = {};
    var organization = server.models.Organization;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (organizationId && companyId) {
      inputObj['companyId'] = companyId;
      inputObj['organizationId'] = organizationId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, organization, 'organizationCompanyDetails', callBc);
    } else if (companyId) {
      inputObj['companyId'] = companyId;
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, organization, 'organizationCompanyDetails', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get organization details based on on organizationId and companyId
  ORGANIZATION.remoteMethod('getOrganizationDetails', {
    description: 'To get organization details based on on organizationId and companyId',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'organizationId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getOrganizationDetails',
      verb: 'GET',
    },
  });
  //UpdateOrganization custom method starts here
   /**
 *updateOrganization- To update Organization Details
 *@constructor
 * @param {object} orgData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  ORGANIZATION.updateOrganization = function(orgdata, cb) {
    var updateOrg = require('../../commonCampusFiles/update-organization-profile');
    updateOrg.updateOrganizationProfile(orgdata, function(err, resp) {
      logger.info('upating Organization Profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //remoteMethod for Organization update
  ORGANIZATION.remoteMethod('updateOrganization', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateOrganization',
      verb: 'PUT',
    },
  });
};
