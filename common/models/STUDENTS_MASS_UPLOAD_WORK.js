'use strict';
module.exports = function(STUDENTS_MASS_UPLOAD_WORK) {
  //mass upload remote method starts here
  var server = require('../../server/server');
  var async = require('async');
  STUDENTS_MASS_UPLOAD_WORK.studentMassUpload = function(stDataa, cb) {
    // var inputFile = './commonValidation/students_mass_upload.csv';
    var name = stDataa.fileDetails.name;
    var container = stDataa.fileDetails.container;
    var inputFile = './attachments/' + container + '/' + name;
    var readCsvFile = require('../../commonValidation/common-mass-upload.js').readFile;
    var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
    var studentMassJson = require('./STUDENTS_MASS_UPLOAD_WORK.json');
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var logger = require('../../server/boot/lib/logger');
    var output = [];
    STUDENTS_MASS_UPLOAD_WORK.destroyAll({}, function(destroyError, destroyOutput) {
      readCsvFile(inputFile, function(err, fileResponse) {
        if (err) {
          cb(err, null);
        } else {
          var createMass = require('../../commonCampusFiles/student-mass-upload.js').createMassUpload;
          for (var i = 0; i < fileResponse.length; i++) {
            var obj = {};
            obj = fileResponse[i];
            obj.rowNumber = i + 1;
            obj.campusId = stDataa.campusId;
            obj.programId = stDataa.programId;
            output.push(obj);
          }
          createMass(output, stDataa, function(createErr, createResponse) {
            if (createErr) {
              throwError(createErr, cb);
            } else {
              output = [];
              cb(null, createResponse);
            }
          });
        }
      });
    });
  };

  //mass upload method creation
  STUDENTS_MASS_UPLOAD_WORK.remoteMethod('studentMassUpload', {
    description: 'Send Valid Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentMassUpload',
      verb: 'POST',
    },
  });
};
