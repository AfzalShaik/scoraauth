'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');

module.exports = function(STUDENT_SKILLS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to student-skills model then we execute logic or change request object before saving into database
  STUDENT_SKILLS.observe('before save', function studentSkillBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  // remote method definition to get student address details
    /**
 *getStudentSkill- To get student skill details by taking required fields
 *@constructor
 * @param {number} studentId - Unique id for each and every student
 * @param {function} callBc - deals with response
 */
  STUDENT_SKILLS.getStudentSkill = function(studentId, callBc) {
    var inputObj = {};
    var studentSkill = server.models.StudentSkills;

    var studentSkillsDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId) {
      inputObj['studentId'] = studentId;
      // below function will give student skill details for an entity based on loopback include filter
      studentSkillsDet(inputObj, studentSkill, 'studentSkill', callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to get student skill details
  STUDENT_SKILLS.remoteMethod('getStudentSkill', {
    description: 'To get student skill details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }, ],
    http: {
      path: '/getStudentSkill',
      verb: 'GET',
    },
  });

  // remote method definition to delete student address details
    /**
 *deleteStudentSkill- To delete student skills details by taking required fields
 *@constructor
 * @param {object} studentIdId - Unique id for ecah and every student
 * @param {number} skillTypeValueIdId - unique id for each and every skill
 * @param {function} callBc - deals with response
 */
  STUDENT_SKILLS.deleteStudentSkill = function(studentId, skillTypeValueId, callBc) {
    if (studentId && skillTypeValueId) {
      var studentSkillModel = server.models.StudentSkills;
      //   var addressModel = server.models.Address;
      var deleteStudentSkill = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteSingleEntityDetails;
      var inputFilterObject = {};
      inputFilterObject['studentId'] = studentId;
      inputFilterObject['skillTypeValueId'] = skillTypeValueId;
      deleteStudentSkill(inputFilterObject, studentSkillModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };

  // remote method declaration to delete student address details
  STUDENT_SKILLS.remoteMethod('deleteStudentSkill', {
    description: 'To delete student skill',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'skillTypeValueId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteStudentSkill',
      verb: 'DELETE',
    },
  });
  // remote method definition to get campus details
    /**
 *searchSkills- To search skills based on required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every department
 * @param {number} skillTypeValueIdId - unique id for each and every skill
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  STUDENT_SKILLS.searchSkills = function(studentId, skillTypeValueId, callBc) {
    var inputObj = {};
    var studentSkillsModel = server.models.StudentSkills;
    var entityDetailsUsingIncludeFilter = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId || skillTypeValueId) {
      // console.log('0000000000000000000000 ', studentId);
      inputObj['studentId'] = (studentId) ? studentId : undefined;
      inputObj['skillTypeValueId'] = (skillTypeValueId) ? skillTypeValueId : undefined;
      var includeRelations = ['studentSkill', 'studentDetails'];
      // below function will give details for an entity based on loopback include filter
      entityDetailsUsingIncludeFilter(inputObj, studentSkillsModel, includeRelations, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get program details based on program id
  STUDENT_SKILLS.remoteMethod('searchSkills', {
    description: 'To get campus details based on program id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    ],
    http: {
      path: '/searchSkills',
      verb: 'GET',
    },
  });
};
