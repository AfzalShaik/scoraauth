'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EMPLOYER_PERSON) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to campus model then we execute logic or change request object before saving into database
  EMPLOYER_PERSON.observe('before save', function employerPersonBeforeSave(ctx, next) {
    if (ctx.isNewInstance && ctx.instance.employerPersonId == undefined) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else if (ctx.isNewInstance) {
      errorResponse(next);
    } else {
      next();
    }
  });

  // remote method definition to get employer person details based on primary key(company id and program id)
    /**
 *getEmployerPerson- To get employer person details by taking required fields
 *@constructor
 * @param {number} employerPersonId - unique id for each and every person
 * @param {number} companyId - unique id for every company
 * @param {function} callBc - deals with response
 */
  EMPLOYER_PERSON.getEmployerPerson = function(employerPersonId, companyId, callBc) {
    if (employerPersonId && companyId) {
      var employerPersonDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['employerPersonId'] = employerPersonId;
      inputFilterObject['companyId'] = companyId;
      logger.info('fetching employerPerson data');
      var employerPerson = server.models.EmployerPerson;
      employerPersonDetById(inputFilterObject, employerPerson, callBc);
    } else {
      logger.error('error while fetching employer data');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get employer person details based on primary key(company id and program id)
  EMPLOYER_PERSON.remoteMethod('getEmployerPerson', {
    description: 'To get employer person details based on primary key(company id and program id)',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'employerPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEmployerPerson',
      verb: 'get',
    },
  });
  //  EMPLOYER_PERSON remoteMethod
   /**
 *updateEmployeePerson- To update Employee Person Details
 *@constructor
 * @param {object} personData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EMPLOYER_PERSON.updateEmployeePerson = function(personData, cb) {
    var personUpdate = require('../../commonCompanyFiles/update-employee-person.js');
    logger.info('updating employer person');
    personUpdate.employeePersonUpdate(personData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //update EMPLOYER_PERSON remoteMethod
  EMPLOYER_PERSON.remoteMethod('updateEmployeePerson', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployeePerson',
      verb: 'PUT',
    },
  });
};
