'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(UNREGISTER_CAMPUS_STUDENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  UNREGISTER_CAMPUS_STUDENT.observe('before save', function unregisterBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  UNREGISTER_CAMPUS_STUDENT.getunregstudentDetails = function(unregstudentid, callBc) {
    var inputObj = {};
    var unregister = server.models.UNREGISTERCAMPUSSTUDENT;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (unregstudentid) {
      inputObj['unregstudentid'] = unregstudentid;
      findEntity(inputObj, unregister, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else if (res == 0) {
          errorResponse('result not found Invalid Id', callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('result not found', callBc);
    } };
/**
 * remote method---
 * exe when ever unregstudentid was called
 * accpects-unregisterStudentId
 * gets-array
 */
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('getunregstudentDetails', {
    description: 'To get student unreg student details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'unregstudentid ',
             type: 'number',
             required: true,
             http: {source: 'query'}},
    http: {
      path: '/getunregstudentDetails',
      verb: 'get',
    },
  });
  UNREGISTER_CAMPUS_STUDENT.deleteunregisterStudentDetails = function(unregstudentid, callBc) {
    if (studentId) {
      var unregister = server.models.unregester;
      unregister.destroyById(unregstudentid, function(err, responseDelete) {
        if (err) {
          callBc(err, null);
        } else if (responseDelete.count == 0) {
          errorFunction('no student id with that was registered', callBc);
        } else {
          callBc(null, responseDelete);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus missing program details
  UNREGISTER_CAMPUS_STUDENT.remoteMethod('deleteunregisterStudentDetails', {
    description: 'To delete unregisterd student details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: {
      arg: 'unregstudentid',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    http: {
      path: '/deleteunregisterStudentDetails',
      verb: 'delete',
    },
  });
  UNREGISTER_CAMPUS_STUDENT.updateUnregisterStudentDetails = function(data, cb) {
    var unregesterStudentUpdate = require('../../update/update-unregister-student');
  };
}
;
