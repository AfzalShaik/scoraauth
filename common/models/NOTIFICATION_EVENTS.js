'use strict';
module.exports = function(NOTIFICATION_EVENTS) {
    //notificationEvents remote method starts here
      /**
 *getNotificationEvents- To get nodification by taking required fields
 *@constructor
 * @param {number} empEventId - Unique id for ecah and every employer
 * @param {function} callBc - deals with response
 */
  NOTIFICATION_EVENTS.getNotificationEvents = function(empEventId, cb) {
    var getNotification = require('../../commonCompanyFiles/get-notification-events');
    getNotification.notificationEvents(empEventId, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //notificationEvents method creation

  NOTIFICATION_EVENTS.remoteMethod('getNotificationEvents', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'empEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getNotificationEvents',
      verb: 'get',
    },
  });

  NOTIFICATION_EVENTS.getCsv = function(csvAddress, cb) {
    // console.log(csvAddress);
    var Client = require('ftp');
    var c = new Client();
    var path = require('path');
    var fullPathCollection = path.join(__dirname, 'event_test_score.csv');
    cb(null, JSON.stringify(fullPathCollection));
  };

  //notificationEvents method creation

  NOTIFICATION_EVENTS.remoteMethod('getCsv', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCsv',
      verb: 'POST',
    },
  });
};
