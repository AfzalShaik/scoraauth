'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(EMPLOYER_PERSON_ADDRESS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;

  // remote method definition to create address for employer person
  EMPLOYER_PERSON_ADDRESS.createEmployerPersonAddress = function(data, callBc) {
    if (data.employerPersonId && data.companyId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = data.companyId;
      inputFilterObject['employerPersonId'] = data.employerPersonId;
      var addressModel = server.models.Address;
      var employerPersonModel = server.models.EmployerPerson;
      checkEntityExistence(employerPersonModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          addressModel.create(data, function(error, addressRes) {
            if (error) {
              logger.error('error while creating addressModel method');
              callBc(error, null);
            } else if (addressRes) {
              EMPLOYER_PERSON_ADDRESS.create({
                'employerPersonId': Number(data.employerPersonId),
                'companyId': Number(data.companyId),
                'addressId': Number(addressRes.addressId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  logger.error('error while creating employer person address');
                  errorResponse(callBc);
                } else {
                  // addressRes.requestStatus = true;
                  logger.info('employer person address created successfully');
                  callBc(null, addressRes);
                }
              });
            } else {
              logger.error('error while creating address in employer person');
              errorResponse(callBc);
            }
          });
        } else {
          logger.error('entity is not found');
          errorResponse(callBc);
        }
      });
    } else {
      logger.error('companyId and employerPersonId are required');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    EMPLOYER_PERSON_ADDRESS.find({'where': {'employerPersonId': data.employerPersonId,
    'companyId': data.companyId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('company id or employer id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        EMPLOYER_PERSON_ADDRESS.updateAll({'employerPersonId': data.employerPersonId,
        'companyId': data.companyId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create address for employer person
  EMPLOYER_PERSON_ADDRESS.remoteMethod('createEmployerPersonAddress', {
    description: 'Useful to create address for employer person',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createEmployerPersonAddress',
      verb: 'post',
    },
  });
  // remote method definition to get  employer person address details
    /**
 *getEmployerPersonAddress- To get  employer person address details
 *@constructor
 * @param {object} employerPersonId - Unique id for ecah and every employer person
 * @param {number} companyId - unique id for each and every company
 * @param {number} addressId - unique id for every address
 * @param {function} callBc - deals with response
 */
  EMPLOYER_PERSON_ADDRESS.getEmployerPersonAddress = function(employerPersonId, companyId, addressId, callBc) {
    var inputObj = {};
    var employerPersonAddressModel = server.models.EmployerPersonAddress;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var employeePersonAddressDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (employerPersonId && companyId && addressId) {
      inputObj['employerPersonId'] = employerPersonId;
      inputObj['companyId'] = companyId;
      inputObj['addressId'] = addressId;
      logger.info('fetching employerPersonAddress');
      // below function will give address details for an entity based on loopback include filter
      employeePersonAddressDet(inputObj, employerPersonAddressModel, 'employerPersonAddress', callBc);
    } else if (employerPersonId && companyId) {
      inputObj['employerPersonId'] = employerPersonId;
      inputObj['companyId'] = companyId;
      // below function will give address details for an entity based on loopback include filter
      employeePersonAddressDet(inputObj, employerPersonAddressModel, 'employerPersonAddress', callBc);
    } else {
      logger.error('error while fetching employerPersonAddress');
      errorResponse(callBc);
    }
  };
  // remote method declaration to get employer person address details
  EMPLOYER_PERSON_ADDRESS.remoteMethod('getEmployerPersonAddress', {
    description: 'To get employer person address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'employerPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getEmployerPersonAddress',
      verb: 'get',
    },
  });
  //  EMPLOYER_PERSON remoteMethod
   /**
 *updateEmployeePersonAddress- To update Employee Person Address Details
 *@constructor
 * @param {object} personData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EMPLOYER_PERSON_ADDRESS.updateEmployeePersonAddress = function(personData, cb) {
    var personUpdate = require('../../commonCompanyFiles/update-employee-person-address.js');
    if (personData.primaryInd == 'Y') {
      searchForInd(personData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    logger.info('updating employeePersonAddress');
    personUpdate.employeePersonAddressUpdate(personData, function(err, resp) {
      if (err) {
        logger.error('error while updating employeePersonAddress');
        cb(err, resp);
      } else {
        var updatetData = {};
        updatetData.primaryInd = personData.primaryInd;
        EMPLOYER_PERSON_ADDRESS.updateAll({'companyId': personData.companyId,
        'addressId': personData.addressId, 'employerPersonId': personData.employerPersonId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            logger.info('updated EmployeePersonAddress successfully');
            cb(null, resp);
          }
        });
      }
    });
  };

  //update EMPLOYER_PERSON remoteMethod
  EMPLOYER_PERSON_ADDRESS.remoteMethod('updateEmployeePersonAddress', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateEmployeePersonAddress',
      verb: 'PUT',
    },
  });

  // remote method definition to deleteEmployeePersonAddress
   /**
 *deleteEmployeePersonAddress- To delete employee person Details
 *@constructor
 * @param {number} companyId - unique id for each and every company
 * @param {number} addressId - unique id for each and every address
 * @param {number} employerPersonId - unique id for each and every employer person
 * @param {function} callBc - deals with response
 */
  EMPLOYER_PERSON_ADDRESS.deleteEmployeePersonAddress = function(companyId, addressId, employerPersonId, callBc) {
    if (companyId && addressId && employerPersonId) {
      var employerPersonAddress = server.models.EmployerPersonAddress;
      var addressModel = server.models.Address;
      var deleteEmployeePersonAddressDet = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['addressId'] = addressId;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      inputFilterObject['addressId'] = addressId;
      inputFilterObject['employerPersonId'] = employerPersonId;
      logger.info('deleting employer person address');
      deleteEmployeePersonAddressDet(primaryCheckInput, inputFilterObject, addressModel, employerPersonAddress, callBc);
    } else {
      logger.error('error while deleting employerPersonAddress');
      errorResponse(callBc);
    }
  };

  // remote method declaration to deleteEmployeePersonAddress
  EMPLOYER_PERSON_ADDRESS.remoteMethod('deleteEmployeePersonAddress', {
    description: 'To delete employer person address',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'addressId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'employerPersonId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteEmployeePersonAddress',
      verb: 'delete',
    },
  });
};
