'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var companyDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var async = require('async');
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
module.exports = function(COMPANY_SEARCH_VW) {
  function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }
  // remote method definition to get companyDetails item details
  /**
   *searchCompany- To search campus based on given parameters
   *@constructor
   * @param {number} companyId - unique id of campus
   * @param {number} companySizeValueId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  COMPANY_SEARCH_VW.searchCompany = function(companyId, searchName, companySizeValueId, companyTypeValueId, industryTypeValueId, stateCode, cityId, rating, regionFlag, campusId, callBc) {
    if (companyId || searchName || companySizeValueId || companyTypeValueId || regionFlag || stateCode || cityId ||
      rating || regionFlag) {
      var companyObj = {};
      companyObj['companyId'] = (companyId) ? companyId : undefined;
      companyObj['companySizeValueId'] = (companySizeValueId) ? companySizeValueId : undefined;
      companyObj['companyTypeValueId'] = (companyTypeValueId) ? companyTypeValueId : undefined;
      companyObj['industryTypeValueId'] = (industryTypeValueId) ? industryTypeValueId : undefined;
      companyObj['cityId'] = (cityId) ? cityId : undefined;
      companyObj['stateCode'] = (stateCode) ? stateCode : undefined;
      companyObj['regionFlag'] = (regionFlag) ? regionFlag : undefined;
      companyObj['rating'] = (rating) ? rating : undefined;
      companyDetails(companyObj, COMPANY_SEARCH_VW, function(err, companySearch) {
        if (err) {
          errorFunction(err, callBc);
        } else if (companySearch.data.length > 0) {
          if (searchName) {
            searchCompanyName(searchName, companySearch.data, function(error, searchedData) {
              callBc(null, searchedData);
            });
          } else {
            async.map(companySearch.data, companyDriveRecruitment, function(error, placementInfo) {
              callBc(null, placementInfo);
            });
          }
        } else {
          errorFunction('Records not found', callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }

    function companyDriveRecruitment(obj, callBc) {
      var employerDrive = server.models.EmployerDrive;
      var lookup = require('../../commonValidation/lookupMethods').getLookupId;
      lookup('EMPLOYER_DRIVE_TYPE_CODE', 'Hiring', function(driveType) {
        lookup('EMPLOYER_DRIVE_STATUS_CODE', 'Approved', function(driveStatus) {
          var companyObj = {};
          companyObj['companyId'] = obj.companyId;
          companyObj['driveTypeValueId'] = driveType.lookupValueId;
          companyObj['driveStatusValueId'] = driveStatus.lookupValueId;
          employerDrive.find({
            'where': {
              'and': [companyObj],
            },
          }, function(err, companyDetails) {
            if (err) {
              errorFunction(err, cb);
            } else {
              obj.activeRecruitmentDrive = (companyDetails.length > 0) ? 'Active' : 'InActive';
              companyInternshipRecruitment(obj, function(internErr, internResponse) {
                obj.activeIntershipDrive = internResponse;
                if (campusId) {
                  visitedCompany(campusId, obj, function(err, visitedOurCampusInPast) {
                    obj.visitedOurCampusInPast = visitedOurCampusInPast;
                    callBc(null, obj);
                  });
                } else {
                  callBc(null, obj);
                }
              });
            }
          });
        });
      });
    }

    function companyInternshipRecruitment(obj, callBc) {
      var employerDrive = server.models.EmployerDrive;
      var lookup = require('../../commonValidation/lookupMethods').getLookupId;
      lookup('EMPLOYER_DRIVE_TYPE_CODE', 'Internship', function(driveType) {
        lookup('EMPLOYER_DRIVE_STATUS_CODE', 'Approved', function(driveStatus) {
          var companyObj = {};
          companyObj['companyId'] = obj.companyId;
          companyObj['driveTypeValueId'] = driveType.lookupValueId;
          companyObj['driveStatusValueId'] = driveStatus.lookupValueId;
          employerDrive.find({
            'where': {
              'and': [companyObj],
            },
          }, function(err, companyDetails) {
            if (err) {
              errorFunction(err, cb);
            } else {
              var activeIntershipDrive = (companyDetails.length > 0) ? 'Active' : 'InActive';
              callBc(null, activeIntershipDrive);
            }
          });
        });
      });
    }

    function visitedCompany(campusId, obj, callBc) {
      var employerDriveCampuses = server.models.EmployerDriveCampuses;
      var companyObj = {};
      companyObj['campusId'] = (campusId) ? campusId : undefined;
      companyObj['companyId'] = obj.companyId;
      employerDriveCampuses.find({
        'where': {
          'and': [companyObj],
        },
      }, function(err, companyDetails) {
        if (err) {
          errorFunction(err, callBc);
        } else {
          var visitedOurCampusInPast = (companyDetails) ? 'Active' : 'InActive';
          callBc(null, visitedOurCampusInPast);
        }
      });
    }
    function searchCompanyName(searchName, myArray, callBc) {
      async.map(myArray, searchWithName, function(error, response) {
        var output = [];
        output = cleanArray(response);
        callBc(null, output);
      });
      function searchWithName(object, cb) {
        var searchShort = (object.searchShortName) ? object.searchShortName.indexOf(searchName) >= 0 : false;
        var search = (object.searchName) ? object.searchName.indexOf(searchName) >= 0 : false;
        if (search || searchShort) {
          cb(null, object);
        } else {
          cb(null, null);
        }
      }
    }
  };
  // remote method declaration to get companyDetails details
  COMPANY_SEARCH_VW.remoteMethod('searchCompany', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'companyId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'searchName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companySizeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'companyTypeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'industryTypeValueId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
        //   required: true,
      http: {
          source: 'query',
        },
    }, {
        arg: 'cityId',
        type: 'number',
        //   required: true,
        http: {
          source: 'query',
        },
      }, {
        arg: 'rating',
        type: 'number',
        //   required: true,
        http: {
          source: 'query',
        },
      }, {
        arg: 'regionFlag',
        type: 'string',
        //   required: true,
        http: {
          source: 'query',
        },
      },
    {
      arg: 'campusId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCompany',
      verb: 'get',
    },
  });
};
