'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
// exporting function to use it in another modules if requires
module.exports = function(CAMPUS) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // when request comes to campus model then we execute logic or change request object before saving into database
  CAMPUS.createCampus = function(data, cb) {
    if (data.tireValueId) {
      // console.log('going in');
      // console.log(data.tireValueId);
      var lookupValue = server.models.LookupValue;
      var lookupType = server.models.LookupType;
      var status = [];
      lookupType.findOne({'where': {lookupCode: 'INSTITUTE_TIER'}}, function(err, lookupResponse) {
        if (err) {
          cb(err, null);
        } else {
          lookupValue.find({'where': {'lookupTypeId': lookupResponse.lookupTypeId}}, function(err, res) {
            if (err) {
              cb(err, null);
            } else {
              res.map(function(campusDet) {
                if (campusDet.lookupValueId == data.tireValueId) {
                  status.push('true');
                }
              });
              if (status[0] == 'true') {
                data.searchName = data.name.toUpperCase();
                data.searchShortName = data.shortName.toUpperCase();
                data.updateUserId = data.createUserId;
                data.createDatetime = new Date();
                data.updateDatetime = new Date();
                CAMPUS.create(data, function(err, res) {
                  if (err) {
                    cb(err, null);
                  } else {
                    cb(null, res);
                  }
                });
              } else {
                // console.log('the id was invalid');
                errorFunction('Invalid TierValueId', cb);
              }
            }
          });
        }
      });
    } else {
      data.searchName = data.name.toUpperCase();
      data.searchShortName = data.shortName.toUpperCase();
      data.updateUserId = data.createUserId;
      data.createDatetime = new Date();
      data.updateDatetime = new Date();
      CAMPUS.create(data, function(err, res) {
        if (err) {
          cb(err, null);
        } else {
          cb(null, res);
        }
      });
    }
  };
  CAMPUS.remoteMethod('createCampus', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: {
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    http: {
      path: '/createCampus',
      verb: 'POST',
    },
  });
  // remote method definition to get campus details based on campus id
  /**
 *getCampus - To get campus details
 *@constructor
 * @param {number} campusId - contains unique campus id
 * @param {function} callBc - deals with response
 */
  CAMPUS.getCampus = function(campusId, callBc) {
    if (campusId) {
      var campusDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = campusId;
      var campus = server.models.Campus;
      campusDetById(inputFilterObject, campus, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus details based on campus id
  CAMPUS.remoteMethod('getCampus', {
    description: 'To get campus details based on campus id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampus',
      verb: 'get',
    },
  });
  /**
 *updateCampus- To update campus details
 *@constructor
 * @param {number} campusData - contains object need to get updated
 * @param {function} callBc - deals with response
 *///updateCampus custom method starts here
  CAMPUS.updateCampus = function(campusData, cb) {
    var updateCampusDet = require('../../commonCampusFiles/update-campus-profile');
    updateCampusDet.updateCampusProfile(campusData, function(err, resp) {
      logger.info('upating campus profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //remoteMethod for campus update
  CAMPUS.remoteMethod('updateCampus', {
    description: 'Send Valid campusId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateCampus',
      verb: 'PUT',
    },
  });
    //remoteMethod for campus Profile
  CAMPUS.getCampusDashboard = function(inputObj, callBc) {
    var getCampusDashboardDetails = require('../../commonCampusFiles/get-campus-dashboard.js').getCampus;
    getCampusDashboardDetails(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusDashboard', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getCampusDashboard',
      verb: 'POST',
    },
  });
//remote method to get graphs for each department

  CAMPUS.getDepartmentData = function(inputObj, callBc) {
    var getGraphsInfo = require('../../commonCampusFiles/get-campus-graphs').getDepartmentGraphs;
    getGraphsInfo(inputObj, function(err, response) {
      if (err) {
        callBc(err, null);
      } else {
        callBc(null, response);
      }
    });
  };
  CAMPUS.remoteMethod('getDepartmentData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/getDepartmentData',
      verb: 'POST',
    },
  });
  CAMPUS.getCampusProfile = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getCampusProfile;
    getProfile(campusId, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getCampusProfile', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getCampusProfile',
      verb: 'GET',
    },
  });
  CAMPUS.getGraphAndTableData = function(campusId, callBc) {
    var getProfile = require('../../commonCampusFiles/campus-profile').getGraphAndTableData;
    getProfile(campusId, function(err, profileRes) {
      if (err) {
        errorFunction('error', callBc);
      } else {
        callBc(null, profileRes);
      }
    });
  };
  CAMPUS.remoteMethod('getGraphAndTableData', {
    description: 'Send Valid campusId ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getGraphAndTableData',
      verb: 'GET',
    },
  });
};
