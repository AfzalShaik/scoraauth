'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var studentDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var async = require('async');
module.exports = function(OFF_CAMPUS_STUDENT_SEARCH) {
  OFF_CAMPUS_STUDENT_SEARCH.getDetails = function(firstName, lastName, expectedSalary, programName,
    universityName, universityId, campusName, campusId,
    highlights, programId, programTypeValueId, programClassValueId,
programCatValueId, programMajorValueId, cgpaScore, cityName,
cityId, stateCode, stateName, region,
planedCompletionDate, actualCompletionDate, cb) {
    if (firstName || lastName || expectedSalary || programName ||
        universityName || universityId || campusName || campusId ||
        highlights || programId || programTypeValueId || programClassValueId ||
    programCatValueId || programMajorValueId || cgpaScore || cityName ||
    cityId || stateCode || stateName || region ||
    planedCompletionDate || actualCompletionDate) {
      var companyObject = {};
      //
      //
      //
      //
      companyObject['firstName'] = (firstName) ? firstName : undefined;
      companyObject['lastName'] = (lastName) ? lastName : undefined;
      companyObject['expectedSalary'] = (expectedSalary) ? expectedSalary : undefined;
      companyObject['programName'] = (programName) ? programName : undefined;
      companyObject['universityName'] = (universityName) ? universityName : undefined;
      companyObject['universityId'] = (universityId) ? universityId : undefined;
      companyObject['campusName'] = (campusName) ? campusName : undefined;
      companyObject['campusId'] = (campusId) ? campusId : undefined;
      companyObject['highlights'] = (highlights) ? highlights : undefined;
      companyObject['programId'] = (programId) ? programId : undefined;
      companyObject['programTypeValueId'] = (programTypeValueId) ? programTypeValueId : undefined;
      companyObject['programClassValueId'] = (programClassValueId) ? programClassValueId : undefined;
      companyObject['programCatValueId'] = (programCatValueId) ? programCatValueId : undefined;
      companyObject['programMajorValueId'] = (programMajorValueId) ? programMajorValueId : undefined;
      companyObject['cgpaScore'] = (cgpaScore) ? cgpaScore : undefined;
      companyObject['cityName'] = (cityName) ? cityName : undefined;
      companyObject['cityId'] = (cityId) ? cityId : undefined;
      companyObject['stateCode'] = (stateCode) ? stateCode : undefined;
      companyObject['stateName'] = (stateName) ? stateName : undefined;
      companyObject['region'] = (region) ? region : undefined;
      companyObject['planedCompletionDate'] = (planedCompletionDate) ? planedCompletionDate : undefined;
      companyObject['actualCompletionDate'] = (actualCompletionDate) ? actualCompletionDate : undefined;
      //
      //
      //
      //
      OFF_CAMPUS_STUDENT_SEARCH.find(
        {
          'where': {
            'and': [companyObject],
          },
        }, function(err, totalResponse) {
        if (err) {
          errorFunction('No Record With That Details', cb);
        } else {
          async.map(totalResponse, getstudentDetails, function(err, response) {
            if (err) {
              errorFunction('there was an error in getting details', cb);
            } else {
              cb(null, response);
            }
          });
        }
      }
      );
    //   studentDetails(companyObject, OFF_CAMPUS_STUDENT_SEARCH, function(err, totalResponse) {
    //     if (err) {
    //       errorFunction('No Record With That Details', cb);
    //     } else {
    //     }
    //   });
    } else {
      errorFunction('cant get details without any input', cb);
    }
  };
  var finalDetails = [];
  function getstudentDetails(data, callBack) {
    var finalDetails = {};
    var program = server.models.Program;
    program.findById(data.programId, function(err, programDetails) {
      if (err) {
        errorResponse('cant find program id', callBack);
      } else {
        var skills = server.models.StudentSkills;
        skills.find({'where': {'studentId': data.studentId}}, function(err, SkillDetails) {
          if (err) {
            errorResponse('cant find skill id', callBack);
          } else {
            async.map(SkillDetails, getNameOfId, function(err, details) {
              if (err) {
                errorResponse('cant find skill details', callBack);
              } else {
                var intrests = server.models.StudentInterests;
                intrests.find({'where': {'studentId': data.studentId}}, function(err, intrestDetails) {
                  if (err) {
                    errorResponse('cant find student intrests', callBack);
                  } else {
                    async.map(intrestDetails, getIntrestName, function(err, intrests) {
                      if (err) {
                        errorResponse('cant find intrest names', callBack);
                      } else {
                        getLookUpValue('PROGRAM_TYPE_CODE', data.programTypeValueId, function(err, programValue) {
                          if (err) {
                            errorResponse('cant find program code', callBack);
                          } else {
                            getLookUpValue('PROGRAM_CLASSIFICATION_CODE', data.programClassValueId, function(err, programClassfication) {
                              if (err) {
                                errorResponse('cant find program classfication', callBack);
                              } else {
                                getLookUpValue('PROGRAM_CATEGORY_CODE', data.programCatValueId, function(err, programCat) {
                                  if (err) {
                                    errorResponse('cant find program category', callBack);
                                  } else {
                                    getLookUpValue('PROGRAM_MAJOR', data.programMajorValueId, function(err, programMajor) {
                                      if (err) {
                                        errorResponse('cant find program major', callBack);
                                      } else {
                                        finalDetails.studentId = data.studentId;
                                        finalDetails.name = data.firstName + ' ' + data.lastName;
                                        finalDetails.cityName = data.cityName;
                                        finalDetails.state = data.stateName;
                                        finalDetails.universityName = data.universityName;
                                        finalDetails.campusId = data.campusId;
                                        finalDetails.campusName = data.campusName;
                                        finalDetails.programId = programDetails.programId;
                                        finalDetails.porgoramValue = programValue;
                                        finalDetails.programClassfication = programClassfication;
                                        finalDetails.programCategory = programCat;
                                        finalDetails.programMajor = programMajor;
                                        finalDetails.programName = programDetails.programName;
                                        finalDetails.yearOfPassedOut = data.planedCompletionDate + data.actualCompletionDate;
                                        finalDetails.skills = details;
                                        finalDetails.intrests = intrests;
                                        finalDetails.expectedSalary = data.expectedSalary;
                                        finalDetails.highlights = data.highlights;
                                        callBack(null, finalDetails); 
                                      }
                                    });
                                  }
                                });
                              }
                            });
                          }
                        });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
  }
  function getNameOfId(id, nameCallBack) {
    var lookupType = server.models.LookupType;
    var lookupValue = server.models.LookupValue;
    lookupType.findOne({'where': {'lookupCode': 'SKILL_TYPE_CODE'}}, function(err, lookupTypeResponse) {
      if (err) {
        errorResponse('cant find look up type id', nameCallBack);
      } else {
        lookupValue.findById(id.skillTypeValueId, function(err, idName) {
          if (err) {
            errorResponse('cant find look up value name', nameCallBack);
          } else {
            var skillname = {};
            skillname.id = idName.lookupValueId;
            skillname.skill = idName.lookupValue;
            nameCallBack(null, skillname);
          }
        });
      }
    });
  };
  function getIntrestName(id, IntrestCallBack) {
    var lookupType = server.models.LookupType;
    var lookupValue = server.models.LookupValue;
    lookupType.findOne({'where': {'lookupCode': 'INTEREST_TYPE_CODE'}}, function(err, lookupTypeResponse) {
      if (err) {
        errorResponse('cant find look up intrest', nameCallBack);
      } else {
        lookupValue.findById(id.interestTypeValueId, function(err, idName) {
          if (err) {
            errorResponse('cant find look intrest value', nameCallBack);
          } else {
            var intrestName = {};
            intrestName.id = idName.lookupValueId;
            intrestName.skill = idName.lookupValue;
            IntrestCallBack(null, intrestName);
          }
        });
      }
    });
  };
  function getLookUpValue(value, id, callBack) {
    var lookupType = server.models.LookupType;
    var lookupValue = server.models.LookupValue;
    lookupType.findOne({'where': {'lookupCode': value}}, function(err, lookupTypeResponse) {
      if (err) {
        errorResponse('cant find look up id', nameCallBack);
      } else {
        lookupValue.findById(id, function(err, idName) {
          if (err) {
            errorResponse('cant find look up Value', nameCallBack);
          } else {
            var details = {};
            details.id = idName.lookupValueId;
            details.name = idName.lookupValue;
            callBack(null, details);
          }
        });
      }
    });
  }
  OFF_CAMPUS_STUDENT_SEARCH.remoteMethod('getDetails', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'firstName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'lastName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'expectedSalary',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'programName',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'universityName',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'universityId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'campusName',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'campusId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'highlights',
      type: 'string',
          //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programId',
      type: 'number',
          //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programTypeValueId',
      type: 'number',
          //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programClassValueId',
      type: 'number',
          //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programCatValueId',
      type: 'number',
          //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'programMajorValueId',
      type: 'number',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cgpaScore',
      type: 'number',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cityName',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cityId',
      type: 'number',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateName',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'region',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'planedCompletionDate',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'actualCompletionDate',
      type: 'string',
              //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getDetails',
      verb: 'get',
    },
  });
}
;
