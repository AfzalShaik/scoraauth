'use strict';
var server = require('../../server/server');
module.exports = function(DEPARTMENT_CONTACT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  // remote method definition to create contact for particular department
   /**
 *createDepartmentContact- To Create Department Contact
 *@constructor
 * @param {object} data - contains all the data need to get created
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_CONTACT.createDepartmentContact = function(data, callBc) {
    if (data.campusId && data.departmentId) {
      if (data.primaryInd == 'Y') {
        searchForInd(data, function(err, res) {
          if (err) {
            errorResponse('there was an error', callBc);
          } else {
            // console.log('sucess');
          }
        });
      }
      var checkEntityExistence = require('../../ServicesImpl/CommonImpl/validateEntityRecord.js').checkEntitySingleRecordExistence;
      var inputFilterObject = {};
      inputFilterObject['campusId'] = data.campusId;
      inputFilterObject['departmentId'] = data.departmentId;
      var contactModel = server.models.Contact;
      var departmentModel = server.models.Department;
      checkEntityExistence(departmentModel, inputFilterObject, function(checkStatus) {
        if (checkStatus) {
          contactModel.create(data, function(error, contactRes) {
            if (error) {
              callBc(error, null);
            } else if (contactRes) {
              DEPARTMENT_CONTACT.create({
                'departmentId': Number(data.departmentId),
                'campusId': Number(data.campusId),
                'contactId': Number(contactRes.contactId),
                'primaryInd': String(data.primaryInd),
              }, function(err, resp) {
                if (err) {
                  errorResponse(callBc);
                } else {
                  // contactRes.requestStatus = true;
                  callBc(null, contactRes);
                }
              });
            } else {
              // console.log('1');
              errorResponse(callBc);
            }
          });
        } else {
          // console.log('2');
          errorResponse(callBc);
        }
      });
    } else {
      // console.log('3');
      errorResponse(callBc);
    }
  };
  function searchForInd(data, priCallback) {
    DEPARTMENT_CONTACT.find({'where': {'departmentId': data.departmentId,
    'campusId': data.campusId, 'primaryInd': 'Y'}}, function(err, res) {
      if (err) {
        errorResponse('campusId or Education person id was invalid', callBc);
      } else if (res == 0) {
        data.primaryInd = data.primaryInd;
      } else {
        var update = {};
        update.primaryInd = 'N';
        DEPARTMENT_CONTACT.updateAll({'educationPersonId': data.educationPersonId,
        'campusId': data.campusId, 'primaryInd': 'Y'}, update, function(err, resp) {
          if (err) {
            errorResponse('there was an error');
          } else {
            // console.log('this is response of update', resp);
            priCallback(null, resp);
          }
        });
      }
    });
  }
  // remote method declaration to create contact for particular department
  DEPARTMENT_CONTACT.remoteMethod('createDepartmentContact', {
    description: 'Useful to create contact for particular department',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      required: true,
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/createDepartmentContact',
      verb: 'post',
    },
  });
  // remote method definition to get  campus department contact details
    /**
 *getCampusDeptContact- To get campus department contact details by taking required fields
 *@constructor
 * @param {object} departmentId - Unique id for ecah and every department
 * @param {number} campusId - unique id for each and every campus
 * @param {number} contactId - unique id for every contact
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_CONTACT.getCampusDeptContact = function(departmentId, campusId, contactId, callBc) {
    var inputObj = {};
    var departmentContactModel = server.models.DepartmentContact;
    var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
    var campusDeptContact = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (departmentId && campusId && contactId) {
      inputObj['departmentId'] = departmentId;
      inputObj['campusId'] = campusId;
      inputObj['contactId'] = contactId;
      // below function will give contact details for an entity based on loopback include filter
      campusDeptContact(inputObj, departmentContactModel, 'departmentContact', callBc);
    } else if (departmentId && campusId) {
      inputObj['departmentId'] = departmentId;
      inputObj['campusId'] = campusId;
      // below function will give contact details for an entity based on loopback include filter
      campusDeptContact(inputObj, departmentContactModel, 'departmentContact', callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus department contact details
  DEPARTMENT_CONTACT.remoteMethod('getCampusDeptContact', {
    description: 'To get campus department contact details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/getCampusDeptContact',
      verb: 'get',
    },
  });
  // remote method definition to delete campus department contact details
    /**
 *deleteCampusDeptContact- To delete campus department contact details by taking required fields
 *@constructor
 * @param {object} departmentId - Unique id for ecah and every department
 * @param {number} campusId - unique id for each and every campus
 * @param {number} contactId - unique id for every contact
 * @param {function} callBc - deals with response
 */
  DEPARTMENT_CONTACT.deleteCampusDeptContact = function(departmentId, campusId, contactId, callBc) {
    if (departmentId && campusId && contactId) {
      var departmentContactModel = server.models.DepartmentContact;
      var contactModel = server.models.Contact;
      var delCampusDeptContact = require('../../ServicesImpl/CommonImpl/deleteEntityRecord.js').deleteEntityRecordExceptPrimary;
      var primaryCheckInput = {};
      primaryCheckInput['contactId'] = contactId;
      var inputFilterObject = {};
      inputFilterObject['departmentId'] = departmentId;
      inputFilterObject['campusId'] = campusId;
      inputFilterObject['contactId'] = contactId;
      delCampusDeptContact(primaryCheckInput, inputFilterObject, contactModel, departmentContactModel, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to delete campus department contact details
  DEPARTMENT_CONTACT.remoteMethod('deleteCampusDeptContact', {
    description: 'To delete campus department contact details',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'departmentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'campusId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'contactId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/deleteCampusDeptContact',
      verb: 'delete',
    },
  });

  //UpdateCompanyContact remote method starts here
   /**
 *updateDepartmentContact- To update Department Contact Details
 *@constructor
 * @param {object} contactData - contains all the data need to get updated
 * @param {function} callBack - deals with response
 */
  DEPARTMENT_CONTACT.updateDepartmentContact = function(contactData, cb) {
    var departmentContactUpdate = require('../../commonCampusFiles/update-department-contact.js');
    if (contactData.primaryInd == 'Y') {
      searchForInd(contactData, function(err, res) {
        if (err) {
          errorResponse('there was an error', callBc);
        } else {
          // console.log('sucess');
        }
      });
    }
    departmentContactUpdate.updateDepartmentContact(contactData, function(err, resp) {
      if (err) {
        cb(err, resp);
      }  else {
        var updatetData = {};
        updatetData.primaryInd = contactData.primaryInd;
        DEPARTMENT_CONTACT.updateAll({'campusId': contactData.campusId,
        'contactId': contactData.contactId, 'departmentId': contactData.departmentId},
        updatetData, function(err, res) {
          if (err) {
            errorResponse(cb);
          } else {
            cb(null, resp);
          }
        });
      }
    });
  };
  //UpdateDepartmentContact method to update both company and campus contacts
  DEPARTMENT_CONTACT.remoteMethod('updateDepartmentContact', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateDepartmentContact',
      verb: 'PUT',
    },
  });
};
