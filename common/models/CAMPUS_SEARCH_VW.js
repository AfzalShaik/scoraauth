'use strict';
var logger = require('../../server/boot/lib/logger');
var server = require('../../server/server');
var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var campusDetails = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var async = require('async');
module.exports = function(CAMPUS_SEARCH_VW) {
  // remote method definition to get campusDetails item details
  /**
   *searchCampus- To search campus based on given parameters
   *@constructor
   * @param {number} campusId - unique id of campus
   * @param {number} universityId - unique id of university
   * @param {number} regionFlag - contains region flag
   * @param {number} stateCode - contains state cose
   * @param {number} cityId - contains state id or a pin code
   * @param {function} callBc - deals with response
   */
  CAMPUS_SEARCH_VW.searchCampus = function(campusId, searchName, universityId, regionFlag, stateCode, cityId, tier, averageSalary, callBc) {
    if (campusId || universityId || regionFlag || stateCode || cityId || tier || averageSalary || searchName) {
      var campusObj = {};
      campusObj['campusId'] = (campusId) ? campusId : undefined;
      campusObj['universityId'] = (universityId) ? universityId : undefined;
      campusObj['cityId'] = (cityId) ? cityId : undefined;
      campusObj['stateCode'] = (stateCode) ? stateCode : undefined;
      campusObj['regionFlag'] = (regionFlag) ? regionFlag : undefined;
      campusObj['averageSalary'] = (averageSalary) ? averageSalary : undefined;
      campusObj['tier'] = (tier) ? tier : undefined;
      campusDetails(campusObj, CAMPUS_SEARCH_VW, function(err, campusSearch) {
        if (err) {
          errorFunction(err, callBc);
        } else if (campusSearch.data.length > 0) {
          if (searchName) {
            searchCampusName(searchName, campusSearch.data, function(error, searchedData) {
              callBc(null, searchedData);
            });
          } else {
            async.map(campusSearch.data, placementDrive, function(error, placementInfo) {
              callBc(null, placementInfo);
            });
          }
        } else {
          errorFunction('Records not found', callBc);
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campusDetails details
  CAMPUS_SEARCH_VW.remoteMethod('searchCampus', {
    description: 'To get campus details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusId',
      type: 'number',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'searchName',
      type: 'string',
      http: {
        source: 'query',
      },
    },
    {
      arg: 'universityId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    {
      arg: 'regionFlag',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'stateCode',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'cityId',
      type: 'number',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'tier',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    }, {
      arg: 'averageSalary',
      type: 'string',
        //   required: true,
      http: {
        source: 'query',
      },
    },
    ],
    http: {
      path: '/searchCampus',
      verb: 'get',
    },
  });

  function placementDrive(obj, cb) {
    var campusDrive = server.models.CampusDrive;
    var lookup = require('../../commonValidation/lookupMethods').getLookupId;
    lookup('EDUCATION_DRIVE_TYPE_CODE', 'Placement', function(driveType) {
      lookup('EDUCATION_DRIVE_STATUS_CODE', 'Approved', function(driveStatus) {
        var campusDriveObj = {};
        campusDriveObj['campusId'] = obj.campusId;
        campusDriveObj['driveTypeValueId'] = driveType.lookupValueId;
        campusDriveObj['driveStatusValueId'] = driveStatus.lookupValueId;
        campusDrive.find({
          'where': {
            'and': [campusDriveObj],
          },
        }, function(err, campusDrives) {
          if (err) {
            errorResponse(cb);
          } else {
            getDrives(campusDrives, obj, function(driveErr, driveResp) {
              cb(null, driveResp);
            });
          }
        });
      });
    });
  }

  function getDrives(campusDrives, obj, callBc) {
    var campus = server.models.Campus;
    var enrollmentModel = server.models.Enrollment;
    var campusObj = {};
    campusObj['campusId'] = obj.campusId;
    campus.findOne({
      'where': {
        'and': [campusObj],
      },
    }, function(err, campusDetails) {
      enrollmentModel.find({
        'where': {
          'and': [campusObj],
        },
      }, function(errEnroll, enrollmentDetails) {
        var start = enrollmentDetails[0];
        start = (enrollmentDetails.length > 0) ? enrollmentDetails[0].startDate : new Date();
        var actual = (enrollmentDetails.length > 0) ? enrollmentDetails[0].planedCompletionDate : new Date();
        var currentDate = new Date();
        var enrollmentArray = [];
        enrollmentArray = (start.getTime() < currentDate.getTime() && actual.getTime() > currentDate.getTime()) ? enrollmentDetails.length : 0;
        obj.activePlacementDrive = 'Active';
        obj.numberOfStudents = campusDetails.numberOfStudents;
        obj.studentsAvailable = enrollmentArray;
        callBc(null, obj);
      });
    });
  }

  function searchCampusName(searchName, myArray, callBc) {
    async.map(myArray, searchWithName, function(error, response) {
      var output = [];
      output = cleanArray(response);
      callBc(null, output);
    });
    function searchWithName(object, cb) {
      var searchShort = (object.searchShortName) ? object.searchShortName.indexOf(searchName) >= 0 : false;
      var search = (object.searchName) ? object.searchName.indexOf(searchName) >= 0 : false;
      if (search || searchShort) {
        cb(null, object);
      } else {
        cb(null, null);
      }
    }
  }
};
