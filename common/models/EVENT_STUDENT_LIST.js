'use strict';
var server = require('../../server/server');
var getSkills = require('../models/CAMPUS_EVENT_STUDENT_SEARCH_VW.js').getStudentIntrestsAndSkills;
module.exports = function(EVENT_STUDENT_LIST) {
  EVENT_STUDENT_LIST.observe('before save', function eventStudentListBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  //studentEvents remote method starts here
    /**
 *studentEvents- To get student event details by taking required fields
 *@constructor
 * @param {object} stData - contains the id for required data
 * @param {function} cb - deals with response
 */
  EVENT_STUDENT_LIST.studentEvents = function(stData, cb) {
    var getEvents = require('../../commonCampusFiles/get-student-events');
    getEvents.getStudentEvents(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //studentEvents method creation

  EVENT_STUDENT_LIST.remoteMethod('studentEvents', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/studentEvents',
      verb: 'get',
    },
  });
  //studentEventActions remote method starts here
   /**
 *studentEventActions- Deals with the actions of student event
 *@constructor
 * @param {object} stData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  EVENT_STUDENT_LIST.studentEventActions = function(stData, cb) {
    var getEvents = require('../../commonCampusFiles/update-student-events');
    getEvents.updateStudentEvents(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };
  //studentEventActions method creation
  EVENT_STUDENT_LIST.remoteMethod('studentEventActions', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentEventActions',
      verb: 'PUT',
    },
  });
  var studentDetails = [];
  EVENT_STUDENT_LIST.getStudentsAssociatedWithCampusEvent = function(campusEventId, cb) {
    EVENT_STUDENT_LIST.find({'where': {'campusEventId': campusEventId}}, function(err, res) {
      if (err) {
        cb(err, null);
      } else {
        var async = require('async');
        studentDetails = [];
        async.map(res, getStudents, function(err1, res1) {
          if (err) {
            cb(err, null);
          } else {
            cb(null, studentDetails);
          }
        });
      }
    });
  };
  var totalSkills = [];
  var totalIntrests = [];
  function getStudents(obj, cb1) {
    // var skills = server.models.StudentSkills;
    // skills.find({'where': {'studentId': obj.studentId}}, function(err, res) {
    //   if (err) {
    //     cb1(err, null);
    //   } else {
        // var intrests = server.models.StudentInterests;
        // intrests.find({'where': {'studentId': obj.studentId}}, function(err1, res1) {
        //   if (err1) {

        //   } else {
            // var async = require('async');
            // async.map(res1, getIntrestName, function(err17, res17) {
            //   if (err17) {

            //   } else {
    var skills = server.models.CampusEventStudentSearchVw;
    skills.getSkillsAndInt(obj, function(err18, res18) {
      if (err18) {
        cb1(err18, null);
      } else {
        var eventstudent = server.models.EventStudentList;
        eventstudent.find({'where': {'studentId': obj.studentId}}, function(err5, res5) {
          if (err5) {
            cb1(err5, null);
          } else {
            var enrolled = res5.length;
            getIds(function(err6, res6) {
              if (err6) {
                cb1(err6, null);
              } else {
                getAllCounts(res6, obj, function(err9, res9) {
                  if (err9) {
                    cb1(err9, null);
                  } else {
                    var student = server.models.CampusEventStudentSearchVw;
                    student.findOne({'where': {'studentId': obj.studentId}}, function(err2, res2) {
                      if (err2) {
                        cb1(err2, null);
                      } else {
                        // totalSkills = [];
                        // totalIntrests = [];
                        // var async = require('async');
                        // async.map(res, getSkillName, function(err16, res16) {
                        //   if (err16) {
                        //     cb1(err16, null);
                        //   } else {
                        var data = {};
                                // data.info = res2;
                                // data.Skills = totalSkills;
                                // data.Intrests = totalIntrests;
                        data.StudentInfo = res18;
                        data.totalOffers = res9.totalOffers;
                        data.totalShortlisted = res9.totalShortlisted;
                        data.enrolled = res5.length;
                        studentDetails.push(data);
                        totalSkills = [];
                        totalIntrests = [];
                        cb1(null, 'done');
                        //   }
                        // });
                      }
                    });
                  }
                });
              }
            });
          }
        });
      }
    });
            //   }
            // });
        //   }
        // });
    //   }
    // });
  }
  function getDetails(skill, intrest, cb6) {
    totalIntrests = [];
    totalSkills = [];
    console.log(skill);
  }
  function getSkillName(ob, cb5) {
    lookupValueName('SKILL_TYPE_CODE', ob.skillTypeValueId, function(err, res) {
      if (err) {
        cb5(err, null);
      } else {
        totalSkills.push(res[0].lookupValue);
        cb5(null, 'done');
      }
    });
  }
  function getIntrestName(ob, cb5) {
    lookupValueName('INTEREST_TYPE_CODE', ob.interestTypeValueId, function(err, res) {
      if (err) {
        cb5(err, null);
      } else {
        totalIntrests.push(res[0].lookupValue);
        cb5(null, 'done');
      }
    });
  }
  function getAllCounts(lookup, studentDetails, cb3) {
    var eventstudent = server.models.EventStudentList;
    eventstudent.find({'where': {and: [
      {'studentId': studentDetails.studentId},
      {'candidateStatusValueId': lookup.Offered},
    ]}}, function(err10, res10) {
      if (err10) {
        cb3(err10, null);
      } else {
        eventstudent.find({'where': {and: [
          {'studentId': studentDetails.studentId},
          {'candidateStatusValueId': lookup.Shortlisted},
        ]}}, function(err11, res11) {
          if (err10) {
            cb3(err11, null);
          } else {
            var data = {};
            data.totalOffers = res10.length;
            data.totalShortlisted = res11.length;
            cb3(null, data);
          }
        });
      }
    });
  }
  function getIds(cb2) {
    lookup('CANDIDATE_STATUS_TYPE', 'Offered', function(err7, res7) {
      if (err7) {
        cb2(err7, null);
      } else {
        lookup('CANDIDATE_STATUS_TYPE', 'Shortlisted', function(err8, res8) {
          if (err8) {
            cb2(err8, null);
          } else {
            var lookupData = {};
            lookupData.Offered = res7[0].lookupValueId;
            lookupData.Shortlisted = res8[0].lookupValueId;
            cb2(null, lookupData);
          }
        });
      }
    });
  }
  function lookup(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValue: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', null);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
  function lookupValueName(type, ob, lookupCallback) {
    var lookup = server.models.LookupType;
    var lookupvalue = server.models.LookupValue;
      //var type = 'SKILL_TYPE_CODE';
    lookup.find({
      'where': {
        lookupCode: type,
      },
    }, function(err, re) {
      if (err) {
        lookupCallback(err, null);
      } else {
        lookupvalue.find({
          'where': {
            lookupValueId: ob,
            lookupTypeId: re[0].lookupTypeId,
          },
        }, function(err, re1) {
          if (err) {
            lookupCallback('error', null);
          } else {
            lookupCallback(null, re1);
          };
        });
      }
    });
  }
  EVENT_STUDENT_LIST.remoteMethod('getStudentsAssociatedWithCampusEvent', {
    description: 'To get student event details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [{
      arg: 'campusEventId',
      type: 'number',
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getStudentsAssociatedWithCampusEvent',
      verb: 'get',
    },
  });
};
