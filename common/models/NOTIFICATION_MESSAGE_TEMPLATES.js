'use strict';
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
var async = require('async');
var notificationEventId;
var notificationOutput = [];
module.exports = function(NOTIFICATION_MESSAGE_TEMPLATES) {
  NOTIFICATION_MESSAGE_TEMPLATES.pushNotification = function(input, callBc) {
    notificationOutput = [];
    NOTIFICATION_MESSAGE_TEMPLATES.find({
      'where': {
        'notificationName': input.notificationName,
      },
    }, function(notifyErr, notificationResponse) {
      async.map(notificationResponse, notificationEventCreate, function(eventError, template) {
        callBc(null, template);
      });

      function notificationEventCreate(notification, callback) {
        createNotificationEvent(input, notification, function(eventErr, eventResponse) {
          notificationEventId = (eventResponse) ? eventResponse.notificationEventId : null;
          if (notification.broadcastFlag === 'GRP' && notification.receiverRoleCode === 'RECDIR') {
            createNotificationDetails(input, notification, function(err, detailsOut) {
              if (err) {
                throwError(err, callback);
              } else {
                var finalResponse = {};
                finalResponse.notificationTemplate = notification;
                finalResponse.notificationEvent = eventResponse;
                finalResponse.notificationDetails = detailsOut;
                callback(null, finalResponse);
              }
            });
          } else if (notification.broadcastFlag === 'IND' && notification.receiverRoleCode === 'STUDENT') {
            createStudentDetail(input, function(err, detailsOut) {
              if (err) {
                throwError(err, callback);
              } else {
                var finalResponse = {};
                finalResponse.notificationTemplate = notification;
                finalResponse.notificationEvent = eventResponse;
                finalResponse.notificationDetails = detailsOut;
                callback(null, finalResponse);
              }
            });
          } else if (notification.broadcastFlag === 'GRP' && notification.receiverRoleCode === 'PLCDIR') {
            createNotificationDetails(input, notification, function(err, detailsOut) {
              if (err) {
                throwError(err, callback);
              } else {
                var finalResponse = {};
                finalResponse.notificationTemplate = notification;
                finalResponse.notificationEvent = eventResponse;
                finalResponse.notificationDetails = detailsOut;
                callback(null, finalResponse);
              }
            });
          } else {
            throwError('Invalid Data', callBc);
          }
        });
      }
    });
  };
  // remote method
  NOTIFICATION_MESSAGE_TEMPLATES.remoteMethod('pushNotification', {
    description: 'Company Will Publish Event ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: {
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    },
    http: {
      path: '/pushNotification',
      verb: 'POST',
    },
  });

  function createNotificationEvent(input, notification, cb) {
    var notificationEvent = server.models.NotificationEvents;
    var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
    var roleTypeValueId, parentId;
    getLookupId('ROLE_TYPE_CODE', 'Employer', function(roleTypeId) {
      getLookupId('ROLE_TYPE_CODE', 'Campus', function(campusRoleTypeId) {
        getLookupId('ROLE_TYPE_CODE', 'Student', function(studentRoleTypeId) {
          if (input.role == 'PLCDIR' || input.role == 'RECDIR') {
            roleTypeValueId = (notification.receiverRoleCode == 'PLCDIR') ? roleTypeId.lookupValueId : campusRoleTypeId.lookupValueId;
            parentId = (notification.receiverRoleCode == 'PLCDIR') ? input.companyId : input.campusId;
          } else {
            roleTypeValueId = studentRoleTypeId.lookupValueId;
            parentId = input.campusId;
          }
          var eventObj = {
            'notificationTemplateId': notification.notificationTemplateId,
            'roleTypeValueId': roleTypeValueId,
            'createDatetime': new Date(),
            'updateDatetime': new Date(),
            'createUserId': input.userId,
            'updateUserId': input.userId,
            'parentId': parentId,
            'transactionId': (notification.receiverRoleCode == 'PLCDIR') ? input.campusEventId : input.empEventId,
            'senderId': input.userId,
          };
          notificationEvent.create(eventObj, function(createdErr, eventResponse) {
            if (createdErr) {
              throwError(createdErr, cb);
            } else {
              cb(null, eventResponse);
            }
          });
        });
      });
    });
  }

  function createNotificationDetails(input, notification, callBack) {
    var listOfEntities = (notification.receiverRoleCode == 'PLCDIR') ? input.campusList : input.companyList;
    async.map(listOfEntities, createDetail, function(error, detailResponse) {
      if (error) {
        throwError(error, callBack);
      } else {
        callBack(null, detailResponse);
      }
    });
  }

  function createDetail(obj, detailCB) {
    var educationPerson = server.models.EducationPerson;
    var employerPerson = server.models.EmployerPerson;
    var modelsName = (obj.campusId) ? educationPerson : employerPerson;
    var inputObj = {};
    var campusObj = {};
    var companyObj = {};
    campusObj['campusId'] = obj.campusId;
    companyObj['companyId'] = obj.companyId;
    inputObj = (obj.campusId) ? campusObj : companyObj;
    modelsName.find({
      'where': {
        'and': [inputObj],
      },
    }, function(eduErr, eduUsers) {
      if (eduErr) {
        throwError(eduErr, detailCB);
      } else {
        async.map(eduUsers, createNotificationDetail, function(createDetailErr, detailsResponse) {
          if (createDetailErr) {
            throwError(createDetailErr, detailCB);
          } else {
            detailCB(null, detailsResponse);
          }
        });
      }
    });
  }

  function createNotificationDetail(object, notificationCB) {
    var notificationDetails = server.models.NotificationDetails;
    var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
    getLookupId('ROLE_TYPE_CODE', 'Campus', function(roleTypeId) {
      getLookupId('ROLE_TYPE_CODE', 'Employer', function(employerRoleTypeId) {
        var roleTypeValueId = (object.campusId) ? roleTypeId.lookupValueId : employerRoleTypeId.lookupValueId;
        var parentId = (object.campusId) ? object.campusId : object.companyId;
        var detailsObj = {
          'notificationEventId': notificationEventId,
          'roleTypeValueId': roleTypeValueId,
          'parentId': parentId,
          'recipientId': object.id,
          'notificationRead': new Date(),
          'notificationDismissedInd': 'N',
          'createDatetime': new Date(),
          'notificationReadInd': 'N',
        };
        notificationDetails.create(detailsObj, function(detailsErr, details) {
          if (detailsErr) {
            throwError(detailsErr, notificationCB);
          } else {
            notificationCB(null, details);
          }
        });
      });
    });
  }

  function createStudentDetail(object, notificationCallBc) {
    var studentList = object.studentList;
    async.map(studentList, createStudentDetail, function(studentErr, studentResponse) {
      notificationCallBc(null, studentResponse);
    });
    function createStudentDetail(obj, studentCB) {
      var notificationDetails = server.models.NotificationDetails;
      var getLookupId = require('../../commonValidation/lookupMethods').getLookupId;
      getLookupId('ROLE_TYPE_CODE', 'Student', function(roleTypeId) {
        var roleTypeValueId = roleTypeId.lookupValueId;
        var parentId = object.campusId;
        var student = server.models.Student;
        student.findOne({
          'where': {
            'studentId': obj.studentId,
          },
        }, function(studentErr, studentInfo) {
          if (studentErr) {
            throwError(studentErr, studentCB);
          } else {
            var detailsObj = {
              'notificationEventId': notificationEventId,
              'roleTypeValueId': roleTypeValueId,
              'parentId': parentId,
              'recipientId': studentInfo.id,
              'notificationRead': new Date(),
              'notificationDismissedInd': 'N',
              'createDatetime': new Date(),
            };
            notificationDetails.create(detailsObj, function(detailsErr, details) {
              if (detailsErr) {
                throwError(detailsErr, studentCB);
              } else {
                studentCB(null, details);
              }
            });
          }
        });
      });
    }
  }
};
