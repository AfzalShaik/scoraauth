'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(STUDENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var errorFunction = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // when request comes to student model then we execute logic or change request object before saving into database
  STUDENT.observe('before save', function studentBeforeSave(ctx, next) {
    // console.log('ctx.isNewInstance: ' + ctx.isNewInstance);
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });

  // remote method definition to get student details
    /**
 *getStudentDetails- To get student details by taking required fields
 *@constructor
 * @param {object} studentId - Unique id for ecah and every student
 * @param {function} callBc - deals with response
 */
  STUDENT.getStudentDetails = function(studentId, callBc) {
    var inputObj = {};
    var student = server.models.Student;
    var campusStudentDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId) {
      inputObj['studentId'] = studentId;
      // below function will give address details for an entity based on loopback include filter
      var includeFilterArray = ['studentMarriageDetails', 'studentStatusDetails'];
      campusStudentDet(inputObj, student, includeFilterArray, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus address details
  STUDENT.remoteMethod('getStudentDetails', {
    description: 'To get student details based on student id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/getStudent',
      verb: 'GET',
    },
  });

  //updateStudent remote method starts here
   /**
 *updateStudent- To update Student Details
 *@constructor
 * @param {object} stData - contains all the data need to get updated
 * @param {function} cb - deals with response
 */
  STUDENT.updateStudent = function(stData, cb) {
    var studentUpdate = require('../../commonCampusFiles/update-student');
    studentUpdate.updateStudentService(stData, function(err, resp) {
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //updateStudent method creation
  STUDENT.remoteMethod('updateStudent', {
    description: 'Send Valid Data ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'array',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/updateStudent',
      verb: 'PUT',
    },
  });
  STUDENT.studentProfile = function(studentId, callBc) {
    var inputObj = {};
    var student = server.models.Student;
    var campusStudentDet = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsUsingIncludeFilter;
    if (studentId) {
      inputObj['studentId'] = studentId;
      var includeFilterArray = ['studentMarriageDetails', 'studentStatusDetails'];
      campusStudentDet(inputObj, student, includeFilterArray, function(err, studentRes) {
        if (err) {
          errorFunction('an error in geting student details', callBc);
        } else {
          var getProfile = require('../../commonCompanyFiles/get-student-profile').getProfile;
          getProfile(studentRes, function(err, res) {
            if (err) {
              errorFunction('an error', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get campus address details
  STUDENT.remoteMethod('studentProfile', {
    description: 'To get student details based on student id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [{
      arg: 'studentId',
      type: 'number',
      required: true,
      http: {
        source: 'query',
      },
    }],
    http: {
      path: '/studentProfile',
      verb: 'GET',
    },
  });
};
