'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var async = require('async');
var count = 0;
var output = [];
var cleanArray = require('../../commonValidation/common-mass-upload.js').cleanArray;
var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
module.exports = function(STUDENTS_ASSESSMENT_UPLOAD_WORK) {
  // remote method definition to create student assement details

  STUDENTS_ASSESSMENT_UPLOAD_WORK.studentAssessmentMassUpload = function(stAssessmentDataa, cb) {
    var name = stAssessmentDataa.fileDetails.name;
    var container = stAssessmentDataa.fileDetails.container;
    var inputAssessmentFile = './attachments/' + container + '/' + name;
    // var inputAssessmentFile = './commonValidation/student_assessment_upload.csv';
    var createAssessment = require('../../commonCampusFiles/create-student-assessment-upload.js').createAssessment;
    STUDENTS_ASSESSMENT_UPLOAD_WORK.destroyAll({}, function(destroyError, destroyOutput) {
      loadMassUploadStudentsData(inputAssessmentFile, function(err, fileResponse) {
        for (var i = 0; i < fileResponse.length; i++) {
          var obj = {};
          obj = fileResponse[i];
          obj.rowNumber = i + 1;
          output.push(obj);
        }
        createAssessment(output, stAssessmentDataa, function(assesErr, assessmentOutput) {
          if (assesErr) {
            cb(assesErr, null);
          } else {
            output = [];
            cb(null, assessmentOutput);
          }
        });
      });
    });
  };

  //assessment mass upload method creation
  STUDENTS_ASSESSMENT_UPLOAD_WORK.remoteMethod('studentAssessmentMassUpload', {
    description: 'Send Valid assessment Data ',
    returns: {
      root: true,
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/studentAssessmentMassUpload',
      verb: 'POST',
    },
  });

  function loadMassUploadStudentsData(inputFile, cb) {
    var fs = require('fs');
    var parse = require('csv-parse');
    // var async = require('async');
    var parser = parse({
      delimiter: ',',
      columns: true
    }, function(err, data) {
      //   async.eachSeries(data, function(line, callBc) {  });
      if (err) {
        cb(err, null);
      } else {
        cb(null, data);
      }
    });

    fs.createReadStream(inputFile).pipe(parser);
  }

  function cleanArray(actual) {
    var newArray = new Array();
    for (var i = 0; i < actual.length; i++) {
      if (actual[i]) {
        newArray.push(actual[i]);
      }
    }
    return newArray;
  }



  function getFailureTests(failedArray, cb) {
    var finalArray = [];
    for (var i = 0; i < failedArray.length; i++) {
      var obj = {
        'rowNumber': failedArray[i].rowNumber,
        'admissionNo': failedArray[i].admissionNo,
        'departmentId': failedArray[i].departmentId,
        'programId': failedArray[i].programId,
        'score': failedArray[i].score,
        'programUnitTypeValueId': failedArray[i].programUnitTypeValueId,
        'error': 'Invalid Admission Number',
      };
      finalArray.push(obj);
    }
    var fs = require('fs');
    var csv = require('fast-csv');
    // var ws = fs.createWriteStream('./commonValidation/hiringOut.csv');
    // csv
    //   .write(finalArray, {
    //     headers: true,
    //   })
    //   .pipe(ws);
    csv
      .writeToPath('./commonValidation/assessmentOut.csv', finalArray, {
        headers: true
      })
      .on('finish', function() {
        // console.log('done!');
      });
    cb(null, 'success');
  }

};
