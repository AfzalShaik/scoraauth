'use strict';

var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
module.exports = function(CAMPUS_EVENT) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
/**
 * campusEventBeforeSave-used for creation
 * @constructor
 * @param {object} ctx -details entered by the user
 * @param {function} next-used for skiping when we want to skip any
 */
  CAMPUS_EVENT.observe('before save', function campusEventBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
/**
 * getCampusEventDet-used for getting details based on id
 *@constructor
 * @param {number} campusEventId-primary key for searching in the database
 * @param {function} callBc-callback function deals with response
 */
  CAMPUS_EVENT.getCampusEventDet = function(campusEventId, callBc) {
    var inputObj = {};
    var campusEventModel = server.models.CampusEvent;
    var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
    if (campusEventId) {
      inputObj['campusEventId'] = campusEventId;
      findEntity(inputObj, campusEventModel, function(err, res) {
        if (err) {
          errorResponse(err, callBc);
        } else {
          callBc(null, res);
        }
      });
    } else {
      errorResponse('results not found', callBc);
    }
  };
/**
 * Remote method for the getCampusEventDet
 */
  CAMPUS_EVENT.remoteMethod('getCampusEventDet', {
    description: 'To get campus event details by id details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: {arg: 'campusEventId ',
             type: 'number',
             required: true,
             http: {source: 'query'}},
    http: {
      path: '/getCampusEventDet',
      verb: 'get',
    },
  });
/**
 *updateCampusEvent -function deals with updating
 *@constructor
 * @param {object} campusEventData-contains all the data which can get updated
 * @param {function} callBc-callback function
 */
  CAMPUS_EVENT.updateCampusEvent = function(campusEventData, callBc) {
    var campusEventUpdate = require('../../update/campusevent-update.js');
    campusEventUpdate.campusEvent(campusEventData, function(err, response) {
      if (err) {
        callBc(err, response);
      } else if (response == 0) {
        callBc(err, response);
      } else {
        callBc(null, response);
      }
    });
  };
/**
 *Remote method for updateCampusEvent
 */
  CAMPUS_EVENT.remoteMethod('updateCampusEvent', {
    description: 'update campus event details',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
    http: {
      path: '/updateCampusEvent',
      verb: 'PUT',
    },
  });
/**
 *CampusBussinessLogic -function deals with Bussiness Logics in Campus Event
 *@constructor
 * @param {object} campusEventData-contains all the data which required
 * @param {function} cb-callback function
 */
  CAMPUS_EVENT.campusBussinessLogic = function(campusEventData, cb) {
    var campusDriveManagment = require('../../ServicesImpl/CampusImpl/campusEventManagment.js');
    campusDriveManagment.campusBussinessEvent(campusEventData, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        response.requestStatus = true;
        cb(null, response);
      }
    });
  };
  //accpect method to update campus drive
  CAMPUS_EVENT.remoteMethod('campusBussinessLogic', {
    description: 'Event details ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [{
      arg: 'data',
      type: 'object',
      http: {
        source: 'body',
      },
    }],
    http: {
      path: '/campusBussinessLogic',
      verb: 'PUT',
    },
  });
};
// 'use strict';

// var server = require('../../server/server');
// var logger = require('../../server/boot/lib/logger');
// module.exports = function(CAMPUS_EVENT) {
//   var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
// /**
//  * campusEventBeforeSave-used for creation
//  * @constructor
//  * @param {object} ctx -details entered by the user
//  * @param {function} next-used for skiping when we want to skip any
//  */
//   CAMPUS_EVENT.observe('before save', function campusEventBeforeSave(ctx, next) {
//     if (ctx.isNewInstance) {
//       ctx.instance.updateUserId = ctx.instance.createUserId;
//       ctx.instance.createDatetime = new Date();
//       ctx.instance.updateDatetime = new Date();
//       next();
//     } else {
//       next();
//     }
//   });
// /**
//  * getCampusEventDet-used for getting details based on id
//  *@constructor
//  * @param {number} campusEventId-primary key for searching in the database
//  * @param {function} callBc-callback function deals with response
//  */
//   CAMPUS_EVENT.getCampusEventDet = function(campusEventId, callBc) {
//     var inputObj = {};
//     var campusEventModel = server.models.CampusEvent;
//     var findEntity = require('../../ServicesImpl/CommonImpl/getEntityDetails.js').entityDetailsById;
//     if (campusEventId) {
//       inputObj['campusEventId'] = campusEventId;
//       findEntity(inputObj, campusEventModel, function(err, res) {
//         if (err) {
//           errorResponse(err, callBc);
//         } else {
//           callBc(null, res);
//         }
//       });
//     } else {
//       errorResponse('results not found', callBc);
//     }
//   };
// /**
//  * Remote method for the getCampusEventDet
//  */
//   CAMPUS_EVENT.remoteMethod('getCampusEventDet', {
//     description: 'To get campus event details by id details',
//     returns: {
//       type: 'array',
//       root: true,
//     },
//     accepts: {arg: 'campusEventId ',
//              type: 'number',
//              required: true,
//              http: {source: 'query'}},
//     http: {
//       path: '/getCampusEventDet',
//       verb: 'get',
//     },
//   });
// /**
//  *updateCampusEvent -function deals with updating
//  *@constructor
//  * @param {object} campusEventData-contains all the data which can get updated
//  * @param {function} callBc-callback function
//  */
//   CAMPUS_EVENT.updateCampusEvent = function(campusEventData, callBc) {
//     var campusEventUpdate = require('../../update/campusevent-update.js');
//     campusEventUpdate.campusEvent(campusEventData, function(err, response) {
//       if (err) {
//         callBc(err, response);
//       } else if (response == 0) {
//         callBc(err, response);
//       } else {
//         callBc(null, response);
//       }
//     });
//   };
// /**
//  *Remote method for updateCampusEvent
//  */
//   CAMPUS_EVENT.remoteMethod('updateCampusEvent', {
//     description: 'update campus event details',
//     returns: {
//       arg: 'data',
//       type: 'object',
//     },
//     accepts: [{arg: 'data', type: 'object', http: {source: 'body'}}],
//     http: {
//       path: '/updateCampusEvent',
//       verb: 'PUT',
//     },
//   });
// /**
//  *CampusBussinessLogic -function deals with Bussiness Logics in Campus Event
//  *@constructor
//  * @param {object} campusEventData-contains all the data which required
//  * @param {function} cb-callback function
//  */
//   CAMPUS_EVENT.CampusBussinessLogic = function(campusEventData, cb) {
//     var campusDriveManagment = require('../../ServicesImpl/CampusImpl/campusEventManagment.js');
//     campusDriveManagment.campusBussinessEvent(campusEventData, function(err, response) {
//       if (err) {
//         cb(err, null);
//       }
//       else {
//         response.requestStatus = true;
//         cb(null, response);
//       }
//     });
//   };
//   //accpect method to update campus drive
//   CAMPUS_EVENT.remoteMethod('CampusBussinessLogic', {
//     description: 'Event details ',
//     returns: {
//       arg: 'data',
//       type: 'object',
//     },
//     accepts: {
//       arg: 'data',
//       type: 'object',
//       http: {
//         source: 'body',
//       },
//     },
//     http: {
//       path: '/CampusBussinessLogic',
//       verb: 'PUT',
//     },
//   });
// };
