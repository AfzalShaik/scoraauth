'use strict';
//requiring server path so that we can use exported functions or methods when ever we need
var server = require('../../server/server');
var logger = require('../../server/boot/lib/logger');
var allmethods = require('../../update/allmethods.js');

// exporting function to use it in another modules if requires
module.exports = function(COMPANY) {
  var errorResponse = require('../../ErrorHandling/customizeErrorTypes.js').invalidInputError;
  var throwError = require('../../ErrorHandling/customizeErrorTypes.js').errorFunction;
  // when request comes to company model then we execute logic or change request object before saving into database
  COMPANY.observe('before save', function companyBeforeSave(ctx, next) {
    if (ctx.isNewInstance) {
      ctx.instance.searchName = (ctx.instance.name) ? ctx.instance.name.toUpperCase() : null;
      ctx.instance.searchShortName = (ctx.instance.shortName) ? ctx.instance.shortName.toUpperCase() : null;
      ctx.instance.updateUserId = ctx.instance.createUserId;
      ctx.instance.createDatetime = new Date();
      ctx.instance.updateDatetime = new Date();
      next();
    } else {
      next();
    }
  });
  // remote method definition to get company details based on company id
  /**
 *getCompany- To get company details based on the companyId
 *@constructor
 * @param {number} companyId - unique id of company
 * @param {function} callBc - deals with response
 */
  COMPANY.getCompany = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, callBc);
    } else {
      errorResponse(callBc);
    }
  };
  // remote method declaration to get company details based on company id
  COMPANY.remoteMethod('getCompany', {
    description: 'To get company details based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getCompany',
      verb: 'get',
    },
  });
  var Joi = require('joi');
  var companyData;
  //update method starts from here
  /**
 *updateCompany- To update company details
 *@constructor
 * @param {number} companyData - contains data need to get updated
 * @param {function} cb - deals with response
 */
  COMPANY.updateCompany = function(companyData, cb) {
    var updateCompanyDet = require('../../commonCompanyFiles/update-company-profile');
    updateCompanyDet.updateCompanyProfile(companyData, function(err, resp) {
      logger.info('upating company profile ');
      if (err) {
        cb(err, resp);
      } else {
        cb(null, resp);
      }
    });
  };

  //custom method for update company
  COMPANY.remoteMethod('updateCompany', {
    description: 'Send Valid companyId ',
    returns: {
      arg: 'data',
      type: 'object',
    },
    accepts: [
      {
        arg: 'data',
        type: 'object',
        http: {
          source: 'body',
        },
      },
    ],
    http: {
      path: '/updateCompany',
      verb: 'PUT',
    },
  });
  COMPANY.getEventStudentList = function(employerEventId, campusEventId, cb) {
    var getDetails = require('../../commonCompanyFiles/export-details');
    if (employerEventId) {
      getDetails.exportData(employerEventId, function(err, resp) {
        logger.info('upating company profile ');
        if (err) {
          cb(err, resp);
        } else {
          cb(null, resp);
        }
      });
    } else if (campusEventId) {
      getDetails.exportDetails(campusEventId, function(err, resp) {
        logger.info('upating company profile ');
        if (err) {
          cb(err, resp);
        } else {
          cb(null, resp);
        }
      });
    } else {
      errorResponse('employerEventId or campusEventId should be provided', cb);
    }
  };
  COMPANY.remoteMethod('getEventStudentList', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'employerEventId', type: 'number', http: {source: 'query'}},
      {arg: 'campusEventId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getEventStudentList',
      verb: 'get',
    },
  });
  COMPANY.companyProfile = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getProfile = require('../../commonCompanyFiles/company-profile').getProfile;
          getProfile(res, function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('companyProfile', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/companyProfile',
      verb: 'get',
    },
  });
  COMPANY.Drives = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getDrives = require('../../commonCompanyFiles/company-profile').getDrivesInProcess;
          getDrives(res.data[0], function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('Drives', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/Drives',
      verb: 'get',
    },
  });
  COMPANY.getJobDetailsOfCompany = function(companyId, callBc) {
    if (companyId) {
      var companyDetById = require('../../ServicesImpl/CommonImpl/getEntityDetails.js')
        .entityDetailsById;
      var inputFilterObject = {};
      inputFilterObject['companyId'] = companyId;
      var company = server.models.Company;
      companyDetById(inputFilterObject, company, function(err, res) {
        if (err) {
          throwError('invalid id', callBc);
        } else {
          var getJobDetails = require('../../commonCompanyFiles/company-profile').getJobDetails;
          getJobDetails(res.data[0].companyId, function(err, res) {
            if (err) {
              throwError('an error in line 160', callBc);
            } else {
              callBc(null, res);
            }
          });
        }
      });
    } else {
      errorResponse(callBc);
    }
  };
  COMPANY.remoteMethod('getJobDetailsOfCompany', {
    description: 'To get company profile based on company id',
    returns: {
      type: 'object',
      root: true,
    },
    accepts: [
      {
        arg: 'companyId',
        type: 'number',
        required: true,
        http: {
          source: 'query',
        },
      },
    ],
    http: {
      path: '/getJobDetailsOfCompany',
      verb: 'get',
    },
  });
  COMPANY.getSheduledEvents = function(companyId, cb) {
    var events = require('../../commonCompanyFiles/get-Events');
    events.getSheduledEvents(companyId, function(err, response) {
      if (err) {
        cb(err, null);
      } else {
        cb(null, response);
      }
    });
  };
  COMPANY.remoteMethod('getSheduledEvents', {
    description: 'To get company address details',
    returns: {
      type: 'array',
      root: true,
    },
    accepts: [
      {arg: 'companyId', type: 'number', http: {source: 'query'}},
    ],
    http: {
      path: '/getSheduledEvents',
      verb: 'get',
    },
  });
};
