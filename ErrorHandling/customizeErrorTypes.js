/**
 * Created by yogi on 27-07-2017.
 */
// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use hyphen separated naming conve ntion for the file.
'use strict';
/**
   *invalidInputError - deals with error with Invalid input parametesr error
   * @param {function} callBc - deals with the response
   */
function invalidInputError(callBc) {
  // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Have a provision to mention what is the invalid input
  // This could be an optional parameter but will enable callers to identify and fix.
  // Add logger statements here.
  var inputError = new Error('invalid input parameters');
  inputError.statusCode = 422; // TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Use constants instead of numbers. What does 422 mean? It could be ERROR_INVALID_INPUT which means better code readability
  inputError.requestStatus = false;
  callBc(inputError, null);
}
/**
   *errorFunction-function deals with error handeling takes the message as input and displays the message as an out put error
   *@constructor
   * @param {string} input - contains the message need to get displayed as an error
   * @param {function} cb - deals with the response
   */
function errorFunction(input, cb) {
  var error = new Error(input);
  error.statusCode = 422;
  error.requestStatus = false;
  cb(error, null);
}
exports.invalidInputError = invalidInputError;
exports.errorFunction = errorFunction;
