'use strict';
var dsConfig = require('../datasources.json');
var path = require('path');
module.exports = function(server) {
  var ScoraUser = server.models.ScoraUser;
  var fieldLabel = server.models.FieldLabel;
  var lookupValue = server.models.LookupValue;
  var caching = require('./caching/labelCaching').apiLabelset;
  var lookup = require('./caching/labelCaching').getLookUpData;
  var formatLookUpData = require('./caching/labelCaching').formatLookUpData;
  var getLookUpData = require('./caching/labelCaching').getLookUpData;

  // Install a `/` route that returns server status
  var router = server.loopback.Router();
  //login page
  router.get('/', function(req, res) {
    var credentials = dsConfig.emailDs.transports[0].auth;
    res.render('login', {
      email: credentials.user,
      password: credentials.pass,
    });
  });
  router.get('/apiLabelset', function(req, res) {
    caching(fieldLabel, 'LabelKey', function(obj) {
      res.send(obj);
    });
  });

  //Lookup values
  router.get('/lookupAPI', function(req, res) {
    getLookUpData(lookupValue, formatLookUpData);
    // console.log('ddddddd');
    res.send('lookupAPI');
  });
  //verified
  router.get('/verified', function(req, res) {
    res.render('verified');
  });

  //log a ScoraUser in
  router.post('/login', function(req, res) {
    // console.log('req.bodyyyyyyyyyyyyyyy ', req.body.email);
    // console.log('req.bodyyyyyyyyyyyyyyy ', req.body.password);
    ScoraUser.login({
      email: req.body.email,
      password: req.body.password,
    }, 'ScoraUser', function(err, token) {
      if (err) {
        if (err.details && err.code === 'LOGIN_FAILED_EMAIL_NOT_VERIFIED') {
          res.render('reponseToTriggerEmail', {
            title: 'Login failed',
            content: err,
            redirectToEmail: '/api/ScoraUsers/' + err.details.userId + '/verify',
            redirectTo: '/',
            redirectToLinkText: 'Click here',
            userId: err.details.userId,
          });
        } else {
          res.render('response', {
            title: 'Login failed. Wrong username or password',
            content: err,
            redirectTo: '/',
            redirectToLinkText: 'Please login again',
          });
        }
        return;
      }
      res.render('home', {
        email: req.body.email,
        accessToken: token.id,
        redirectUrl: '/api/ScoraUsers/change-password?access_token=' + token.id,
      });
    });
  });

  //log a user out
  router.get('/logout', function(req, res, next) {
    if (!req.accessToken) return res.sendStatus(401);
    ScoraUser.logout(req.accessToken.id, function(err) {
      if (err) return next(err);
      res.redirect('/');
    });
  });

  //send an email with instructions to reset an existing ScoraUser's password
  router.post('/request-password-reset', function(req, res, next) {
    // console.log('in reset password reset routeeeeeeeeeeeeeeeeeeeeeee');
    ScoraUser.resetPassword({
      email: req.body.email,
    }, function(err) {
      if (err) return res.status(401).send(err);

      res.render('response', {
        title: 'Password reset requested',
        content: 'Check your email for further instructions',
        redirectTo: '/',
        redirectToLinkText: 'Log in',
      });
    });
  });

  //show password reset form
  router.get('/reset-password', function(req, res, next) {
    // console.log('reset passwordddddddddddddddddddddddddddddddddddddddddddddddddddddddd');
    if (!req.accessToken) return res.sendStatus(401);
    res.render('password-reset', {
      redirectUrl: '/api/ScoraUsers/reset-password?access_token=' +
        req.accessToken.id,
    });
  });
  server.use(router);
  // server.use(caching());
};
