'use strict';

var server = require('./../../server');
var nodeCache = require('node-cache');

//Definition of System Tables
var fieldLabel = server.models.FieldLabel;
var currencyModel = server.models.Currency;
var lookupType = server.models.LookupType;
var lookupValue = server.models.LookupValue;

//Definition of utility functions
var findEntityDetailsAsIs = require('./utils').findEntityDetailsAsIs;
var createCache = require('./utils').createCache;
var setCache = require('./utils').setCache;
var getCache = require('./utils').getCache;
var printEntityDetails = require('./utils').printEntitydetails;


/**
 *The function will format the Label Table as needed in Cache.
 *The function accept an error function, a json object for Label Model and a callback function
 * @param {function} error
 * @param {Object} entitydetails
 * @param {function} callback
 */
function formatLabelTable(callback, error, modelname, entitydetails) {
  var formattedObject = {};
  entitydetails.map(function (obj) {
    formattedObject[obj.fieldName] = obj.label;
  });
  callback(null, modelname, formattedObject);
}

const key = 'labelKey';


/**
 * 
 * 
 * @param {string} modelname 
 * @param {string} key 
 * @param {function} callback 
 */
function apiLabelset(modelname, key, callback) {
  //var setCacheCB = setCache.bind(null, cacheName, key, entityDetails);
  //var createCacheCB = createCache.bind(null, modelname, entityDetails, setCacheCB);
  //var formatLabelTableCB = formatLabelTable.bind(null, entitydetails, createCacheCB);
  findEntityDetailsAsIs(modelname,
    formatLabelTable.bind(null,
      createCache.bind(null,
        setCache.bind(null,
          getCache.bind(null, callback, key), key), key)));
}

function getLookUpData(modelName, callback) {
  modelName.find({
    include: ['lookupValueIbfk1rel']
  }, function (error, result) {
    if (error) {
      callback(error, null);
    } else {
      callback(null, error, result);
    }
  });
}


function formatLookUpData(callback, error, entitydetails) {
  //console.log(entitydetails);
  let lookupValueArray = [];
  let lookupTypeArray = [];
  let formattedObject = {};

  const lookupValueId = 'lookupValueId';
  const lookupTypeId = 'lookupTypeId';
  const lookupValue = 'lookupValue';

  var checkSameTypeCode;
  var tempTypeCode;

  let flag = false;
  entitydetails.map(function (lookupRecord) {
    const lookupValueObject = {};
    var tempObject = JSON.stringify(lookupRecord);
    var lookupRecord = JSON.parse(tempObject);
    lookupValueObject[lookupValueId] = lookupRecord.lookupValueId;
    lookupValueObject[lookupTypeId] = lookupRecord.lookupTypeId;
    lookupValueObject[lookupValue] = lookupRecord.lookupValue;

    if(flag === false){
      checkSameTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
      tempTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
      flag = true;
    }
    if (checkSameTypeCode !== lookupRecord.lookupValueIbfk1rel.lookupCode) {
      formattedObject[tempTypeCode] = lookupValueArray;
      tempTypeCode = checkSameTypeCode;
      lookupValueArray = [];
    }
      lookupValueArray.push(lookupValueObject);
      checkSameTypeCode = lookupRecord.lookupValueIbfk1rel.lookupCode;
  })
  lookupTypeArray.push(formattedObject);
  // console.log(lookupTypeArray)
}
getLookUpData(lookupValue, formatLookUpData);
//How to declare the function.
apiLabelset(fieldLabel, 'LabelKey', function (obj) {
  return obj;
});
exports.apiLabelset = apiLabelset;
exports.formatLookUpData = formatLookUpData;
exports.getLookUpData = getLookUpData;