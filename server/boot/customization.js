/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client
/**
 * Created by admin on 17-07-2017.
 */
// var server1 = require('../../server/server');
// This file will handle request before entering into execution of business logic available at common/models folder
// Onnce transaction done then we change response before sending to client

// TODO: CODEREVIEW - SWAROOP - 15AUG17 -- Remove once Vijay's authorization comes into place.
'use strict';
module.exports = function(server) {
  var remotes = server.remotes();
  //Before remote method for authentication
  remotes.before('**', function(ctx, next) {
    var url = ctx.req.originalUrl.toString();
    var method = ctx.req.method.toString();
    var throwError = require('../../ErrorHandling/customizeErrorTypes').errorFunction;
    var createUserId = ctx.req.body.createUserId;
    var updateUserId = (ctx.req.body.updateUserId) ? ctx.req.updateUserId : createUserId;
    var scoraServices = server.models.ScoraServices;
    var scoraRolePrivileges = server.models.ScoraRolePrivileges;
    var scoraPrivilegeService = server.models.ScoraPrivilegeService;
    var searchShort = (url) ? url.indexOf('access_token') >= 0 : false;

    if (url.indexOf('/api/ScoraUsers') > -1) {
      loginService(method, next);
    } else {
      if (searchShort) {
        // console.log('searchShortttttttttttttttttttttttttttt', url.split('='));
        var accessToken = url.split('=');
        if (accessToken) {
          var serviceName = url.split('?');
          scoraServices.findOne({
            'where': {
              'and': [{
                'serviceName': serviceName[0],
              },
              {
                'methodName': method,
              },
              ],
            },
          }, function(err, services) {
            if (services) {
              var serviceId = services.serviceId;
              var accessTokenModel = server.models.AccessToken;
              accessTokenModel.findOne({'were': {'id': accessToken}}, function(err, token) {
                // console.log('searchShortttttttttttttttttttttttttttt', token);
                var userId = token.userId;
                var scoraUserRole = server.models.ScoraUserRole;
                if (token) {
                  scoraUserRole.findOne({
                    'where': {
                      'id': userId,
                    },
                  }, function(error, role) {
                    // console.log(',,,,,,,,,,,,,,,,,,,,,,,,,,,,,', role);
                    if (role) {
                      var roleCode = role.roleCode;
                      var scoraRolePrivileges = server.models.ScoraRolePrivileges;
                      scoraRolePrivileges.findOne({
                        'where': {
                          'roleCode': roleCode,
                        },
                      }, function(roleErr, scoraRolePrivileges) {
                        // console.log('...............................', scoraRolePrivileges);
                        if (scoraRolePrivileges) {
                          var privilegeId = scoraRolePrivileges.privilegeId;
                          scoraPrivilegeService.findOne({
                            'where': {
                              'and': [{
                                'privilegeId': privilegeId,
                              }, {
                                'serviceId': serviceId,
                              }],
                            },
                          }, function(priErr, privileges) {
                            // console.log(';;;;;;;;;;;;;;;;;;;;;;;;;;;;;;', privileges);
                            if (privileges) {
                              next();
                            } else {
                              unauthorizedRes(next);
                            }
                          });
                        } else {
                          unauthorizedRes(next);
                        }
                      });
                    } else {
                      unauthorizedRes(next);
                    }
                  });
                } else {
                  unauthorizedRes(next);
                }
              });
            } else {
              unauthorizedRes(next);
            }
          });
        } else {
          unauthorizedRes(next);
        }
      } else {
        unauthorizedRes(next);
      }
    }
  });
  // Modify response before sending response to requested user/client
  remotes.after('**', function(ctx, next) {
    if (ctx.req.method.toString() == 'POST') {
      ctx.result = {
        data: ctx.result,
        requestStatus: true,
      };
      next();
    } else {
      next();
    }
  });
  // error message if any unauthorized request comes to server
  var unauthorizedRes = function(next) {
    var adminAuthorizationError = new Error('AUTHORIZATION_REQUIRED');
    adminAuthorizationError.statusCode = 401;
    // adminAuthorizationError.requestStatus = false;
    next(adminAuthorizationError, null);
  };
  var loginService = function(method, next) {
    console.log('hhhhhhhh');
    if (method == 'POST' || method == 'GET') {
      next();
    } else {
      unauthorizedRes(next);
    }
  };
};
